/**
 * @file Config.
 */

export const SOURCE_LIST = [`newsletter`, `prelaunch`] as const; // "as const" required to create union type from tuple.
export const PORT = Number.parseInt(process.env.PORT || `0`, 10);
export const MAILCHIMP_API_URI = process.env.MAILCHIMP_API_URI || ``;
export const MAILCHIMP_LIST_ID = process.env.MAILCHIMP_LIST_ID || ``;
export const SLACK_WEBHOOK_URI = process.env.SLACK_WEBHOOK_URI || ``;
