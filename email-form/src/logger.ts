/**
 * @file Logger.
 */

/**
 * Describes the types that can be logged out.
 */
export type TLoggableValue = string | number | boolean | Date;

/**
 * Logs out the given input as an info log.
 * @param input A value that can be logged.
 * @returns Nothing.
 */
export function info(input: TLoggableValue): void {
	process.stdout.write(`${input}\n`);
}

/**
 * Logs out the given input as an error log.
 * @param input A value that can be logged.
 * @returns Nothing.
 */
export function error(input: TLoggableValue): void {
	process.stderr.write(`${input}\n`);
}
