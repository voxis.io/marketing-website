/**
 * @file Mailchimp API.
 */

import crypto from 'crypto';
import fetch from 'node-fetch';

import { MAILCHIMP_API_URI, MAILCHIMP_LIST_ID } from './config';
import { TSubmissionSource } from './definitions';
import * as logger from './logger';

/**
 * Hashes and returns the given email address.
 * @param email The email to hash.
 * @returns The hash of the email.
 */
export function createSubscriberHash(email: string): string {
	return crypto.createHash(`md5`).update(email).digest(`hex`);
}

/**
 * Add the user's email to the Mailchimp list.
 * @param email The user's email address.
 * @returns Nothing.
 */
export async function addEmailToMailchimp(email: string): Promise<void> {
	logger.info(`Adding email "${email}" to Mailchimp`);

	const url = `${MAILCHIMP_API_URI}/lists/${MAILCHIMP_LIST_ID}`;
	const body = {
		update_existing: true,
		members: [
			{
				email_address: email,
				email_type: `html`,
				status: `subscribed`,
			},
		],
	};
	const opts = {
		method: `post`,
		body: JSON.stringify(body),
		headers: {
			'accept': `application/json`,
			'content-type': `application/json`,
		},
	};
	const res = await fetch(url, opts);
	if (res.status < 200 && res.status > 299) throw new Error(`Failed to add email "${email}" to Mailchimp, status code: ${res.status}`);

	logger.info(`Email "${email}" added to Mailchimp`);
}

/**
 * Tag the user's email in the Mailchimp list.
 * @param subscriberHash The hash of the email on the list to add the tag to.
 * @param source The source of the email submission.
 * @returns Nothing.
 */
export async function addTagToMailchimp(subscriberHash: string, source: TSubmissionSource): Promise<void> {
	logger.info(`Adding tag "${source}" to Mailchimp user "${subscriberHash}"`);

	const url = `${MAILCHIMP_API_URI}/lists/${MAILCHIMP_LIST_ID}/members/${subscriberHash}/tags`;
	const body = {
		tags: [
			{
				name: source,
				status: `active`,
			},
		],
	};
	const opts = {
		method: `post`,
		body: JSON.stringify(body),
		headers: {
			'accept': `application/json`,
			'content-type': `application/json`,
		},
	};

	const res = await fetch(url, opts);
	if (res.status < 200 && res.status > 299) throw new Error(`Failed to add tag "${source}" to Mailchimp user "${subscriberHash}", status code: ${res.status}`);

	logger.info(`Tag "${source}" added to Mailchimp user "${subscriberHash}"`);
}
