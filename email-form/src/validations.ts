/**
 * @file Validations.
 */

import { SOURCE_LIST } from './config';
import { TSubmissionSource } from './definitions';

/**
 * Throws an error if the email isn't valid.
 * @param email The user's email address.
 * @returns Nothing.
 */
export function validateEmail(email: string): void {
	if (!email.match(/^.+@.+$/i)) throw new Error(`Invaid email address "${email}"`);
}

/**
 * Throws an error if the source isn't valid.
 * @param source The source of the email submission.
 * @returns Nothing.
 */
export function validateSource(source: TSubmissionSource): void {
	if (!SOURCE_LIST.includes(source)) throw new Error(`Invalid source "${source}"`);
}
