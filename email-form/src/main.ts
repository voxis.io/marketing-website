/**
 * @file Main entry point.
 */

import http, { IncomingMessage, ServerResponse } from 'http';

import { PORT } from './config';
import { IRequestBody, TSubmissionSource } from './definitions';
import * as logger from './logger';
import { addEmailToMailchimp, addTagToMailchimp, createSubscriberHash } from './mailchimp';
import { sendSlackMessage } from './slack';
import { validateEmail, validateSource } from './validations';

/**
 * Stringifies and sends the given data and status code to the client.
 * @param res The outgoing response.
 * @param statusCode The status code to set.
 * @param data The JSON data to send.
 * @returns Nothing.
 */
async function sendResponse(res: ServerResponse, statusCode: number, data = {}): Promise<void> {
	return new Promise(resolve => {
		const output = JSON.stringify(data);
		res.setHeader(`content-type`, `application/json`);
		res.statusCode = statusCode;
		res.end(output, resolve);
	});
}

/**
 * Takes an email address in the event body, adds it to a Mailchimp list, and sends a Slack webhook notification.
 * @param req The incoming request.
 * @param res The outgoing response.
 * @param rawBody The raw body payload as a string.
 * @returns Nothing.
 */
async function processMessage(req: IncomingMessage, res: ServerResponse, rawBody: string): Promise<void> {
	const body = JSON.parse(rawBody) as IRequestBody;
	const email = (body.email ?? ``).toLowerCase();
	const source = (body.source ?? `prelaunch`).toLowerCase() as TSubmissionSource;

	validateEmail(email);
	validateSource(source);

	await addEmailToMailchimp(email);
	const subscriberHash = createSubscriberHash(email);
	await addTagToMailchimp(subscriberHash, source);
	await sendSlackMessage(email, source);

	await sendResponse(res, 200, { success: true, email, source });
}

/**
 * Handles the incoming request.
 * @param req The incoming request.
 * @param res The outgoing response.
 * @returns Nothing.
 */
async function handleIncomingMessage(req: IncomingMessage, res: ServerResponse): Promise<void> {
	try {
		logger.info(`Incoming request`);

		let rawBody = ``;

		req.on(`data`, (chunk: Buffer) => (rawBody += chunk.toString()));
		req.on(`end`, () => processMessage(req, res, rawBody));
	} catch (e) {
		const err = e as NodeJS.ErrnoException;
		logger.error(`Failed to handle incoming request: ${err.message}`);
		await sendResponse(res, 500, { success: false, error: err.message });
	}
}

/**
 * Main entry point.
 */
// tslint:disable-next-line:no-floating-promises
(async function main(): Promise<void> {
	await new Promise(resolve => {
		const server = http.createServer(handleIncomingMessage);
		server.listen(PORT, resolve);
	});
})();
