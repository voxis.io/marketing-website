/**
 * @file Slack API.
 */

import { SLACK_WEBHOOK_URI } from './config';
import { TSubmissionSource } from './definitions';
import * as logger from './logger';

/**
 * Returns the correct action portion of the message text to send to Slack.
 * @param source The source of the email submission.
 * @returns The action portion of the message text.
 */
function getMessageAction(source: TSubmissionSource): string {
	switch (source) {
		case `newsletter`:
			return `signed up for the \`newsletter\`!`;

		case `prelaunch`:
			return `registered for the \`prelaunch\`!`;

		default:
			return `done something cool but I'm not sure what?! :thinking_face:`;
	}
}

/**
 * Sends a message to the Slack channel via a webhook.
 * @param email The user's email address.
 * @param source The source of the email submission.
 * @returns Nothing.
 */
export async function sendSlackMessage(email: string, source: TSubmissionSource): Promise<void> {
	logger.info(`Sending message to Slack for email "${email}"`);

	const messageAction = getMessageAction(source);
	const body = {
		text: `:tada: Woo! ${email} has just ${messageAction}`,
	};
	const opts = {
		method: `post`,
		body: JSON.stringify(body),
		headers: {
			'accept': `application/json`,
			'content-type': `application/json`,
		},
	};
	const res = await fetch(SLACK_WEBHOOK_URI, opts);
	if (res.status < 200 && res.status > 299) throw new Error(`Failed to send message to Slack for email "${email}", status code: ${res.status}`);

	logger.info(`Message sent to Slack for email "${email}"`);
}
