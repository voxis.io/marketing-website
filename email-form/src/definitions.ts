/**
 * @file Type definitions.
 */

import { SOURCE_LIST } from './config';

/**
 * The possible values for the source of the email submission.
 */
export type TSubmissionSource = typeof SOURCE_LIST[number];

/**
 * Incoming request body payload.
 */
export interface IRequestBody {
	email?: string;
	source?: TSubmissionSource;
}
