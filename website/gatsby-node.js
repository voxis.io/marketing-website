/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

const path = require(`path`);
const { createFilePath } = require(`gatsby-source-filesystem`);

exports.onCreateWebpackConfig = ({ getConfig, actions }) => {
  if (getConfig().mode === `production`) {
    actions.setWebpackConfig({
      devtool: false,
    });
  }
};

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions;

  if (node.internal.type === `MarkdownRemark`) {
    const slug = createFilePath({ node, getNode, basePath: `pages`, trailingSlash: false });
    createNodeField({ node, name: `slug`, value: slug });
  }
};

async function createBlogPostPage(actions, node, graphql) {
  const { createPage } = actions;

  const result = await graphql(`
    query {
      markdownRemark(fields: { slug: { eq: "${node.fields.slug}" } }) {
        excerpt(pruneLength: 200)
        frontmatter {
          title
          author
        }
      }
    }
  `);

  createPage({
    path: node.fields.slug,
    component: path.resolve(`./src/templates/BlogPosts/BlogPost.js`),
    context: { // Data passed to context is available in page queries as GraphQL variables.
      slug: node.fields.slug,
      meta: {
        title: result.data.markdownRemark.frontmatter.title,
        author: result.data.markdownRemark.frontmatter.author,
        synopsis: result.data.markdownRemark.frontmatter.synopsis,
      },
    },
  });
}

exports.createPages = async ({ graphql, actions }) => {
  const result = await graphql(`
    query {
      allMarkdownRemark {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
    }
  `);

  const promises = result.data.allMarkdownRemark.edges.map(async ({ node }) => {
    const [, nodeType] = node.fields.slug.split(`/`);

    switch (nodeType) {
      case `learn`: await createBlogPostPage(actions, node, graphql); break;
      default: break;
    }
  });

  await Promise.all(promises);
};
