const FACEBOOK_USERNAME = `voxis.io`;
const LINKEDIN_USERNAME = `voxis-io`;
const TWITTER_USERNAME = `voxis_io`;

module.exports = {
  siteMetadata: {
    title: `Voxis™ | Code to Cloud in 60s`,
    titleShort: `Voxis™`,
    description: `Voxis is an open source toolkit for shipping cloud software; with auto CI/CD pipelines, zero config deployments, global CDN, full lifecycle management.`,
    author: `Voxis Engineering`,
    brandName: `Voxis`,
    legalName: `Voxis Engineering Ltd`,
    officeAddress: `TechHub London, 1-15 Clere Street, London, EC2A 4UY, United Kingdom`,
    companyNumber: `11244549`,
    vatNumber: `[vatNumber]`,
    siteUrl: `https://www.voxis.io`,
    socialLinks: {
      facebook: `https://www.facebook.com/${FACEBOOK_USERNAME}`,
      linkedIn: `https://www.linkedin.com/company/${LINKEDIN_USERNAME}`,
      twitter: `https://www.twitter.com/${TWITTER_USERNAME}`,
    },
    socialIds: {
      facebook: FACEBOOK_USERNAME,
      linkedIn: LINKEDIN_USERNAME,
      twitter: TWITTER_USERNAME,
    },
    emails: {
      main: `hello@voxis.io`,
    },
  },
  plugins: [
    {
      resolve: `gatsby-plugin-smoothscroll`,
    },
    {
      resolve: `gatsby-plugin-sharp`,
    },
    {
      resolve: `gatsby-transformer-sharp`,
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-autolink-headers`, // Must be before Prism.
            options: {
              enableCustomId: true,
              isIconAfterHeader: false,
            },
          },
          {
            resolve: `gatsby-remark-prismjs`,
            options: {
              classPrefix: `language-`,
              showLineNumbers: true,
              inlineCodeMarker: `›`,
              prompt: {
                user: `root`,
                host: `localhost`,
                global: true,
              },
            },
          },
          {
            resolve: `gatsby-remark-external-links`,
            options: {
              target: `_blank`,
              rel: `noreferrer noopener`,
            },
          },
          {
            resolve: `gatsby-remark-copy-linked-files`,
          },
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1280,
              linkImagesToOriginal: true,
              showCaptions: true,
              quality: 80,
              withWebp: true,
            },
          }
        ],
      },
    },
    {
      resolve: `gatsby-plugin-react-helmet`,
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `content`,
        path: `${__dirname}/src/content`,
      },
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /images\/vector\/foreground/i,
        },
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Voxis Engineering`,
        short_name: `Voxis`,
        start_url: `/`,
        background_color: `#1E3044`,
        theme_color: `#01B9AC`,
        display: `minimal-ui`,
        icon: `src/images/raster/voxis-icon.png`,
      },
    },
    {
      resolve: `gatsby-plugin-offline`,
    },
    {
      resolve: `gatsby-plugin-sitemap`,
    },
    {
      resolve: `gatsby-plugin-sass`,
      options: {
        data: `
          @import "${__dirname}/src/styles/globals/theme.scss";
          @import "${__dirname}/src/styles/globals/mixins.scss";
        `,
      },
    },
    {
      resolve: `gatsby-plugin-remove-trailing-slashes`,
    },
    {
      resolve: `gatsby-plugin-canonical-urls`,
      options: {
        siteUrl: `https://www.voxis.io`,
        stripQueryString: true,
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: `UA-149878131-1`,
        head: false,
        anonymize: false,
        respectDNT: false,
        pageTransitionDelay: 0,
      },
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `Barlow:400,400i,700,700i`,
        ],
        display: 'swap',
      },
    },
    {
      resolve: `gatsby-plugin-robots-txt`,
      options: {
        policy: [{ userAgent: `*`, allow: `/` }],
      }
    }
  ],
};
