/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */

const React = require(`react`);

const freshChatScript = <script dangerouslySetInnerHTML={{
	__html: `
function initFreshChat() {
	window.fcWidget.init({
		token: "2fce396c-bcea-4a3f-b803-6ec26f9d96bd",
		host: "https://wchat.eu.freshchat.com",
		siteId: "voxis-io-${process.env.NODE_ENV}"
	});
}
function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.eu.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
`,
}} />;

const freshMarketerScript = <script src='https://cdn.freshmarketer.eu/1003262/3126.js'></script>;

const shareThisScript = <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5e5d5a2c88cddc001ad2b84c&product=inline-share-buttons" async="async" />;

exports.onRenderBody = ({ setHeadComponents }) => {
	setHeadComponents([
		freshChatScript,
		freshMarketerScript,
		shareThisScript,
	]);
};
