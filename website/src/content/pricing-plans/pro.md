---
name: Pro
priceMonthly: 45
priceAnnually: 450
priceText: ''
featureDeployedApps: '10'
featureCompute: '500hrs'
featureDataStorage: '10GB'
featureDataTransfer: '50GB'
featureConcurrentBuilds: '3'
mostPopular: true
weight: 2
---
