---
name: Starter
priceMonthly: 0
priceAnnually: 0
priceText: 'Free'
featureDeployedApps: '3'
featureCompute: '25hrs'
featureDataStorage: '1GB'
featureDataTransfer: '5GB'
featureConcurrentBuilds: '1'
mostPopular: false
weight: 1
---
