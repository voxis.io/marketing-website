---
name: Business
priceMonthly: 1000
priceAnnually: 10000
priceText: ''
featureDeployedApps: '50'
featureCompute: '10,000hrs'
featureDataStorage: '100GB'
featureDataTransfer: '500GB'
featureConcurrentBuilds: '10'
mostPopular: false
weight: 3
---
