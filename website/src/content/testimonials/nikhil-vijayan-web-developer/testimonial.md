---
name: 'Nikhil Vijayan'
hidden: false
photo: './photo.jpg'
job: 'Web Developer'
date: '2020-05-17'
clientType: 'developer'
text: 'Using Voxis has made my life much easier. I find it much more simple than the alternatives to deploy apps to the cloud, I connect my GitHub repository and everything I push is deployed without any effort.'
weight: 4
---
