---
name: 'Will Wayman'
hidden: false
photo: './photo.jpg'
job: 'Web Developer'
date: '2020-05-17'
clientType: 'developer'
text: "My team's productivity increased after we added Voxis to the mix. The setup was quick and the amount of time we spend managing deployments and infrastructure has dropped dramatically."
weight: 3
---
