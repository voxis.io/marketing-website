---
name: 'Tom Smith'
hidden: true
photo: './photo.png'
job: 'Founder at Tech Startup'
date: '2020-02-15'
clientType: 'startup'
text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
weight: 2
---
