---
image: ImgUseCaseWebApp
titleSingular: Web App
titlePlural: Web Apps
subTitle: Web apps built with React, Vue or the latest framework are a breeze to deploy and host. Backend server not required.
relatedFeatures:
  [
    'deployments',
    'free-tls-ssl',
		'global-cdn',
		'metrics-and-logging',
		'multiple-environments',
		'predictable-costs',
  ]
demoRepoUrl: https://gitlab.com/voxis.io/live-demos/http-status-codes
demoName: HTTP Status Codes
demoImage: ./media/http-status-codes-logo.png
weight: 2
---

Some **markdown** content goes here for web apps.

- What are web apps.

- Why do I want one.

- How can I build one.

- How do they differ to older ways of doing things.

- How does Voxis help.

- How do I get started.
