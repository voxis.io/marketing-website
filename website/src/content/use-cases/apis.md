---
image: ImgUseCaseApi
titleSingular: API
titlePlural: APIs
subTitle: Build rich backends in record time with boilerplate code, cloud infrastructure, and CI/CD provided by Voxis.
relatedFeatures:
  [
		'metrics-and-logging',
		'multiple-environments',
		'automatic-rollbacks',
		'automated-testing',
		'infinite-auto-scaling',
		'redundancy',
  ]
demoRepoUrl: https://gitlab.com/voxis.io/live-demos/http-status-codes
demoName: HTTP Status Codes
demoImage: ./media/http-status-codes-logo.png
weight: 3
---

Some **markdown** content goes here for backend APIs.
