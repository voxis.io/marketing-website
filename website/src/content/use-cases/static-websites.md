---
image: ImgUseCaseStaticWebsite
titleSingular: Static Website
titlePlural: Static Websites
subTitle: Deploy websites built with static site generators like Gatsby, Hugo, and Nuxt in seconds. No servers, no mess.
relatedFeatures: ['deployments', 'free-tls-ssl', 'global-cdn']
demoRepoUrl: https://gitlab.com/voxis.io/live-demos/http-status-codes
demoName: HTTP Status Codes
demoImage: ./media/http-status-codes-logo.png
weight: 1
---

# Static Websites

## Contents

1. [What are static websites?](#what)
2. [Why do I want one to build a static website?](#why)
3. [How can I build a static website?](#how)
4. [How do I get started?](#start)

## What are static websites? {#what}

The term "static website" refers to a website made up of HTML, CSS and JavaScript files that aren't dynamically rendered on request. In a way, this modern trend harks backs to a simpler time where our only choice for our websites was to serve up static files. These days, users expect rich interactive experiences, with custom behaviour, streaming media, and stunning designs. And this has to work on any browser on desktop, mobile and everything in between.

The last 20 years has seen a proliferation of dynamically generated websites with the likes of PHP, Ruby on Rails, WordPress, Node.js, Express, and friends. These technologies have allowed us to deliver the rich experiences users expect, but they also require a backend application to generate the static HTML and CSS. This means more code, a greater surface area for attack, and servers burning energy 24/7 regardless of whether we need their full capacity.

Thankfully, the proliferation of frontend JavaScript tooling for creating rich web apps has allowed us to move a lot of the complexity to build time, so that the static elements can be generated ahead of time, and served up by a simple web server without the complexity of a backend. Of course, you can still pull in dynamic data from a backend, database, or other data source of your choice, but it's no longer necessary to build an application to do all this.

## Why do I want one to build a static website? {#why}

You might well ask why you should care about this new trend for static site generation? Well thankfully, you can wave goodbye to flaky FTP, security holes, and server maintenance, and say hello to the modern web:

- **Modern tooling** - The freedom to use modern techniques and build tooling to speed up development and make it less of a chore.
- **Browser support** - Automated build tooling enables you to target a much wider range of browsers and devices with no work on your part.
- **No backend necessary** - Less code to maintain and no servers to worry about means more time to spend on higher-value activities.
- **Cheaper to host** - Instead of a server instance you can use a static site host, blob storage such as AWS S3, or a low-cost web server.
- **More secure** - The surface area for vulnerabilities is much smaller because there's no backend or servers to maintain.
- **Energy efficient** - No backend means less energy consumed in the cloud, whilst optimised websites are less battery-hungry for users.
- **TLS by default** - Most methods for hosting static sites provide free TLS (SSL) out of the box, so that's one less thing to worry about.

## How can I build a static website? {#how}

There are many ways to build static websites. These days you won't want to reach for plain HTML and CSS, but you might like to investigate one of the plethora of static site generators that can make your life easier. Some of the most popular include:

**[Gatsby](https://www.gatsbyjs.org/)** - A widely used static site generator based on React that allows you to build complex interactive websites by writing React components and choosing from a vast range of plugins. Gatsby provides a GraphQL API for pulling data into components and pages, and aims to be fast at compiling big sites.

**[Hugo](https://gohugo.io/)** - A popular static site generator built in Go. Content is written in Markdown files with metadata specified in YAML, TOML or JSON. Hugo is a great choice for blogs and documentation sites, and whilst it's a bit opinionated you get simplicity and blazing fast build times in exchange. Hugo wants to make website building fun again.

**[Nuxt](https://nuxtjs.org/)** - If you prefer Vue to React you may like Nuxt. Similar to Gatsby, Nuxt allows developers the freedom of the full Vue framework to build their websites without losing the benefits of a statically generated website. Nuxt has modules for many common uses cases including connecting to GraphQL and REST endpoints, AMP support, and authentication, amongst others.

**[Docusaurus](https://docusaurus.io/)** - A great choice for documentation sites, Docusaurus provides features to enable versioning, internationalisation, and searching of documentation out of the box. Content is written in Markdown files in a predefined directory structure. Docusaurus is a popular choice for many opensource projects.

**[Svelte](https://svelte.dev/)** - A fresh approach to frontend frameworks where the heavy lifting is done at build time, not in the user's browser. This means much less JavaScript has to be sent to the client, resulting in faster loading times, super fast responsiveness, and a less battery-hungry website. Svelte is an interesting, if not widely used, choice.

**[Jekyll](https://jekyllrb.com/)** - A slightly older tool than some of the others listed here, Jekyll is built in Ruby and compiles Markdown or Liquid files into a website. If you're looking for a simple tool, Jekyll could be a good choice, but it lacks some of the advanced functionality of the newer tools.

**[VuePress](https://vuepress.vuejs.org/)** - Another static site generator built on Vue with a focus on documentation sites. VuePress is maintained by the core Vue.js team and used for Vue's own documentation. It aims to be simple and approachable for newcomers, whilst being idiomatic and true to Vue's way of working.

In addition to the specialised static site generators, you could also opt to built a website directly with one of the popular frontend frameworks. By layering on server-side rendering you can ensure your content is rendered on the server and not in the user's browser.

- **[React](https://reactjs.org/)** - The most popular frontend framework of the time, from the Facebook team.
- **[Vue](https://vuejs.org/)** - A very popular independent alternative to React with it's own unique philosophy.
- **[Angular](https://angular.io/)** - One of the earlier frameworks from Google, less popular these days but still widely used.

## How do I get started? {#start}

If you're ready to give static sites a go, why not head over to the documentation for one of the tools listed above? Once you're ready to deploy your site you can use Voxis to host it for free. Static sites are deployed in 60 seconds with no credit card required, they're completely free to host, and you can use your own custom domain name.

1. [Create an account](/start) with Voxis.
2. Connect your VCS (GitLab, GitHub or Bitbucket).
3. That's it! Your website is already live.

Yes it really is that simple. Voxis also gives you free TLS (SSL) out of the box, global availability via a battle-tested CDN, and plenty of your time back!
