---
title: 'Build a HTTP API in Node.js with Google Cloud Functions'
draft: false
sponsored: false
author: 'Justin Emery'
featuredImage: './cover.jpg'
featuredImageRenderMode: 'fullwidth'
date: '2020-04-20T12:00:00'
synopsis: "In this post we show step-by-step how to build a HTTP API using Cloud Functions, Google Cloud Platform's serverless Function as a Service offering."
category: 'development'
tags: ['development', 'javascript', 'backend', 'google-cloud']
---

# Introduction

In this post we will show step-by-step how to build a HTTP API using Cloud Functions, a serverless offering from Google. Whilst the official documentation for Google Cloud Functions includes [a "Hello World" tutorial](https://cloud.google.com/functions/docs/tutorials/http) for HTTP Cloud Functions, there are many challenges that you will only encounter once you start writing Cloud Functions with more than a single line of code!

So in this tutorial we will be building a simple but complete API. It will offer a number of API endpoints and handle different HTTP methods. After completing this tutorial, you should be comfortable with the basics of implementing and deploying an API in Node.js for the Cloud Functions environment.

> **TL;DR:** The [full source code](https://gitlab.com/voxis.io/learn/inspirational-quotes-api) for this tutorial is available on GitLab, and we recommend you refer to that as you follow along. Not every line of code is reproduced in the tutorial.

# Who is this post for?

Developers who want to learn how to build and deploy an API using Cloud Functions.

You should:

- be familiar with JavaScript and Node.js
- be comfortable using the command line / terminal
- have some understanding of HTTP requests (`GET`, `POST`, etc)

# Our scenario

We will build an API to generate random inspiration quotes. You know the type:

> If you want to turn a vision into reality, you have to give 100% and never stop believing in your dream.
> <cite>Arnold Schwarzenegger</cite>

In order to generate a random quote, we will need a collection of quotes to select from, so our API will also provide functionality to manage a collection of quotes. We will cover the full range of CRUD operations (Create, Read, Update, Delete) that is commonly required for any collection of data. In addition to storing the text of each quote, we will have the ability to save an optional author for the quote.

So we will have API endpoints to:

- Add a quote to the collection (Create)
- List all quotes in the collection (Read)
- Update a quote (Update)
- Remove a quote (Delete)
- Generate a random inspirational quote from the collection

# Intro to Google Cloud Functions

Google Cloud Functions is part of the Google Cloud Platform, which offers a wide variety of cloud computing services. It is the Google equivalent of Amazon Web Services (AWS), Microsoft Azure, or Oracle Cloud.

Cloud Functions is a serverless platform, more specifically it provides _Function as a Service_ (FaaS). Most cloud computing platforms now offer some form of FaaS - perhaps the most well known is AWS Lambda from Amazon.

The basic idea behind FaaS is that stateless functions become the core of the application. Whilst FaaS solutions may allow use of memory for optimisation, for example to avoid loading configuration for every request, that memory cannot be depended on. Instances of the function, each with a separate memory pool, may be spun up or spun down depending on demand. At any one point in time there may 0, 1, or many instances of the function running. This is what makes FaaS "serverless" - it scales without the developer needing to have any awareness of the servers it will run on.

This ability to scale automatically is a huge advantage - however it does mean writing applications in a different way. Rather than one monolithic application, each function runs as a separate instance, and should handle a small, well-defined piece of functionality. And importantly, we cannot rely on storing state in global variables as we might with traditional applications.

Cloud Functions are triggered in response to some event. These events may take many forms, but in this tutorial we are specifically looking at HTTP Cloud Functions, which are triggered in order to response to an incoming HTTP request.

In this tutorial, we will specify the API, build it in Node.js, and test it locally with the help the Functions Framework for Node.js. We'll then configure the project within Google Cloud Platform, and deploy our Cloud Functions.

# Specifying the API

Before we start coding and configuring, it's best practice to specify the API. We will do this without concern for implementation details, so that we know what we are building and have a plan to follow. If we later come across difficulties as we implement the API, we can always adapt the specification a little.

Our API will be be available over HTTP, and follow RESTful principles. There are whole books written about designing RESTful APIs (also known as web services), but in brief, this means we'll be using a range of HTTP methods (GET, POST, PUT, DELETE) to manipulate resources made available at specified URLs (endpoints).

For the purposes of API specification we can ignore the domain and specify only the path. So for example, the full URL for a resource might be `https://api.example.com/quotes/4` but we can shorten this to `/quotes/4`. This allows us to test the API in different environments using different hosts, including `localhost` for local testing.

So let's go ahead and specify the API endpoints and available methods.

We will have the following endpoints in our API:

- `/quotes` - our inspirational quotes collection.
- `/quotes/{quote-id}` - a single inspirational quote, accessed by `quote-id`. In this case, we are specifying a template for a set of URLs.
- `/random` - an endpoint for fetching a random quote

All our endpoints will use JSON format. They will both produce JSON (in the HTTP response body), and consume JSON (in the HTTP request body) where required.

Now let's specify the methods available on these endpoints:

#### `GET /quotes` - Get the collection of all quotes

This will respond with JSON like:

```json
{
  "quotes": [
    {
      "id": 1,
      "text": "The people who are crazy enough to think they can change the world are the ones who do.",
      "author": "Steve Jobs"
    },
    {
      "id": 2,
      "text": "Live as if you were to die tomorrow. Learn as if you were to live forever.",
      "author": "Gandhi"
    }
  ]
}
```

This response format has been designed to be open to future extension: rather than returning an array directly, we return the array as a property of an object. We then have the option to extend the API to return additional metadata in other properties in future.

#### `POST /quotes` - Add a new quote to the collection

This will accept some JSON like:

```json
{
  "text": "Live as if you were to die tomorrow. Learn as if you were to live forever.",
  "author": "Gandhi"
}
```

And also return JSON like:

```json
{
  "id": 2,
  "text": "Live as if you were to die tomorrow. Learn as if you were to live forever.",
  "author": "Gandhi"
}
```

The only difference between request and response is that the server has generated an ID for the quote, and added it to the response.

#### `GET /quotes/{quote-id}` - Get a single quote by ID

The returns the specified quote as JSON, for example:

```json
{
  "id": 2,
  "text": "Live as if you were to die tomorrow. Learn as if you were to live forever.",
  "author": "Gandhi"
}
```

#### `PUT /quotes/{quote-id}` - Update a quote by ID

This updates a quote, for example updating the author property to use full name:

```json
{
  "text": "Live as if you were to die tomorrow. Learn as if you were to live forever.",
  "author": "Mahatma Gandhi"
}
```

Note that in HTTP, `PUT` means that the whole resource should be replaced, not only specific fields. This isn't followed by every API, but we do follow it here. If we wanted to be able update specific fields only, we could add the `PATCH` HTTP method which exists for this purpose.

#### `DELETE /quotes/{quote-id}` - Delete a quote by ID

In this case, there is no request body, since the ID for the quote to delete is included in the URL. We will return the quote that was deleted in the response.

#### `GET /random` - Get a random quote

This endpoint is unique in that it is not part of the CRUD functionality for quotes. However, it uses the same data to generate a quote:

```json
{
  "id": 2,
  "text": "Live as if you were to die tomorrow. Learn as if you were to live forever.",
  "author": "Mahatma Gandhi"
}
```

As we want this to generate a new quote every time, we'll use a HTTP Header in the response to stop the response from being cached by browsers: `Cache-Control: no-cache, no-store`

Now that we have defined what we're building, let's move onto implementation...

# Scaffolding our project for local development

Initially, we will develop and run our Cloud Functions in a local development environment. Each deployment of a Cloud Function can take a couple of minutes, so it is not practical to try out every change live on Google Cloud Platform. We will do some initial configuration (known as "scaffolding") to set up our project and allow Cloud Functions to be served on `localhost` during development.

### Project directory structure

Before we start, let's quickly review all the files in the [completed project](https://gitlab.com/voxis.io/learn/inspirational-quotes-api), so you know where we're heading:

| Path                             | Description                                                                                              |
| -------------------------------- | -------------------------------------------------------------------------------------------------------- |
| ./.eslintrc.js                   | Defines ESLint rules to keep our code tidy (we don't cover this in the tutorial, but it's good practice) |
| ./.gcloudignore                  | Optional, defines files that do not need to be uploaded as part of the Google Cloud deployment           |
| ./.gitignore                     | Defines files which should be ignored by Git                                                             |
| ./LICENSE                        | License file for project                                                                                 |
| ./README.md                      | Installation and usage instructions                                                                      |
| ./deploy.sh                      | Script to deploy our Cloud Functions to Google Cloud Platform                                            |
| ./package-lock.json              | Generated by npm as a record of the precise dependency versions installed                                |
| ./package.json                   | Defined npm dependencies, scripts, and other project meta-data                                           |
| ./node_modules/                  | Holds dependencies installed by npm                                                                      |
| ./src/                           | Our JavaScript source code!                                                                              |
| ./src/index.js                   | The main entry point, containing our Cloud Functions                                                     |
| ./src/InMemoryQuoteDataAccess.js | Defines a Data Access Object for storing quotes                                                          |

### Creating our project

Create a new directory `inspirational-quotes-api`.

First we’ll initialize a package on the command line and fill in the prompts:

```bash
npm init
```

Next we will install the Functions Framework for Node.js:

```bash
npm install @google-cloud/functions-framework
```

This framework is optional, but allows us to test our Cloud Functions in local development.

Let’s create a couple of functions to test!

Within `src/index.js` we create two placeholder functions:

```js
const quotes = (req, res) => {
  res.json({ description: 'Quotes collection' });
};

const random = (req, res) => {
  res.json({ description: 'Random inspirational quote' });
};

module.exports = {
  quotes,
  random,
};
```

For now, each of these functions returns some JSON with a description of the endpoint. Each function needs to be exported so that we can test it.

### Running our cloud functions locally

We can now test the first function:

```bash
npx functions-framework --target=quotes
```

```
Serving function...
Function: quotes
URL: http://localhost:8080/
```

Or the second:

```bash
npx functions-framework --target=random
```

```
Serving function...
Function: random
URL: http://localhost:8080/
```

In either case, we can view the output of the function by opening `http://localhost:8080/` in our web browser:

![Viewing function response at http://localhost:8080/ in the browser](./functions-framework-local-server.png 'Viewing the response from our function in the browser')

You may be wondering how to test multiple functions at once. It is rather inconvenient to have to run a command to test a function, then stop it, before running another command to test a different function. In fact, we’ve come across a limitation of the Functions Framework for Node.js - it only supports testing one function at a time.

However, there is a straightforward workaround for this. We can create an additional function for local development purposes, to act as a _front controller_ which will handle routing for all requests to the appropriate function. We’ll only use this for local development (we’ll look at how functions are routing on Google Cloud later).

Add the following function:

```js
const index = async (req, res) => {
  if (req.path.startsWith('/quotes')) {
    await quotes(req, res);
  } else if (req.path.startsWith('/random')) {
    await random(req, res);
  } else {
    res.send('No function is defined at this path');
  }
};
```

This inspects the path of the request, and forwards the request to the appropriate function.

We add the `index` function to `module.exports`, then we can run it:

```bash
npx functions-framework --target=index
```

```
Serving function...
Function: index
URL: http://localhost:8080/
```

Now we can test both our functions in the browser by visiting `http://localhost:8080/quotes` and `http://localhost:8080/random`.

![Viewing function response at http://localhost:8080/random in the browser](./functions-framework-with-front-controller.png 'Viewing a response from our function at a specific path, using our simple front controller')

If we visit `http://localhost:8080/` without a path, we'll see an error "No function is defined at this path". If we wanted to return something more helpful, we could return some HTML with links to the available paths!

Now that we have a command to test all our functions, let’s add it to our package.json as a script for convenient:

```json
"scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "functions-framework --target=index"
},
```

We can now simply run `npm start` to spin up this web server and test all our functions.

### Watching for changes

We still have one frustrating limitation in our local development environment. Whenever we update our functions, we need to stop the test server and run the command again. We can automate this by watching the source files for changes.

To configure this, first add the the `npm-watch` package as a dependency:

```bash
npm install -save-dev npm-watch
```

Then add the following to `scripts` within `package.json`:

```json
"scripts": {
  "watch": "npm-watch start"
},
```

And add a new section to configure npm-watch:

```json
  "watch": {
    "start": "*.js"
  },
```

Now our watch configuration is complete. Whenever we want to start our server and watch changes we can run:

```bash
npm run watch
```

We can confirm this is working by changing the response description returned by one of our functions. Then simply refresh in your browser to see that the function sends the new response message.

# Building the API

Now that we have our project scaffolded for local development, we can add logic to our functions to implement the endpoints according to our specification.

### Storing our inspirational quotes

When it comes to storing our quotes, we are going take a shortcut for the purposes of this tutorial. We will not be storing our quotes collection to persistent storage (eg. some kind of database). Instead, we will store them in memory.

This means **all our data will be lost whenever the program is restarted**. What's more, since we are working with a serverless function, our program is restarted more often than a traditional server-based deployment. At any point in time, there may be zero, one, or many instances of our functions running at one time.

So an in-memory store is **not at all suitable** for a real application. The _only_ reason we are using it is that allows us to get up and running quickly. We can test the logic of our functions without any concern for writing more complex database access code.

We will implement our in-memory store using a simple class. Later, we could implement a class with all the same methods that provides real persistent data storage.

```js
class InMemoryQuoteDataAccess {
  constructor() {
    this.quotes = [];
    this.nextId = 1;
  }
}
```

In the class constructor, we create the data structure we use to store quotes (in memory). This is simply an `Array`, plus we store the next ID that we can assign when a new quote is added.

We then add various methods for CRUD (Create, Read, Update, Delete), plus a method to select a random quote. For brevity, I'll just cover one of those methods, to create a new quote:

```js
async addQuote(text, author) {
  const newQuote = {
    id: this.nextId,
    text,
    author,
  };
  this.quotes.push(newQuote);
  this.nextId += 1;
  return newQuote;
}
```

The method first creates a new object using the provided `title` and `author` parameters, plus an ID from the `nextId` property. It then adds the quote to the array using `.push(newQuote)`. It increments `nextId` so that we will have a new ID next time. Finally it returns the quote object.

Notice that the method uses the `async` keyword. Although this in-memory implementation will return synchronously (immediately), a real persistent database implementation would not. We want this implementation to look the same as a real implementation so that we could easily swap it out later.

The other methods manipulate the `quotes` array in a similar fashion - see [the source code](https://gitlab.com/voxis.io/learn/inspirational-quotes-api/blob/master/src/InMemoryQuoteDataAccess.js) for details. In total there are less than 20 lines of logic in this class - a real database implementation would be more complex and require more configuration to test.

### Implementing the API endpoints

Now that we have a storage class to work with, we can implement the API endpoints. We'll be processing HTTP requests, performing some actions using `InMemoryQuoteDataAccess`, and then returning a HTTP response.

To start off, we'll edit `src/index.js` to import and instantiate `InMemoryQuoteDataAccess` our data access object:

```js
const InMemoryQuoteDataAccess = require('./InMemoryQuoteDataAccess');
const quoteDataAccess = new InMemoryQuoteDataAccess();
```

##### Implementing the `random` function

Now we can implement our Cloud Functions. Let's start with the `random` function, which is simpler because it will only handle a single endpoint using a single HTTP method. Earlier, we added a stub for this function:

```js
const random = (req, res) => {
  res.json({ description: 'Random inspirational quote' });
};
```

You may be wondering about the `req` and `res` objects that are passed to the Cloud Function. These represent the incoming HTTP request and outgoing HTTP response. They are based on the [Request](https://expressjs.com/en/4x/api.html#req) and [Response](https://expressjs.com/en/4x/api.html#res) objects in the popular Express framework for Node.js. The Cloud Functions implementation is different to Express, but all the methods and properties available in the Express API docs are also available to Cloud Functions.

Now we'll replace the body of the function with the real implementation:

```js
const random = async (req, res) => {
  // Ensure result is never cached so that a new random quote is always generated:
  res.setHeader('Cache-Control', 'no-cache, no-store');

  const randomQuote = await quoteDataAccess.selectRandomQuote();
  if (randomQuote) {
    res.json(randomQuote);
  } else {
    res.status(404).json({ error: 'No quotes available' });
  }
};
```

To start with, we set the `Cache-Control` header - we never want our random quotes to be cached by a web browser or any other software such as proxies.

Then we use our data access object (`quoteDataAccess`) to select a random quote. If no quotes are yet being stored, this will not return a quote, so we instead respond with a 404. Otherwise we respond with the quote, in JSON format.

Cloud functions respond to every HTTP method, whether `GET`, `POST` or any other. In this implementation, we did not check the HTTP method, so the same response would be sent for `GET`, `POST`, or even `DELETE`. In a more robust implementation, we could check for this using the `req.method` property. We'll see how to do this next.

##### Implementing the `quotes` function

Our `quotes` Cloud Function will be substantially more complex, because the `/quotes` resource responds differently to different HTTP methods, and also has sub-resources (each individual quote such as `/quotes/1`).

This Cloud Function will therefore need to "route" the request depending on the path and HTTP method. We'll keep the routing code within the function itself, and call other functions, which we'll write, for each particular path and HTTP method.

We'll start with the routing for the collection resource (at `/quotes`):

```js
const quotes = async (req, res) => {
  if (req.path === '/quotes' || req.path === '/') {
    if (req.method === 'GET') {
      await getQuotesCollection(req, res);
    } else if (req.method === 'POST') {
      await postNewQuote(req, res);
    } else {
      res.status(405).json({ error: 'Method Not Allowed' });
    }
  }
```

First we check if the path matches `/quotes` or `/`. When deployed to Google Cloud Platform, the path will simply be `/` as the base path that function is deployed to is omitted.

Next we check the HTTP method. If the method is `GET`, we'll call the `getQuotesCollection` function, and if `POST`, we'll call `postNewQuote`. We'll write both of these functions shortly. These will not be "Cloud Functions", just standard JavaScript functions (because we won't export them and deploy them separately).

If any other unsupported HTTP method is used, we return a [405 status code](https://httpstatuscodes.dev/405), which is the standard response for this scenario in HTTP.

The second half of our function will handle the individual quotes in the collection:

```js
  } else {
    const matches = req.path.match(/(\/quotes)?\/([0-9]+)/);
    if (matches) {
      const quoteId = parseInt(matches[2], 10);
      const quote = await quoteDataAccess.getQuote(quoteId);
      if (!quote) {
        res.status(404).json({ error: 'Quote Not Found' });
      } else if (req.method === 'GET') {
        await getQuote(quote, req, res);
      } else if (req.method === 'PUT') {
        await putQuote(quote, req, res);
      } else if (req.method === 'DELETE') {
        await deleteQuote(quote, req, res);
      } else {
        res.status(405).json({ error: 'Method Not Allowed' });
      }
    } else {
      res.status(404).json({ error: 'Not Found', path: req.path });
    }
  }
};
```

We first check if the path contains a number which could be an ID for a quote (if not, respond with a 404 status code). We extract that number and convert from string to an `integer` using `parseInt`.

Next we check if a quote exists with that ID, using `quoteDataAccess.getQuote`. If that ID does not exist (because it was not created or has been deleted), we respond with a 404. Otherwise we continue to check the HTTP method.

Depending on whether the HTTP method is `GET`, `PUT`, or `DELETE`, we call `getQuote`, `putQuote`, or `deleteQuote` respectively. We'll add these functions soon - for each of them, we'll pass the `quote` in addition to the `req` and `res` objects, to save fetching it again. Again, if an unsupported HTTP method was used, we respond with the standard 405 status code.

Now our routing is complete, let's implement the functions to handle particular routes and methods. Each of these will use our data access object.

We implement a simple function to return all the quotes in response to get `GET /quotes`:

```js
const getQuotesCollection = async (req, res) => {
  res.json({
    quotes: await quoteDataAccess.getQuotes(),
  });
};
```

We implement a function to create a new quote in response to `POST /quotes`:

```js
const postNewQuote = async (req, res) => {
  const { text, author } = req.body;
  if (typeof text !== 'string') {
    res.status(400).json({ error: 'Invalid request' });
  } else {
    const newQuote = await quoteDataAccess.addQuote(text, author);
    res.json(newQuote);
  }
};
```

This includes some basic validation, to ensure the most essential field (the quote `text` string) has been provided. If this was not provided, we respond with a [400 status code](https://httpstatuscodes.dev/400), which indicates the request was unsuccessful due to being invalid.

Next we implement the individual quote resources (`/quotes/{quote-id}`). We start with `GET`:

```js
const getQuote = async (quote, req, res) => {
  res.json(quote);
};
```

This simply returns the quote, formatted as JSON. We already checked the quote exists as part of our routing code.

We implement updating a quote (`PUT`):

```js
const putQuote = async (quote, req, res) => {
  const { text, author } = req.body;
  const updatedQuote = await quoteDataAccess.updateQuote(
    quote.id,
    text,
    author,
  );
  res.json(updatedQuote);
};
```

This simply passes on the parameters of the response body to the data access object. A more robust implementation could include some validation of the parameters.

Finally, we implement deleting a quote (`DELETE`):

```js
const deleteQuote = async (quote, req, res) => {
  const deletedQuote = await quoteDataAccess.removeQuote(quote.id);
  res.json(deletedQuote);
};
```

We simply call `quoteDataAccess.removeQuote` and return the removed quote as JSON.

Now our implementation is complete, we will try it out.

# Testing locally with Postman

It is convenient to test our Cloud Functions in our local environment before we deploy to Google Cloud Platform, since each deployment takes some time. We can fix any obvious bugs before further testing in the real Google Cloud Functions environment.

We start the Cloud Functions locally with `npm run watch` which we configured earlier. This starts a web server, making our Cloud Functions available at `http://localhost:8080`.

I will demonstrate testing the API using the [Postman API client](https://www.postman.com/product/api-client). There are many HTTP clients available, but Postman is a good choice as it's fully functional, and freely available on all major desktop platforms.

To test one of our endpoints in Postman, we set the HTTP method, the URL, and where relevant we can also specify a request body.

First, let's try to generate a random quote with method `GET` and URL `http://localhost:8080/random`:

![GET http://localhost:8080/random returning 404 Not Found response](./postman-get-request-random-not-found.png 'Testing GET /random with Postman')

After hitting send, we get a 404 response, because we don't yet have any quotes.

So let's open a new tab and add some quotes, with method `POST` and URL `http://localhost:8080/quotes`. In this case, we also need to send a request body including the parameters for the quote we are adding. We can do this via JSON, by setting the `Content-Type` HTTP Header to `application/json` and attaching a JSON body like:

```json
{
  "text": "Live as if you were to die tomorrow. Learn as if you were to live forever.",
  "author": "Gandhi"
}
```

Postman gives us a little assistance in sending JSON, including syntax highlighting for a JSON body, and automatically setting the `Content-Type: application/json` HTTP Header (so long as it is not overridden by another header).

However, Postman has even better support the `x-www-form-urlencoded` format. This may sound like some obscure format, but it is in fact the default format for HTML forms. In Postman, we can specify keys and values to send in this format. But will our Cloud Function accept this?

The answer is yes, unlike the Express framework, Cloud Functions handles multiple body formats without configuration. As the documentation states:

> Cloud Functions automatically reads the request body, so you will always receive the body of a request independent of the content type.

JSON and form encoded are supported, along with plain text (but that wouldn't work here as it would give us a string instead of an object with parameters). Other formats, like XML, you would need to parse yourself.

So let's submit a new quote as `x-www-form-urlencoded`:

![POST http://localhost:8080/quotes with x-www-form-urlencoded](./postman-post-request-add-quote.png 'Testing POST /quotes with Postman and an x-www-form-urlencoded request body')

You'll see the quote object returned (as JSON, below) now has an ID assigned.

We can add a few quotes in this way, and you'll then be able to see them when fetching to whole collection:

![GET http://localhost:8080/quotes returning a list of quotes](./postman-get-request-quotes-collection.png 'Testing GET /quotes with Postman')

You will notice all the quotes I submitted are the same! There is no attempt by the API to detect duplicates... perhaps that could be improved in future!

We can also now fetch a random quote:

![GET http://localhost:8080/random returning a random quote](./postman-get-request-random-success.png 'Testing GET /quotes with Postman')

We can update a quote with `PUT`:

![PUT http://localhost:8080/quotes/1 updating a quote](./postman-put-request-update-quote.png 'Testing PUT /quotes/1 with Postman')

And delete a quote:

![DELETE http://localhost:8080/quotes/2 removing a quote](./postman-delete-request-remove-quote.png 'Testing DELETE /quotes/2 with Postman')

If you restart the local server (which happens whenever you make a code change, if you are using `watch`), you'll see all the quotes are lost:

![GET http://localhost:8080/quotes returning an empty list](./postman-get-request-quotes-none.png 'Testing GET /quotes with Postman after a server restart')

This is because we are using the in-memory storage. We'd need to write an implementation with persistence to retain the data, but the in-memory storage is enough to do some basic testing to show that the logic of our API is working.

Now that our Cloud Functions are working in our local development environment, let's deploy them to Google Cloud Platform!

# Setting up our project in Google Cloud Platform

In order to use Google Cloud Functions you need to create and configure a project in Google Cloud Platform. You will need to create a project, enable billing, and enable Cloud Functions for that project. To do this, it's best to review the instructions in the [official quickstart](https://cloud.google.com/functions/docs/first-nodejs#creating_a_gcp_project_using_cloud_sdk) as these aspects of the platform could change at any time. Although you need to enable billing, at the time of writing, Google Cloud Functions has a free tier allowing 2 million function invocations, which should provide more than enough resources to follow this tutorial. However, prices are subject to change so you should [check them yourself](https://cloud.google.com/functions/pricing) to be sure.

When you create a project, a project ID will be created. You can accept a random one, or change it to an ID of your choice. A random project ID will look something like `spry-surf-271912`. We will use project ID this later when deploying our Cloud Functions.

![Google Cloud Platform - Create Project Screen](./google-cloud-platform-create-project.png 'Creating a new project in Google Cloud Platform')

In addition to creating and configuring a project you also need to [install the Google Cloud SDK](https://cloud.google.com/sdk/install), so that you have the `gcloud` command which we will use for deployment.

### Deploying Google Cloud Functions

To deploy our Cloud Functions, first we need to select the right project.

On the command line, you can check the current project (if any) with:

```bash
gcloud config get-value project
```

You can list all your projects with:

```bash
gcloud projects list
```

You should see the project you created earlier. You can then set the project with:

```bash
gcloud config set project [PROJECT_ID]
```

Where `[PROJECT_ID]` is the ID of your project.

You can now deploy the `random` function with:

```bash
gcloud functions deploy random --runtime nodejs10 --trigger-http --allow-unauthenticated
```

The `gcloud` command looks for a `random` function that was exported in the main source file (`src/index.js`), and will deploy that to a path `/random` within the selected Cloud Functions project.

We can also deploy the `quotes` function with:

```bash
gcloud functions deploy quotes --runtime nodejs10 --trigger-http --allow-unauthenticated
```

We can put these together into a script `deploy.sh`:

```shell
#!/bin/bash

# Deploys each of the functions.
gcloud functions deploy quotes --runtime nodejs10 --trigger-http --allow-unauthenticated
gcloud functions deploy random --runtime nodejs10 --trigger-http --allow-unauthenticated
```

Then simply run `./deploy.sh` to deploy every Cloud Function. It can take up to 2 minutes per Cloud Function.

Notice that we used the flag `--runtime nodejs10`. This tells Cloud Functions to use the Node.js v10 runtime. At the time of writing, this runtime is in beta, but since it has been in beta for almost a year, it should be fairly stable. The alternative would be to use the older v8 runtime - this is now an old release of Node.js, but it unlike the beta v10 runtime, it does qualify for support from Google under their SLA.

### Testing and debugging on Google Cloud Platform

When you deploy a Cloud Function, you will be given a URL like `https://us-central1-spry-surf-271912.cloudfunctions.net/quotes` at which it has been deployed. We can test this in Postman, in the same way that we tested during local development.

If you encounter bugs when running Cloud Functions, you can add `console.log` statements to help diagnose the issue, and redeploy the functions. You will be able to read these logs via [Logging](https://console.cloud.google.com/logs/) in the Google Cloud Platform Console in your browser, or on the command line using `gcloud functions logs read`. Even if you have not added any `console.log` statements, the logs will show recent function executions, including the number of milliseconds they took to execute, and the status code.

When you test the `/random` endpoint on Google Cloud Platform, you will notice that it always returns a 404. This demonstrates the fact that these functions are running as distinct instances, with separate memory pools. Since we have only implemented in-memory storage of quotes, our functions cannot work together using the same data. In order for the data to be re-used across different Cloud Functions, we'll need to implement some persistent storage.

Similarly, you will notice that any quotes you add disappear after some time. This is because Google will not keep the instance running when some period of time has passed since the last request. Again, the solution is to implement persistent storage.

# Conclusion & next steps

In this tutorial we have seen how to design and build a HTTP API using serverless functions, and deploy it on Google Cloud Functions.

We went much further than the official documentation, but there's still a number of limitations we would look to address for a real API:

- As mentioned numerous times in the tutorial, we really do need to implement some persistent storage solution.
- If we want to use the API from a frontend, we'll need to implement CORS to allow the frontend to be able to access it in the web browser.
- In a real API, we would want some form of authentication. For example, to determine who can add, update, and delete quotes.
- Typically we'd like to use our own custom domain such as `api.example.com` instead of `us-central1-spry-surf-271912.cloudfunctions.net`. One reason for this is so that we would be free to change our implementation (we may want to switch to a different platform in future).

The [full source code](https://gitlab.com/voxis.io/learn/inspirational-quotes-api) for this tutorial is available on GitLab.
