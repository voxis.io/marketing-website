---
title: '5 VS Code Extensions For Team Productivity'
draft: false
sponsored: false
author: 'Josh Cole'
featuredImage: './cover.png'
featuredImageRenderMode: 'fullwidth'
date: '2020-05-01T12:00:00'
synopsis: 'If a craftsman wants to do good work, he must first sharpen his tools. Productivity relies on teams not just using tools, but using the right tools for the job. This post explores five of the VS Code extensions that I encourage my team to use with the aim of boosting their productivity.'
category: 'leadership'
tags: ['productivity', 'leadership', 'management', 'teams', 'vscode']
---

Is your team as productive as it could be? Maybe it's close, although I'd put money on there being room for improvement in there somewhere.

Now, IDE extensions aren't going to solve major problems by any stretch of the imagination, but they will help you eek out that little bit more productivity without asking anything more of the team.

And that's what productivity is often about; lots of small tweaks and adjustments that allow humans to do what they do best (craftsmanship and creative work) by leveraging better tools.

As Confucius said;

> "If a craftsman wants to do good work, he must first sharpen his tools."

The following are five VS Code extensions that I encourage every engineer on my team to utilise. Each of them has a small but valuable incremental contribution to productivity, hence the recommendation.

# 5 - [Todo Tree](https://marketplace.visualstudio.com/items?itemName=gruntfuggly.todo-tree)

### Description

Todo Tree quickly searches your workspace for comment tags like `TODO`, `HACK`, and `FIXME`, displaying them in a tree view in the sidebar.

Clicking a TODO within the tree will open the file and navigate to the correct line, whilst the comments themselves will be highlighted so they're visually annoying and less likely to get missed. This can be configured in settings.

![Todo Tree](./todo-tree.png)

### Why

I generally don't like to see "todo" comments like this left in code, particularly code that's supposedly "finished" and/or going into production.

```js
// TODO: implement new feature
// FIXME: this is a known issue I haven't fixed yet
// HACK: revisit this later when we have more time
```

That being said, life is messy and it's not always possible to avoid such things. Developers are bound to write these comments and they sometimes get accidentally committed to source control, it happens. 🤷‍♂️

And there are times when the comments have actual value such as when pushing code at the end of the workday to act as reminders for tomorrow, or if a developer is off sick and someone else needs to pick up where they left off.

I don't tend to encourage these comments, but given the fact they will crop up somewhere we might as well have a good tool for finding and dealing with them. This is especially useful when code is changing quickly in the early stages of a new project.

# 4 - [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)

### Description

GitLens provides a more visually friendly UI to work with Git repositories with shortcuts for many common uses of the tool.

It helps with visualising code authorship through surfacing useful information directly in the code editor, and via tools for git blame, history navigation, and code comparisons.

The extension also comes with lots of customisation options to adjust its behaviour to your individual tastes.

![GitLens](./gitlens-1.png)

![GitLens](./gitlens-2.png)

### Why

I've used Git for every project for many years as I suspect you have too. Git is a complex tool to master fully and engineers in your team will be at different stages in their learning.

That's where this extension comes in. Simplification of a complex tool is a valuable endeavour. This extension also means developers don't have to switch context between their IDE, the command line and your VCS provider.

Since Microsoft's purchase of GitHub the Git integration in VS Code has come on leaps and bounds. Perhaps in the future this extension won't be necessary at all.

For now, GitLens enables some valuable insights into the evolution of your codebase without needing to resort to the command line or the online tools of your VCS.

# 3 - [Editorconfig](https://marketplace.visualstudio.com/items?itemName=editorconfig.editorconfig)

### Description

EditorConfig helps maintain consistent coding styles for multiple developers working on the same project across various editors and IDEs.

This extension reads the `.editorconfig` committed to your project's repository and automatically applies the settings within when you switch to the project. Full definition of the options available can be found here: https://editorconfig.org

![EditorConfig](./editorconfig.png)

### Why

For me, consistency is an important principle of high quality engineering. Consistency reduces mistakes because it reduces the cognitive load on those reading or writing the code, and it quickly becomes implicitly understood how something should or could work without needing to deep dive every time a similar problem crops up.

Editor Config has been around for quite some time and it's one of those very simple tools that ensures a modicum of consistency between developers. I like to keep git diffs as clean as possible, and seeing changes to things like trailing whitespace, newlines, indentation characters, and so on quickly becomes painful.

The extension automatically sets the correct IDE settings for each project you switch into based on an `.editorconfig` file in the repository root.

Make a decision on the style to follow and be done with it. All developers with this extension installed will then be writing code in the same style, keeping git diffs clear of whitespace noise.

# 2 - [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)

### Description

Code Spell Checker is a basic spell checker that works well with camelCase code. The goal of the extension is to catch common spelling errors whilst keeping the number of false positives low.

It's not designed to be as comprehensive as a word processor spell checker but it does well enough to prevent silly mistakes.

The extension also supports many more languages than US English which is a boon for those of us who work in other variants of English or entirely different languages.

![Code Spell Checker](./code-spell-checker.png)

### Why

Spelling mistakes are common. That might not be a dealbreaker for passive code such as comments, documentation, strings containing user notifications, and so on, but it can have a huge knock-on effect and mistakes tend to live forever.

See the case of the HTTP Referrer header that was [misspelled](https://en.wikipedia.org/wiki/HTTP_referer) in the original specification over 20 years ago and continues to live on forever more.

Incorrectly named functions, variables and so on tend to be hard to change once baked in, and may cause confusion and frustration for every developer that ever needs to read or use that code in the future.

Mistakes like this tend to take more energy to correct than you might want to expend. So save yourself the eternal embarrassment and get it right first time.

# 1 - [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

### Description

Prettier is an opinionated code formatter. It enforces a consistent style by parsing your code and re-printing it with its own rules that take the maximum line length into account, wrapping code when necessary.

Prettier supports: JavaScript, TypeScript, Flow, JSX, JSON, CSS, SCSS, Less, HTML, Vue, Angular, GraphQL, Markdown, and YAML. Full definition of the options available can be found here: https://prettier.io

![Prettier](./prettier.png)

### Why

Now I know not all engineers will be writing code in languages that Prettier supports, but for those developers who are it's a huge timesaver. Similar to the Editor Config extension above, Prettier automates work that is valuable but quickly becomes a huge time sink such as formatting and organising code to make it consistent and readable.

Prettier is useful for the languages it supports because those languages are so flexible they can be written in many styles. Not to mention that most of them don't have official style guides or formatting tools, unlike more modern languages.

I can't count how many hours I've seen wasted by developers arguing over whether semicolons are necessary in JavaScript, or whether indentation should be tabs or spaces, or reading git diffs that are mired with stylistic changes.

Whilst there are sensible answers to all these crinkles, I've seen these debates continue to rage unnecessarily in every team. Prettier puts an end to all that by enforcing an opinionated style on all developers with only a small amount of flexibility to change its behaviour.

What I love about Prettier is that code styling is applied automatically on save so I don't need to even think about how tidy I'm being, Prettier does that for me. Plus one for productivity!
