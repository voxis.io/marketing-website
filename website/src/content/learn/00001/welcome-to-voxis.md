---
title: 'Welcome to Voxis'
draft: false
sponsored: false
author: 'Josh Cole'
featuredImage: './cover.png'
date: '2020-03-21T10:00:00'
synopsis: "Voxis is an open source toolkit for shipping cloud software quicker and cheaper than you could in the past. Our ethos is simple. We want to unleash developer creativity and accelerate software innovation. Today we're excited to launch our new website and preview program, allowing a select number of beta testers to try out Voxis.io for themselves."
category: 'voxis'
tags: ['voxis', 'beta']
---

# Open source toolkit

Voxis is an open source toolkit for shipping cloud software quicker and cheaper than you could in the past. It works with all the major cloud vendors; AWS, Azure and Google Cloud, and it can even be used for on-premise deployments. Forget all those custom deployment scripts, half-baked testing strategies, and boilerplate code - Voxis automates all the work you don't want to do so you can focus on just the tasks that create value for your customers.

Our ethos is simple. We want to unleash developer creativity and accelerate software innovation. These days, deployment pipelines and cloud infrastructure are table stakes for cloud software and whilst they're absolutely necessary, they aren't value-generating activities. That's where Voxis comes in. Voxis combines battle-tested leading open source software such as Kubernetes, Envoy Proxy, Docker and others with industry best practice to help you architect, deliver, and maintain cloud software in record time.

By automating everything repetitive and allowing you to focus all your energy on the creative work that _does_ generate value, you can get so much more done with the same resources. In short, Voxis gives developers super powers.

# What can Voxis do?

##### Deploy to a cloud vendor of your choice

<img src="../../../images/vector/foreground/features/zero-config-deployments.svg" class="right">

Voxis is designed to deploy your web apps, APIs and applications anywhere, to any cloud vendor or even on-premise if you need to. Voxis can do this because it manages the entire lifecycle of your applications, from the CI/CD pipeline to deployment, from application resource allocation to auto-scaling, and much more. Voxis is for teams that just want to build great software for their users without the pain and expense of worrying about infrastructure.

##### Deploy to Voxis.io

If you want the absolute minimum work to get your apps and websites online then look no further than Voxis.io. By choosing to host your software directly with the core team you can support a key open source project whilst getting the best expertise, the full power of the Voxis toolkit, and the benefit of our economies of scale and low cost.

##### Execute CI/CD pipelines in seconds

Configuring CI/CD pipelines is always a pain and they're never perfect. And even once you have them up and running they can be slow and error prone if not done right. The energy you invest here can be much better spent in other areas, so why not have the work done for you? The Voxis toolkit comes with a fully-featured CI/CD pipeline out of the box so that's one chore eliminated. Your applications will be built and deployed in seconds, not minutes, and all with zero configuration necessary.

##### Improve software quality

<img src="../../../images/vector/foreground/features/automated-testing.svg" class="right">

Voxis helps you raise the quality bar with its automated quality control checks. This includes executing your test suite, checking for code coverage, performing other code quality checks, vulnerability scanning, and automatically rolling back bad releases. You'll also get multiple environments out of the box, so if you want a staging site to try out changes, or you even want an environment for each developer, you can do as you please!

##### Host static websites for free

The days of paying for an entire server instance, VM or container to host your website are gone. Why should you pay to host a few lightweight HTML files and some media when you have bigger fish to fry? Voxis makes serving up static websites and web apps so efficient that the cost is effectively reduced to zero. You'll never to pay to host static sites on Voxis.io and when deployed to your cloud vendor of choice Voxis makes use of super low-cost serverless technology.

##### Go global at the push of a button

<img src="../../../images/vector/foreground/features/global-cdn.svg" class="right">

These days we often like to make our software available to the huge global market. This can often take a lot of work preparing software for internationalisation, configuring multiple regions, setting up a CDN, and so on. Voxis reduces this burdeon by automatically managing regions and ensuring assets are delivered to the edge where they're most needed for fast access by end-users. Voxis determines which assets and code are most needed in which locations, and ensures your software is always delivering the highest-quality experience possible to your users.

##### Lifecycle management

Voxis doesn't stop at deployment, it also manages the full lifecycle of your applications. Voxis manages the resource allocation for your apps, ensuring they have the resources they need to execute without costing you the Earth, whilst smoothly handling spikes in demand with smart auto-scaling. Automatic TLS (SSL) renewal and multi-region redundancy are two more things you can take off your to-do list as they're provided out of the box.

##### Monitor and improve

<img src="../../../images/vector/foreground/features/dashboard.svg" class="right">

Typically you need several other bits of kit to monitor and manage your applications once they're live. Voxis comes with metrics and log aggregation so you don't need to buy and manage other software for such core tasks. In addition to this, our enterprise offering includes a customisable operations dashboard giving you top-line information and status alerts in realtime.

# Beta testers

Today we're excited to launch our new website and preview program, allowing a select number of beta testers to try out Voxis.io for themselves and help us build Voxis into the open source tool you need to deliver great software to your customers, efficiently and effectively. The beta test list is first come first served, so please enter your email address on the waitlist page to sign up: [Voxis Preview](/preview).
