---
title: 'Introduction to Finite State Machines with React'
draft: false
sponsored: false
author: 'Kameron Tanseli'
featuredImage: './cover.jpg'
featuredImageRenderMode: 'cover'
date: '2020-04-04T12:00:00'
synopsis: 'Finite State Machines (FSM) allow us to declare beforehand how the state of our application will transition as we use it, and is particularly useful to describing complex UI state in frameworks such as React.'
category: 'development'
tags: ['frontend', 'react', 'state', 'development', 'javascript']
---

> A **finite state machine** (sometimes called a finite state automaton) is a computation model that can be implemented with hardware or software and can be used to simulate [sequential logic](https://brilliant.org/wiki/logic-gates/) and some computer programs. - [brilliant.org](https://brilliant.org/wiki/finite-state-machines/)

From a top-down level, Finite State Machines (FSM) allow us to declare beforehand how the state of our application will transition as we use it. You will normally see Finite State Machines expressed through diagrams like this:

![Courtesy of brilliant.org](./state-machine.png)

To understand FSM let's break down this diagram:

1. We start in the state "Locked", as signified by the bold-pointed arrow
   1. When "Pushed" is triggered in the "Locked" state we stay "Locked"
   2. When "Coin" is triggered we transition to the "Unlocked" state
2. When in the "Unlocked" state we can only trigger "Push" to get back to the "Locked" state.
   1. When "Coin" is triggered in the "Unlocked" state we remain "Unlocked".

As you can see expressing your state beforehand is quite refreshing and allows us to breakdown complex UI logic.

# Why we need Finite State Machines

Imagine you have a form that takes some arbitrary input and sends it off to a server somewhere on the Internet. For a simple form we have 6 different scenarios that can happen:

1. **Idle / Invalid**, Nothing has happened and the form is blank
2. **Submission**, The user has typed something and clicked "Submit"
3. **Validation**, The form validates the user's input
4. **Loading**, The form tries to send the data
5. **Error**, The form fails to send the data
6. **Done**, The form has finished sending the data

**The problem comes when these different scenarios start overlapping with each other**. For example, it would be frustrating to the user if the form was in the "Loading" state while the user tries to submit the form again.

These mistakes with state occur more than you think. There are about 16 different combinations of state just for this simple form and managing all of that is down to the programmer keeping track of the state:

```js
const [isLoading, setIsLoading] = useState(false);
const [isError, setIsError] = useState(false);
const [isDone, setIsDone] = useState(false);
const [isValid, setIsValid] = useState(false);
```

# How we can use them with React

One of my favorite libraries to use with React is called [Robot](https://thisrobot.life/), the 1KB functional library for creating Finite State Machines.

```bash{prompt}
npm install robot3 --save
```

With Robot we can declaratively define our states and their transitions beforehand and use it within our components. Robot makes creating the initial machines easy through their composition-based approach:

```js
import { createMachine, state, transition } from 'robot3';

const machine = createMachine('inactive', {
  // when state is 'inactive'
  inactive: state(
    // when 'toggle' is triggered transition to 'active'
    transition('toggle', 'active'),
  ),
  // when state is 'active'
  active: state(
    // when 'toggle' is triggered transition to 'inactive'
    transition('toggle', 'inactive'),
  ),
});
```

Above is the machine. The machine takes an initial state, `js›"inactive"`, and an object with keys representing the name of the state and a function called `js›state()` that defines what transitions can occur when inside that state.

In the example above this would work as follows:

1. The machine is currently in the `js›"inactive"` state
2. The machine is sent `js›"toggle"`
3. The machine is now in the `js›"active"`
4. The machine is sent `js›"toggle"`
5. The machine is now in the `js›"inactive"` state

To integrate the machine with React there is the [React-Robot](https://thisrobot.life/integrations/react-robot.html) package that exports a hook for using the machines inside your component:

```jsx
function App() {
  const [current, send] = useMachine(machine);
  // current is our machine
  // - it contains context (the values we pass to the machine)
  // - it contains "name" (the current state it is in)

  // send is a function for triggering transitions

  return (
    <>
      // state: 'inactive'
      <div>State: {current.name}</div>
      // Send "toggle" to the machine on click
      <button onClick={() => send('toggle')}>Toggle</button>
    </>
  );
}
```

# Storing state within a Finite State Machine

Simple examples like the above are good at highlighting the basic functionality of Finite State Machines but their true power really shines when things get complicated.

For example, let's consider the above example of a form and its 6 different states:

1. **Idle / Invalid**, Nothing has happend and the form is blank
2. **Submission**, The user has typed something and clicked "Submit"
3. **Validation**, The form validates the user's input
4. **Loading**, The form tries to send the data
5. **Error**, The form fails to send the data
6. **Done**, The form has finished sending the data

Let's start by defining the states in the machine:

```js
import { createMachine, state, transition } from 'robot3';

const machine = createMachine('invalid', {
  invalid: state(),
  validation: state(), // handles submission
  sending: state(), // handles loading
  thankyou: state(), // handles done
  error: state(),
});
```

For the **Idle** state we should **only** be able to transition to `js›"validation"`. This is because the only thing a user can do from this point of time is submit our form:

```js
import { createMachine, state, transition } from 'robot3';

const machine = createMachine('invalid', {
  invalid: state(
    // when "submit" is sent go to "submission"
    transition('submit', 'validation'),
  ),
  validation: state(),
  sending: state(),
  thankyou: state(),
  error: state(),
});
```

Next up is the `js›"submission"` state. When in this state the machine should validate the form's data and decide whether to transition to `js›"sending"` or to send the state back to `js›"invalid"`:

In order to not wait for an event to be triggered and have the validation happen instantly, we can use `js›immediate()` instead of `js›transition()`.

We can also make use of `js›guard()` to pass a validation function to `js›immediate()` that will decide whether or not the machine can progress to `js›"sending"`:

```js
import { createMachine, state, transition, immediate, guard } from 'robot3';

const isAValidEmail = ({ email }) => email !== '';

const machine = createMachine('invalid', {
  invalid: state(transition('submit', 'validation')),
  validation: state(
    // if valid go to sending
    immediate('sending', guard(isAValidEmail)),
    // else back to invalid
    immediate('invalid'),
  ),
  sending: state(),
  thankyou: state(),
  error: state(),
});
```

When in the `js›"sending"` state we should immediately start sending the data to the server. Because handling promises is a special scenario we can use `js›invoke()` which send's the machine `done` or `error` when the promise is resolved or rejected:

```js
import {
  createMachine,
  state,
  transition,
  immediate,
  guard,
  invoke,
} from 'robot3';

const sendToServer = () => {
  return new Promise((r) => {
    setTimeout(() => r(), 2000);
  });
};

const isAValidEmail = ({ email }) => email !== '';

const machine = createMachine('invalid', {
  invalid: state(transition('submit', 'validation')),
  validation: state(
    immediate('sending', guard(isAValidEmail)),
    immediate('invalid'),
  ),
  sending: invoke(
    sendToServer, // call promise
    transition('done', 'thankyou'), // on success
    transition('error', 'error'), // on failure
  ),
  thankyou: state(),
  error: state(),
});
```

The final state of our component should be `js›"thankyou"` so we can leave it blank:

```js
thankyou: state();
```

**Error** however needs, to display a message and transition back to `js›"invalid"`:

```js
import {
  createMachine,
  state,
  transition,
  immediate,
  guard,
  invoke,
} from 'robot3';

const sendToServer = () => {
  return new Promise((r) => {
    setTimeout(() => r(), 2000);
  });
};

// promisify setTimeout
const waitFiveSeconds = () => {
  return new Promise((r) => {
    setTimeout(() => r(), 5000);
  });
};

const isAValidEmail = ({ email }) => email !== '';

const machine = createMachine('invalid', {
  invalid: state(transition('submit', 'validation')),
  validation: state(
    immediate('sending', guard(isAValidEmail)),
    immediate('invalid'),
  ),
  sending: invoke(
    sendToServer,
    transition('done', 'thankyou'),
    transition('error', 'error'),
  ),
  thankyou: state(),
  error: invoke(
    waitFiveSeconds, // keep message for 5 secs
    transition('done', 'invalid'), // go back to invalid
  ),
});
```

Before we can use this with React we need to talk about `context`. You can think of context as the machine's internal values that we can programmatically set using `reduce` much like how a Redux Reducer works:

```js
import {
  createMachine,
  state,
  transition,
  immediate,
  guard,
  invoke,
  reduce,
} from 'robot3';

reduce((ctx, ev) => ({ ...ctx, email: ev.data.email }));
```

In order to get `isAValidEmail` to successfuly trigger we need to somehow set `email` within the machine's context. To do this, we can insert the above line into our `submit` transition:

```js
const machine = createMachine('invalid', {
  invalid: state(
    transition(
      'submit',
      'validation',
      // sets a new context object
      reduce((ctx, ev) => ({
        ...ctx,
        email: ev.data.email,
      })),
    ),
  ),
  validation: state(
    immediate('sending', guard(isAValidEmail)),
    immediate('invalid'),
  ),
  sending: invoke(
    sendToServer,
    transition('done', 'thankyou'),
    transition('error', 'error'),
  ),
  thankyou: state(),
  error: invoke(waitFiveSeconds, transition('done', 'invalid')),
});
```

Now we can create our form component to handle this machine:

```jsx
function ContactUs() {
  const [email, setEmail] = useState('');

  const [current, send] = useMachine(machine, () => ({
    email: '', // sets initial context
  }));

  // when in the invalid state show red borders
  const borderColor = current.name === 'invalid' ? 'red' : 'green';

  const handleSubmit = (e) => {
    e.preventDefault();
    // send "submit" and merge email into the
    // machine's context
    send({ type: 'submit', data: { email } });
  };

  return (
    <div>
      {current.name === 'thankyou' ? (
        <h3>Thank you for contacting us!</h3>
      ) : (
        <form onSubmit={handleSubmit}>
          {current.name === 'sending' && <p>Sending...</p>}
          {current.name === 'error' && <p>Something went wrong :(</p>}
          {current.name === 'invalid' && (
            <p>
              <small>Please provide a valid email</small>
            </p>
          )}
          <input
            type="email"
            name="email"
            value={email}
            onChange={(e) => setEmail(e.currentTarget.value)}
            placeholder="Your Email"
            style={{ borderColor }}
          />
          <button type="submit">Submit</button>
        </form>
      )}
    </div>
  );
}
```

You should end up with an end result like the below, or you can view the [source code repository](https://gitlab.com/voxis.io/learn/state-machine).

<iframe src="https://codesandbox.io/embed/boring-wilson-bn90d?fontsize=14" title="boring-wilson-bn90d" allow="geolocation; microphone; camera; midi; vr; accelerometer; gyroscope; payment; ambient-light-sensor; encrypted-media; usb" style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;" sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>

What makes this so special? Our `ContactUs` component only needs to worry about what state the machine is in (there can only be 1 state at a time) and triggering the machine when the form is submitted.

This means that our `ContactUs` component is free from having to manage sending calls to the server or handling complex state with `js›useState()`.

Another benefit of the `machine` is that it is incredibly easy to test:

```js
import { interpret } from 'robot3';

it('should transition to sending when invalid', () => {
  const service = interpret(machine, () => {});
  const current = service.machine.current;
  expect(current.name).toBe('invalid');

  service.send({
    type: 'submit',
    data: { email: 'james@mail.com' },
  });

  expect(current.name).toBe('sending');
  expect(current.context).toEqual({
    email: 'james@mail.com',
  });
});
```

# Going Forward

Hopefully I've managed to convey the usefulness of Finite State Machines in the React world. If you're interested in reading more about State Machines, I would recommend the links below:

- [Source code](https://gitlab.com/voxis.io/learn/state-machine) - The example code from this post
- [Robot](https://thisrobot.life/) - **1kB** functional library for creating Finite State Machines
- [XState.js](https://xstate.js.org/docs/) - Statecharts XML spec JS library
- [JS State Machine](https://github.com/jakesgordon/javascript-state-machine) - A javascript finite state machine library
- [Smashing Magazine](https://www.smashingmagazine.com/2018/01/rise-state-machines/)
