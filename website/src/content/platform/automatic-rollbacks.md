---
image: ImgFeatureAutomaticRollbacks
title: Automatic rollbacks
subTitle: Broken deployments are automatically rolled back with minimal impact.
weight: 4
---
