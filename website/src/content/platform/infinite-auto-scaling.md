---
image: ImgFeatureInfiniteScaling
title: Infinite auto-scaling
subTitle: A modern serverless stack allows us to scale your code for any workload.
weight: 8
---
