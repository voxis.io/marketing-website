---
image: ImgFeatureAutomatedTesting
title: Automated testing
subTitle: Voxis executes your test suite and performs its own code quality checks.
weight: 7
---
