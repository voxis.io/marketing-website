---
question: Do I need a credit card to try out Voxis?
draft: false
weight: 3
pricingPage: true
lastUpdated: '2020-02-09T14:29:00'
---

No there is no need for a credit card and no payments to make. You can try Voxis for as long as you need without paying a thing.
