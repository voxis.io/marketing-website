---
question: Are static websites free to host?
draft: false
weight: 5
pricingPage: true
lastUpdated: '2020-02-09T15:16:00'
---

Yes, you can host static websites for free on our free plan.
