---
question: What are compute hours?
draft: false
weight: 4
pricingPage: true
lastUpdated: '2020-02-09T15:01:00'
---

We measure resource usage in three ways; compute, disk space, and data transfer. Compute refers to the amount of time Voxis is executing your application code or CI pipelines on a high-spec single-core CPU.
