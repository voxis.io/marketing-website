import Img from 'gatsby-image';
import PropTypes from 'prop-types';
import React from 'react';

import queries from '../../queries';

import styles from './TrustSection.module.scss';

const ClientLogo = ({ data }) => {
  const { frontmatter: { name, logo } } = data;

  return (
    <Img
      fixed={logo.childImageSharp.fixed}
      className={styles.clientLogoWrapper}
      title={name}
      alt={name}
    />
  );
};

const TrustSection = ({ className }) => {
  const clients = queries.clients().slice(0, 10);

  return (
    <section id="trust-section" className={`${styles.section} ${className}`}>
      <h2>We're trusted by leading engineers and businesses</h2>
      <div>
        {clients.map((data, index) => <ClientLogo key={index} data={data} />)}
      </div>
    </section>
  );
};

TrustSection.propTypes = {
  className: PropTypes.string,
};

TrustSection.defaultProps = {
  className: ``,
};

export default TrustSection;
