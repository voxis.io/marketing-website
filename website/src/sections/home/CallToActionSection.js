import PropTypes from 'prop-types';
import React from 'react';

import Button from '../../components/Button';

import styles from './CallToActionSection.module.scss';

const CallToActionSection = ({ className }) => (
  <section className={`${styles.section} ${className}`}>
    <div className={styles.title}>Ready to try Voxis?</div>
    <div className={styles.subTitle}>No credit card required.<br />Deploy your first app in 60 seconds.</div>
    <Button className={styles.getStartedBtn} to="/start" icon="IconRocket">Get Started Free</Button>
  </section>
);

CallToActionSection.propTypes = {
  className: PropTypes.string,
};

CallToActionSection.defaultProps = {
  className: ``,
};

export default CallToActionSection;
