import { Link } from 'gatsby';
import Img from 'gatsby-image';
import moment from 'moment';
import PropTypes from 'prop-types';
import React from 'react';

import Button from '../../components/Button';
import queries from '../../queries';

import styles from './LearnSection.module.scss';

const BlogPostCard = ({ data }) => {
  const { featuredImage } = data.frontmatter;

  return (
    <div className={styles.blogPostCard}>
      <Link to={data.fields.slug} className={styles.featuredImageContainer}>
        {featuredImage && <Img fluid={featuredImage.childImageSharp.fluid} className={styles.featureImageWrapper} objectFit="cover" objectPosition="center" />}
      </Link>
      <div className={styles.category}>{data.frontmatter.category}</div>
      <div className={styles.content}>
        <div className={styles.meta}>
          <div className={styles.date}>{moment(data.frontmatter.date).format(`D MMM, YYYY`)}</div>
          <div className={styles.author}>{data.frontmatter.author}</div>
        </div>
        <div className={styles.copy}>
          <Link to={data.fields.slug} className={styles.title}>{data.frontmatter.title}</Link>
          <div className={styles.synopsis}>{data.frontmatter.synopsis}</div>
        </div>
        <Link to={data.fields.slug} className={styles.readMore}>
          <span>Read More</span>
          <span>➞</span>
        </Link>
      </div>
    </div>
  );
};

const LearnSection = ({ className }) => {
  const blogPosts = queries.blogPosts().slice(0, 3);

  return (
    <section className={`${styles.section} ${className}`}>
      <h3>Learn</h3>
      <div className={styles.flexer}>
        {blogPosts.map((item, index) => <BlogPostCard key={index} data={item} />)}
      </div>
      <div className={styles.buttonRow}>
        <p>Want to keep learning?</p>
        <Button className={styles.button} to="/learn" icon="IconBlog">View the Blog</Button>
      </div>
    </section>
  );
};

LearnSection.propTypes = {
  className: PropTypes.string,
};

LearnSection.defaultProps = {
  className: ``,
};

export default LearnSection;
