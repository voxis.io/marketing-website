import PropTypes from 'prop-types';
import React from 'react';

import UseCaseCard from '../../components/UseCaseCard';
import queries from '../../queries';

import styles from './UseCasesSection.module.scss';

const UseCasesSection = ({ className }) => {
  const useCases = queries.useCases().slice(0, 3);

  return (
    <section className={`${styles.section} ${className}`}>
      <h3>Deploy</h3>
      <div className={styles.container}>
        {useCases.map((data, index) => <UseCaseCard key={index} data={data} />)}
      </div>
    </section>
  );
};

UseCasesSection.propTypes = {
  className: PropTypes.string,
};

UseCasesSection.defaultProps = {
  className: ``,
};

export default UseCasesSection;
