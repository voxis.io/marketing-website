import PropTypes from 'prop-types';
import React from 'react';

import Terminal from '../../components/Terminal';

import styles from './DemoSection.module.scss';

const DemoSection = ({ className }) => {
  const terminalLines = [
    { type: `shell-prompt`, dir: `http-status-codes`, branch: `redesign`, userInput: `vox create` },
    { type: `app-output`, print: `Found ./package.json` },
    { type: `app-output`, print: `App Name: <col:cyan>http-status-codes</cyan>` },
    { type: `app-output`, print: `Git URL: <col:cyan>git@gitlab.com:voxis.io/live-demos/http-status-codes.git</cyan>` },
    { type: `app-prompt`, print: `Create app with these details? [y/n] (y):`, userInput: `y` },
    { type: `app-output`, print: `Creating app...` },
    { type: `app-output`, print: `Done` },
    { type: `app-output`, print: `` },
    { type: `shell-prompt`, dir: `http-status-codes`, branch: `redesign`, userInput: `git push` },
    { type: `app-output`, print: `Total 0 (delta 0), reused 0 (delta 0)` },
    { type: `app-output`, print: `To git@gitlab.com:voxis.io/live-demos/http-status-codes.git` },
    { type: `app-output`, print: `* [new branch]      redesign -> redesign` },
    { type: `app-output`, print: `` },
    { type: `shell-prompt`, dir: `http-status-codes`, branch: `redesign`, userInput: `open https://httpstatuscodes.dev` },
    { type: `shell-prompt`, dir: `http-status-codes`, branch: `redesign`, userInput: `` },
  ];

  return (
    <section className={`${styles.section} ${className}`}>
      <h3>No Fuss Deployments</h3>
      <p><strong>go live</strong> in seconds, globally</p>
      <Terminal className={styles.terminal} windowClassName={styles.terminalWindow} lines={terminalLines} />
    </section>
  );
};

DemoSection.propTypes = {
  className: PropTypes.string,
};

DemoSection.defaultProps = {
  className: ``,
};

export default DemoSection;
