import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';

import AwsLogo from '../../images/vector/foreground/brands/aws.svg';
import AzureLogo from '../../images/vector/foreground/brands/azure.svg';
import Button from '../../components/Button';
import DigitalOceanLogo from '../../images/vector/foreground/brands/digital-ocean.svg';
import FlexBox from '../../components/layout/FlexBox';
import FlexCell from '../../components/layout/FlexCell';
import GoogleCloudLogo from '../../images/vector/foreground/brands/googlecloud.svg';
import IbmCloudLogo from '../../images/vector/foreground/brands/ibm-cloud.svg';
import icons from '../../images/vector/foreground/icons';
import queries from '../../queries';
import imgUseCases from '../../images/vector/foreground/use-cases';

import styles from './HeroContent.module.scss';

const { IconArrowRightSolid, IconCaret } = icons;

const UseCaseCard = ({ data }) => {
  const ImgUseCase = imgUseCases[data.frontmatter.image];

  return (
    <Link to={data.fields.slug} className={styles.useCaseCard}>
      {ImgUseCase && <ImgUseCase className="image" />}
      <div className="title">{data.frontmatter.titlePlural}</div>
      <div className="arrow"><IconArrowRightSolid /></div>
    </Link>
  );
};

const scrollToTrustSection = () => document.getElementById(`trust-section`).scrollIntoView({ behavior: `smooth` });

const HeroContent = ({ className }) => {
  const useCases = queries.useCases().slice(0, 3);

  return (
    <div className={`${styles.heroContent} ${className}`}>
      <FlexBox direction="row">
        <FlexCell>
          <div className={styles.title}>Code to Cloud<br />In 60 Seconds</div>
          <div className={styles.subTitle}>
            Voxis is everything you need to deploy modern<br className={styles.subTitleBreak} />
          apps to the cloud without the pain of DevOps.
        </div>
        </FlexCell>
        <FlexCell className={styles.rightCell}>
          {useCases.map((data, index) => <UseCaseCard key={index} data={data} />)}
        </FlexCell>
      </FlexBox>
      <FlexBox className={styles.ctaFlexer} direction="row">
        <FlexCell className={styles.buttonContainer}>
          <Button className={styles.button} to="/start" icon="IconRocket">Get Started Free</Button>
        </FlexCell>
        <FlexCell className={styles.cloudLogoContainer}>
          <AwsLogo className={styles.cloudLogo} />
          <AzureLogo className={styles.cloudLogo} />
          <GoogleCloudLogo className={styles.cloudLogo} />
          <DigitalOceanLogo className={styles.cloudLogo} />
          <IbmCloudLogo className={styles.cloudLogo} />
        </FlexCell>
      </FlexBox>
      <IconCaret className={styles.downArrow} onClick={scrollToTrustSection} />
    </div>
  );
};

HeroContent.propTypes = {
  className: PropTypes.string,
};

HeroContent.defaultProps = {
  className: ``,
};

export default HeroContent;
