import PropTypes from 'prop-types';
import React from 'react';

import styles from './LegalSection.module.scss';

const LegalSection = ({ className }) => {
  return (
    <section className={`${styles.section} ${className}`}>
      <div className={styles.card}>
        <h3>Terms and Conditions</h3>
        <p>TBC</p>
      </div>
    </section>
  );
};

LegalSection.propTypes = {
  className: PropTypes.string,
};

LegalSection.defaultProps = {
  className: ``,
};

export default LegalSection;
