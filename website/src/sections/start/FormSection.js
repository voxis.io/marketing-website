import PropTypes from 'prop-types';
import React from 'react';

import Button from '../../components/Button';

import styles from './FormSection.module.scss';

const FormSection = ({ className }) => {
  return (
    <section className={`${styles.section} ${className}`}>
      <div className={styles.title}>Pre-Release Testing</div>
      <div className={styles.subTitle}>Public registrations aren't open yet but<br />you can apply to join the preview.</div>
      <Button className={styles.getStartedBtn} to="/preview" icon="IconLock">Join the Preview</Button>
    </section>
  );
};

FormSection.propTypes = {
  className: PropTypes.string,
};

FormSection.defaultProps = {
  className: ``,
};

export default FormSection;
