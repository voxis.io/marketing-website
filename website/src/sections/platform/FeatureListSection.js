// import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';

import FeatureCard from '../../components/FeatureCard';
import queries from '../../queries';

import styles from './FeatureListSection.module.scss';

const FeatureListSection = ({ className }) => {
  const features = queries.features();

  return (
    <section className={`${styles.section} ${className}`}>
      {features.map((data, index) => <FeatureCard key={index} data={data} />)}
    </section>
  );
};

FeatureListSection.propTypes = {
  className: PropTypes.string,
};

FeatureListSection.defaultProps = {
  className: ``,
};

export default FeatureListSection;
