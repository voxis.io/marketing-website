import PropTypes from 'prop-types';
import React from 'react';

// import queries from '../../queries';

import styles from './UseCaseSection.module.scss';

const UseCaseSection = ({ className }) => {
  return (
    <section className={`${styles.section} ${className}`}>
      <h3>Use Cases</h3>

    </section>
  );
};

UseCaseSection.propTypes = {
  className: PropTypes.string,
};

UseCaseSection.defaultProps = {
  className: ``,
};

export default UseCaseSection;
