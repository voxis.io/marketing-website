import PropTypes from 'prop-types';
import React from 'react';

import styles from './ReliabilitySection.module.scss';

const ReliabilitySection = ({ className }) => {
  return (
    <section className={`${styles.section} ${className}`}>
      <h3>Reliability & Security</h3>

    </section>
  );
};

ReliabilitySection.propTypes = {
  className: PropTypes.string,
};

ReliabilitySection.defaultProps = {
  className: ``,
};

export default ReliabilitySection;
