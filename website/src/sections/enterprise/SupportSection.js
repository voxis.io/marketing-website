import PropTypes from 'prop-types';
import React from 'react';

import styles from './SupportSection.module.scss';

const SupportSection = ({ className }) => {
  return (
    <section className={`${styles.section} ${className}`}>
      <h3>Premium Support</h3>

    </section>
  );
};

SupportSection.propTypes = {
  className: PropTypes.string,
};

SupportSection.defaultProps = {
  className: ``,
};

export default SupportSection;
