import PropTypes from 'prop-types';
import React from 'react';

import styles from './CtaSection.module.scss';

const CtaSection = ({ className }) => {
  return (
    <section className={`${styles.section} ${className}`}>
      <h3>Talk to Us</h3>

    </section>
  );
};

CtaSection.propTypes = {
  className: PropTypes.string,
};

CtaSection.defaultProps = {
  className: ``,
};

export default CtaSection;
