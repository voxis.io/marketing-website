import PropTypes from 'prop-types';
import React from 'react';

import styles from './DeploymentSection.module.scss';

const OptionBox = ({ title, copy }) => (
  <div className={styles.optionBox}>
    <div className={styles.optionTitle}>{title}</div>
    <div className={styles.optionIcon}></div>
    <div className={styles.optionCopy}>{copy}</div>
  </div>
);

const DeploymentSection = ({ className }) => (
  <div className={`${styles.container} ${className}`}>
    <div className={styles.topBox}></div>
    <div className={styles.bottomBox}></div>
    <div className={styles.floater}>
      <div className={styles.flexer}>
        <OptionBox title="On Premise" copy="Deploy Voxis on-premise for greater security, compliance and control of your runtime environments and data." />
        <OptionBox title="Cloud Native" copy="Deploy Voxis to any public cloud vendor at the click of a button. Going cloud native has never been easier." />
        <OptionBox title="Fully Managed" copy="Voxis.io is a fully managed, battle tested deployment of the Voxis platform at scale. No fuss deployments." />
      </div>
    </div>
  </div>
);

DeploymentSection.propTypes = {
  className: PropTypes.string,
};

DeploymentSection.defaultProps = {
  className: ``,
};

export default DeploymentSection;
