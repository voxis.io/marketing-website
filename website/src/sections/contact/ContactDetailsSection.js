import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';
import PropTypes from 'prop-types';
import React from 'react';

import Button from '../../components/Button';
import icons from '../../images/vector/foreground/icons';

import styles from './ContactDetailsSection.module.scss';

const { IconFacebook, IconLinkedIn, IconEmail } = icons;

const openLiveChat = () => {
  if (typeof window === `undefined`) return;
  window.fcWidget.open();
};

const ContactDetailsSection = ({ className }) => {
  const data = useStaticQuery(graphql`
    query ContainDetailsQuery {
      site {
        siteMetadata {
          legalName
          officeAddress
          socialLinks {
            facebook
            linkedIn
          }
          emails {
            main
          }
        }
      }
      allImageSharp(filter: {original: {src: {glob: "/static/map*.jpg"}}}) {
        edges {
          node {
            fluid(maxHeight: 1600, quality: 100) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  `);

  const mapImage = data.allImageSharp.edges[0].node;
  const { legalName, officeAddress, socialLinks, emails } = data.site.siteMetadata;

  return (
    <section className={`${styles.section} ${className}`}>
      <div className={styles.container}>
        <div className={styles.map}>
          <a href="https://www.bing.com/maps?osid=4c2d45f1-3fdf-4996-a210-f0b410dee766&cp=51.523789~-0.085135&lvl=16&v=2&sV=2&form=S00027" target="_blank" rel="noopener noreferrer">
            <Img fluid={mapImage.fluid} className={styles.mapImage} objectFit="cover" objectPosition="center" />
          </a>
          {/* <div className={styles.mapCover}></div> */}
        </div>
        <div className={styles.details}>
          <div className={styles.float}>
            <Button onClick={openLiveChat} icon="IconChat" className={styles.liveChatBtn}>Start a Live Chat</Button><br />
            <div className={styles.info}>
              <a href={`mailto:${emails.main}`}>{emails.main}</a><br />
              <br />
              {legalName}<br />
              <br />
              {officeAddress.split(/,\s/g).map((line, index) => (<div key={index}>{line}</div>))}
            </div>
            <div className={styles.socials}>
              <a href={socialLinks.facebook} target="_blank" className={styles.socialIcon} rel="noopener noreferrer"><IconFacebook /></a>
              <a href={socialLinks.linkedIn} target="_blank" className={styles.socialIcon} rel="noopener noreferrer"><IconLinkedIn /></a>
              <a href={`mailto:${emails.main}`} className={styles.socialIcon}><IconEmail /></a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

ContactDetailsSection.propTypes = {
  className: PropTypes.string,
};

ContactDetailsSection.defaultProps = {
  className: ``,
};

export default ContactDetailsSection;
