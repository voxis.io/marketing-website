import PropTypes from 'prop-types';
import React from 'react';

import FaqItem from '../../components/FaqItem';
import queries from '../../queries';

import styles from './FaqListSection.module.scss';

function __groupFaqsByCategory(faqs) {
  return faqs.reduce((acc, faq) => {
    const [, , category] = faq.fields.slug.split(`/`);
    acc[category] = acc[category] || [];
    acc[category].push(faq);
    return acc;
  }, {});
}

const FaqGroup = ({ category, faqs }) => {
  return (
    <div id={category} className={styles.faqGroup}>
      <h4><a href={`#${category}`}>{category}</a></h4>
      {faqs.map((data, index) => <FaqItem key={index} data={data} />)}
    </div>
  );
};

const FaqListSection = ({ className }) => {
  const allFaqs = queries.faqs();
  const groupedFaqs = __groupFaqsByCategory(allFaqs);

  return (
    <section className={`${styles.section} ${className}`}>
      <h3>Frequently Asked Questions</h3>
      {Object.entries(groupedFaqs).map(([category, faqs]) => <FaqGroup key={category} category={category} faqs={faqs} />)}
    </section>
  );
};

FaqListSection.propTypes = {
  className: PropTypes.string,
};

FaqListSection.defaultProps = {
  className: ``,
};

export default FaqListSection;
