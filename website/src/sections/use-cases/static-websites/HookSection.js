import PropTypes from 'prop-types';
import React from 'react';

import Button from '../../../components/Button';
import Css3Logo from '../../../images/vector/foreground/brands/css3.svg';
import DocusaurusLogo from '../../../images/vector/foreground/brands/docusaurus.svg';
import FlexBox from '../../../components/layout/FlexBox';
import FlexCell from '../../../components/layout/FlexCell';
import GatsbyLogo from '../../../images/vector/foreground/brands/gatsby.svg';
import Html5Logo from '../../../images/vector/foreground/brands/html5.svg';
import HugoLogo from '../../../images/vector/foreground/brands/hugo.svg';
import NuxtLogo from '../../../images/vector/foreground/brands/nuxt.svg';
import SvelteLogo from '../../../images/vector/foreground/brands/svelte.svg';
import Terminal from '../../../components/Terminal';
import VuePressLogo from '../../../images/vector/foreground/brands/vuepress.svg';

import styles from './HookSection.module.scss';

const ToolsGrid = () => {
  return (
    <FlexBox className={styles.toolsFlexer}>
      <FlexCell flex="1">
        <FlexBox direction="row">
          <FlexCell flex="1"><GatsbyLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><NuxtLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><HugoLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><DocusaurusLogo className={styles.toolLogo} /></FlexCell>
        </FlexBox>
      </FlexCell>
      <FlexCell flex="1">
        <FlexBox direction="row">
          <FlexCell flex="1"><SvelteLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><Html5Logo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><Css3Logo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><VuePressLogo className={styles.toolLogo} /></FlexCell>
        </FlexBox>
      </FlexCell>
    </FlexBox>
  );
};

const HookSection = ({ className }) => {
  const terminalLines = [
    { type: `shell-prompt`, dir: `website`, branch: `redesign`, userInput: `git push` },
    { type: `app-output`, print: `Total 0 (delta 0), reused 0 (delta 0)` },
    { type: `app-output`, print: `To git@gitlab.com:voxis.io/live-demos/website.git` },
    { type: `app-output`, print: `* [new branch]      redesign -> redesign` },
  ];

  return (
    <section className={`${styles.section} ${className}`}>
      <FlexBox className={styles.hooks} direction="row">
        <FlexCell flex="1" className={styles.terminalCell}>
          <h3>One Line Deploy</h3>
          <p><strong>git push</strong> is all you need</p>
          <Terminal className={styles.terminalContainer} windowClassName={styles.terminalWindow} lines={terminalLines} />
        </FlexCell>
        <FlexCell flex="1" className={styles.toolsCell}>
          <h3>Modern Tools</h3>
          <p><strong>use any</strong> static site generator</p>
          <ToolsGrid />
          <div className={styles.callToActionContainer}>
            <Button to="/start" icon="IconRocket" className={styles.callToActionBtn}>Get Started Free</Button>
          </div>
        </FlexCell>
      </FlexBox>
    </section>
  );
};

HookSection.propTypes = {
  className: PropTypes.string,
};

HookSection.defaultProps = {
  className: ``,
};

export default HookSection;
