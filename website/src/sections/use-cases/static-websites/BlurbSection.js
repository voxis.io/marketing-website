import PropTypes from 'prop-types';
import React from 'react';

import styles from './BlurbSection.module.scss';

const BlurbSection = ({ className, data }) => {
  return (
    <section className={`${styles.section} ${className}`}>
      <div className={styles.floater}>
        <div className={styles.body} dangerouslySetInnerHTML={{ __html: data.markdownRemark.html }}></div>
      </div>
    </section>
  );
};

BlurbSection.propTypes = {
  className: PropTypes.string,
};

BlurbSection.defaultProps = {
  className: ``,
};

export default BlurbSection;
