import PropTypes from 'prop-types';
import React from 'react';

import AwsLogo from '../../../images/vector/foreground/brands/aws.svg';
import AzureLogo from '../../../images/vector/foreground/brands/azure.svg';
import Button from '../../../components/Button';
import DenoLogo from '../../../images/vector/foreground/brands/deno.svg';
import DigitalOceanLogo from '../../../images/vector/foreground/brands/digital-ocean.svg';
import IbmCloudLogo from '../../../images/vector/foreground/brands/ibm-cloud.svg';
import FlexBox from '../../../components/layout/FlexBox';
import FlexCell from '../../../components/layout/FlexCell';
import GoLangLogo from '../../../images/vector/foreground/brands/golang.svg';
import GoogleCloudLogo from '../../../images/vector/foreground/brands/googlecloud.svg';
import NodeJsLogo from '../../../images/vector/foreground/brands/nodejs.svg';
import PythonLogo from '../../../images/vector/foreground/brands/python.svg';
import Terminal from '../../../components/Terminal';

import styles from './HookSection.module.scss';

const ToolsGrid = () => {
  return (
    <FlexBox className={styles.toolsFlexer}>
      <FlexCell flex="1">
        <FlexBox direction="row">
          <FlexCell flex="1"><NodeJsLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><DenoLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><GoLangLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><PythonLogo className={styles.toolLogo} /></FlexCell>
        </FlexBox>
      </FlexCell>
      <FlexCell flex="1" className={styles.cloudTitle}>
        <p><strong>run anywhere</strong> on any cloud or on-premise</p>
      </FlexCell>
      <FlexCell flex="1">
        <FlexBox direction="row" className={styles.cloudLogoContainer}>
          <FlexCell flex="none"><AwsLogo className={styles.cloudLogo} /></FlexCell>
          <FlexCell flex="none"><AzureLogo className={styles.cloudLogo} /></FlexCell>
          <FlexCell flex="none"><GoogleCloudLogo className={styles.cloudLogo} /></FlexCell>
          <FlexCell flex="none"><DigitalOceanLogo className={styles.cloudLogo} /></FlexCell>
          <FlexCell flex="none"><IbmCloudLogo className={styles.cloudLogo} /></FlexCell>
        </FlexBox>
      </FlexCell>
    </FlexBox>
  );
};

const HookSection = ({ className }) => {
  const terminalLines = [
    { type: `shell-prompt`, dir: `api`, userInput: `vox status` },
    { type: `app-output`, print: `App: telephony-api` },
    { type: `app-output`, print: `7 service(s), 3 region(s)` },
    { type: `app-output`, print: `` },
    { type: `app-output`, print: `<col:cyan>Service    Runtime    Version    Cloud    Regions</col>` },
    { type: `app-output`, print: `━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━` },
    { type: `app-output`, print: ` auth       Node         7        AWS        3` },
    { type: `app-output`, print: ` users      Node        35        AWS        3` },
    { type: `app-output`, print: ` numbers    Node         6        AWS        3` },
    { type: `app-output`, print: ` calls      Go          22        AWS        3` },
    { type: `app-output`, print: ` sms        Node        17        AWS        3` },
    { type: `app-output`, print: ` video      Go           2        AWS        3` },
    { type: `app-output`, print: ` stats      Python       4        AWS        3` },
  ];

  return (
    <section className={`${styles.section} ${className}`}>
      <FlexBox className={styles.hooks} direction="row">
        <FlexCell flex="1" className={styles.terminalCell}>
          <h3>Fully Managed</h3>
          <p><strong>no infrastructure</strong> = no fuss + no pain + no waste</p>
          <Terminal className={styles.terminalContainer} windowClassName={styles.terminalWindow} lines={terminalLines} />
        </FlexCell>
        <FlexCell flex="1" className={styles.toolsCell}>
          <h3>You Write, We Run</h3>
          <p><strong>serverless</strong> but without the pain</p>
          <ToolsGrid />
          <div className={styles.callToActionContainer}>
            <Button to="/start" icon="IconRocket" className={styles.callToActionBtn}>Get Started Free</Button>
          </div>
        </FlexCell>
      </FlexBox>
    </section>
  );
};

HookSection.propTypes = {
  className: PropTypes.string,
};

HookSection.defaultProps = {
  className: ``,
};

export default HookSection;
