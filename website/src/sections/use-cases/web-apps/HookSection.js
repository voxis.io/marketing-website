import PropTypes from 'prop-types';
import React from 'react';

import AngularLogo from '../../../images/vector/foreground/brands/angular.svg';
import BabelLogo from '../../../images/vector/foreground/brands/babel.svg';
import Button from '../../../components/Button';
import Css3Logo from '../../../images/vector/foreground/brands/css3.svg';
import DocusaurusLogo from '../../../images/vector/foreground/brands/docusaurus.svg';
import FlexBox from '../../../components/layout/FlexBox';
import FlexCell from '../../../components/layout/FlexCell';
import GatsbyLogo from '../../../images/vector/foreground/brands/gatsby.svg';
import Html5Logo from '../../../images/vector/foreground/brands/html5.svg';
import HugoLogo from '../../../images/vector/foreground/brands/hugo.svg';
import JsLogo from '../../../images/vector/foreground/brands/js.svg';
import NpmLogo from '../../../images/vector/foreground/brands/npm.svg';
import NuxtLogo from '../../../images/vector/foreground/brands/nuxt.svg';
import ReactLogo from '../../../images/vector/foreground/brands/react-logo.svg';
import SvelteLogo from '../../../images/vector/foreground/brands/svelte.svg';
import Terminal from '../../../components/Terminal';
import TypeScriptLogo from '../../../images/vector/foreground/brands/ts.svg';
import VueLogo from '../../../images/vector/foreground/brands/vue.svg';
import VuePressLogo from '../../../images/vector/foreground/brands/vuepress.svg';
import WebpackLogo from '../../../images/vector/foreground/brands/webpack.svg';

import styles from './HookSection.module.scss';

const ToolsGrid = () => {
  return (
    <FlexBox className={styles.toolsFlexer}>
      <FlexCell flex="1">
        <FlexBox direction="row">
          <FlexCell flex="1"><GatsbyLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><VueLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><ReactLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><NuxtLogo className={styles.toolLogo} /></FlexCell>
        </FlexBox>
      </FlexCell>
      <FlexCell flex="1">
        <FlexBox direction="row">
          <FlexCell flex="1"><HugoLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><DocusaurusLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><SvelteLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><AngularLogo className={styles.toolLogo} /></FlexCell>
        </FlexBox>
      </FlexCell>
      <FlexCell flex="1">
        <FlexBox direction="row">
          <FlexCell flex="1"><Html5Logo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><Css3Logo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><JsLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><TypeScriptLogo className={styles.toolLogo} /></FlexCell>
        </FlexBox>
      </FlexCell>
      <FlexCell flex="1">
        <FlexBox direction="row">
          <FlexCell flex="1"><NpmLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><BabelLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><WebpackLogo className={styles.toolLogo} /></FlexCell>
          <FlexCell flex="1"><VuePressLogo className={styles.toolLogo} /></FlexCell>
        </FlexBox>
      </FlexCell>
    </FlexBox>
  );
};

const HookSection = ({ className }) => {
  const terminalLines = [
    { type: `shell-prompt`, dir: `http-status-codes`, branch: `new-feature`, userInput: `git push` },
    { type: `app-output`, print: `Total 0 (delta 0), reused 0 (delta 0)` },
    { type: `app-output`, print: `To git@gitlab.com:voxis.io/live-demos/http-status-codes.git` },
    { type: `app-output`, print: `* [new branch]      new-feature -> new-feature` },
  ];

  return (
    <section className={`${styles.section} ${className}`}>
      <FlexBox className={styles.hooks} direction="row">
        <FlexCell flex="1" className={styles.terminalCell}>
          <h3>Push&nbsp;&nbsp;•&nbsp;&nbsp;Build&nbsp;&nbsp;•&nbsp;&nbsp;Deploy</h3>
          <p><strong>go live</strong> in seconds, globally</p>
          <Terminal className={styles.terminalContainer} windowClassName={styles.terminalWindow} lines={terminalLines} />
          <div className={styles.callToActionContainer}>
            <Button to="/start" icon="IconRocket" className={styles.callToActionBtn}>Get Started Free</Button>
          </div>
        </FlexCell>
        <FlexCell flex="1" className={styles.toolsCell}>
          <h3>Build It Your Way</h3>
          <p><strong>no changes</strong> to your workflow</p>
          <ToolsGrid />
        </FlexCell>
      </FlexBox>
    </section>
  );
};

HookSection.propTypes = {
  className: PropTypes.string,
};

HookSection.defaultProps = {
  className: ``,
};

export default HookSection;
