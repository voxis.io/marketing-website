import PropTypes from 'prop-types';
import React from 'react';

import queries from '../../queries';
import UseCaseCard from '../../components/UseCaseCard';

import styles from './UseCaseListSection.module.scss';

const UseCaseListSection = ({ className }) => {
  const useCases = queries.useCases();

  return (
    <section className={`${styles.section} ${className}`}>
      <div className={styles.container}>
        {useCases.map((data, index) => <UseCaseCard key={index} data={data} />)}
      </div>
    </section>
  );
};

UseCaseListSection.propTypes = {
  className: PropTypes.string,
};

UseCaseListSection.defaultProps = {
  className: ``,
};

export default UseCaseListSection;
