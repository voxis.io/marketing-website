import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';

import FlexBox from '../../../components/layout/FlexBox';
import FlexCell from '../../../components/layout/FlexCell';
import queries from '../../../queries';
import imgUseCases from '../../../images/vector/foreground/use-cases';

import styles from './HeroContent.module.scss';

const UseCaseTabBtn = ({ data, selected }) => {
  const ImgUseCase = imgUseCases[data.frontmatter.image];

  console.log(`data.fields.slug`, data.fields.slug);

  return (
    <Link to={data.fields.slug} className={`${styles.useCaseCard} ${selected && styles.tabBtnOn}`}>
      <FlexBox className="top-row" direction="row">
        <FlexCell className="image-container">{ImgUseCase && <ImgUseCase className="image" />}</FlexCell>
        <FlexCell className="title">{data.frontmatter.titlePlural}</FlexCell>
      </FlexBox>
      <div className="sub-title">{data.frontmatter.subTitle}</div>
    </Link>
  );
};

const HeroContent = ({ className }) => {
  const useCases = queries.useCases().slice(0, 3);
  const curPath = typeof window !== `undefined` ? window.location.pathname : ``;

  return (
    <div className={`${styles.container} ${className}`}>
      {useCases.map((data, index) => <UseCaseTabBtn key={index} selected={data.fields.slug === curPath} data={data} />)}
    </div>
  );
};

HeroContent.propTypes = {
  className: PropTypes.string,
};

HeroContent.defaultProps = {
  className: ``,
};

export default HeroContent;
