// import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';

import FeatureCard from '../../../components/FeatureCard';
import queries from '../../../queries';

import styles from './FeatureListSection.module.scss';

function __filterRelatedFeatures(relatedFeatures, feature) {
  const [, , featureId] = feature.fields.slug.split(`/`);
  return relatedFeatures.includes(featureId);
}

const FeatureListSection = ({ className, relatedFeatures }) => {
  const features = queries.features().filter(feature => __filterRelatedFeatures(relatedFeatures, feature));

  return (
    <section className={`${styles.section} ${className}`}>
      <h3>Related Features</h3>
      <div className={styles.container}>
        {features.map((data, index) => <FeatureCard key={index} data={data} />)}
      </div>
    </section>
  );
};

FeatureListSection.propTypes = {
  className: PropTypes.string,
  relatedFeatures: PropTypes.array,
};

FeatureListSection.defaultProps = {
  className: ``,
};

export default FeatureListSection;
