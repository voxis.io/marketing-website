import PropTypes from 'prop-types';
import React from 'react';

import styles from './BlurbSection.module.scss';

const BlurbSection = ({ className }) => {
  return (
    <section className={`${styles.section} ${className}`}>
      <div className={styles.floater}>
        <div className={styles.body}>
          <p>Hello world this is some <strong>text</strong> here!</p>
        </div>
      </div>
    </section>
  );
};

BlurbSection.propTypes = {
  className: PropTypes.string,
};

BlurbSection.defaultProps = {
  className: ``,
};

export default BlurbSection;
