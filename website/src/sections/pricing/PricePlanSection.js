import PropTypes from 'prop-types';
import React from 'react';

import Button from '../../components/Button';
import queries from '../../queries';

import styles from './PricePlanSection.module.scss';

const HeaderRowMostPopular = ({ pricingPlans }) => {
  return (
    <tr>
      <th className="most-popular-header"></th>
      {pricingPlans.map((plan, index) => <th key={index} className={`most-popular-header ${plan.frontmatter.mostPopular && `on`}`}>Most Popular</th>)}
    </tr>
  );
};

const HeaderRowPlanName = ({ pricingPlans }) => {
  return (
    <tr>
      <th className="plan-name"></th>
      {pricingPlans.map((plan, index) => <th key={index} className={`plan-name ${plan.frontmatter.mostPopular && `most-popular-row`}`}>{plan.frontmatter.name}</th>)}
    </tr>
  );
};

const PriceCurrency = ({ plan }) => (
  <>
    <div>
      <span className="currency">$</span>
      <span className="amount currency">{plan.frontmatter.priceMonthly}</span>
    </div>
    <div>
      <span className="period">per month</span>
    </div>
  </>
);

const PriceText = ({ plan }) => (
  <>
    <span className="amount text">{plan.frontmatter.priceText}</span>
  </>
);

const HeaderRowPrice = ({ pricingPlans }) => {
  return (
    <tr>
      <th className="price"></th>
      {pricingPlans.map((plan, index) =>
        <th key={index} className={`price ${plan.frontmatter.mostPopular && `most-popular-row`}`}>
          {plan.frontmatter.priceText ? <PriceText plan={plan} /> : <PriceCurrency plan={plan} />}
        </th>
      )}
    </tr>
  );
};

const FeatureRow = ({ title, propName, singlePlan, pricingPlans }) => {
  const list = (singlePlan ? [singlePlan] : pricingPlans);
  return (
    <tr>
      <td className="feature">{title}</td>
      {list.map((plan, index) =>
        <td key={index} className={`feature ${plan.frontmatter.mostPopular && `most-popular-row`}`}>
          {plan.frontmatter[propName]}
        </td>
      )}
    </tr>
  );
};

const CtaRow = ({ pricingPlans }) => {
  return (
    <tr>
      <td className="cta"></td>
      {pricingPlans.map((plan, index) =>
        <td key={index} className={`cta ${plan.frontmatter.mostPopular && `most-popular-row`}`}>
          <Button className="btn" to={`/start?plan=${plan.frontmatter.name.toLowerCase()}`}>
            <span>Get Started</span>
            <span>➞</span>
          </Button>
        </td>
      )}
    </tr>
  );
};

const PricingPlanCard = ({ plan }) => {

  return (
    <div className={`${styles.pricingPlanCard} ${plan.frontmatter.mostPopular && styles.mostPopularCard}`}>
      <div className={`most-popular-header ${plan.frontmatter.mostPopular && `on`}`}>Most Popular</div>
      <div className="plan-name">{plan.frontmatter.name}</div>
      <div className="price">
        {plan.frontmatter.priceText ? <PriceText plan={plan} /> : <PriceCurrency plan={plan} />}
      </div>
      <table>
        <tbody>
          <FeatureRow title="Deployed Apps" propName="featureDeployedApps" singlePlan={plan} />
          <FeatureRow title="Compute" propName="featureCompute" singlePlan={plan} />
          <FeatureRow title="Data Storage" propName="featureDataStorage" singlePlan={plan} />
          <FeatureRow title="Data Transfer" propName="featureDataTransfer" singlePlan={plan} />
          <FeatureRow title="Concurrent Builds" propName="featureConcurrentBuilds" singlePlan={plan} />
        </tbody>
      </table>
      <div className="cta">
        <Button className="btn" to={`/start?plan=${plan.frontmatter.name.toLowerCase()}`}>
          <span>Get Started</span>
          <span>➞</span>
        </Button>
      </div>
    </div>
  );
};

const PricePlanSection = ({ className }) => {
  const pricingPlans = queries.pricingPlans();

  return (
    <section className={`${styles.section} ${className}`}>
      <table className={styles.table}>
        <thead>
          <HeaderRowMostPopular pricingPlans={pricingPlans} />
          <HeaderRowPlanName pricingPlans={pricingPlans} />
          <HeaderRowPrice pricingPlans={pricingPlans} />
        </thead>
        <tbody>
          <FeatureRow title="Deployed Apps" propName="featureDeployedApps" pricingPlans={pricingPlans} />
          <FeatureRow title="Compute" propName="featureCompute" pricingPlans={pricingPlans} />
          <FeatureRow title="Data Storage" propName="featureDataStorage" pricingPlans={pricingPlans} />
          <FeatureRow title="Data Transfer" propName="featureDataTransfer" pricingPlans={pricingPlans} />
          <FeatureRow title="Concurrent Builds" propName="featureConcurrentBuilds" pricingPlans={pricingPlans} />
        </tbody>
        <tfoot>
          <CtaRow pricingPlans={pricingPlans} />
        </tfoot>
      </table>
      <div className={styles.cardContainer}>
        {pricingPlans.map((plan, index) => <PricingPlanCard key={index} plan={plan} />)}
      </div>
    </section>
  );
};

PricePlanSection.propTypes = {
  className: PropTypes.string,
};

PricePlanSection.defaultProps = {
  className: ``,
};

export default PricePlanSection;
