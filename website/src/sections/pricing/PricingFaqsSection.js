import PropTypes from 'prop-types';
import React from 'react';

import FaqItem from '../../components/FaqItem';
import FlexBox from '../../components/layout/FlexBox';
import FlexCell from '../../components/layout/FlexCell';
import queries from '../../queries';

import styles from './PricingFaqsSection.module.scss';

function __isEven(index) {
  return Boolean(index % 2);
}

function __isOdd(index) {
  return !Boolean(index % 2);
}

const PricingFaqsSection = ({ className }) => {
  const allFaqs = queries.faqs();
  const faqs = allFaqs.filter(faq => faq.frontmatter.pricingPage);

  return (
    <section className={`${styles.section} ${className}`}>
      <h3>Frequently Asked Questions</h3>
      <FlexBox className={styles.container} direction="row">
        <FlexCell className={styles.leftColumn}>
          {faqs.map((data, index) => (__isOdd(index) && <FaqItem key={index} className={styles.faqItem} data={data} />))}
        </FlexCell>
        <FlexCell className={styles.rightColumn}>
          {faqs.map((data, index) => (__isEven(index) && <FaqItem key={index} className={styles.faqItem} data={data} />))}
        </FlexCell>
      </FlexBox>
    </section>
  );
};

PricingFaqsSection.propTypes = {
  className: PropTypes.string,
};

PricingFaqsSection.defaultProps = {
  className: ``,
};

export default PricingFaqsSection;
