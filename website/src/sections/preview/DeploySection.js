import PropTypes from 'prop-types';
import React from 'react';

import UseCaseCard from '../../components/UseCaseCard';
import queries from '../../queries';

import styles from './DeploySection.module.scss';

const DeploySection = ({ className }) => {
  const useCases = queries.useCases();

  return (
    <section className={`${styles.section} ${className}`}>
      <h3>Deploy</h3>
      <div className={styles.container}>
        {useCases.map((data, index) => <UseCaseCard key={index} data={data} />)}
      </div>
    </section>
  );
};

DeploySection.propTypes = {
  className: PropTypes.string,
};

DeploySection.defaultProps = {
  className: ``,
};

export default DeploySection;
