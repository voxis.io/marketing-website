import PropTypes from 'prop-types';
import React from 'react';

import EmailSignUp from '../../components/EmailSignUp';
import FlexBox from '../../components/layout/FlexBox';
import FlexCell from '../../components/layout/FlexCell';

import styles from './HeroContent.module.scss';

const HeroContent = ({ className }) => (
  <>
    <FlexBox className={`${styles.flex} ${styles.heroContent} ${className}`} direction="row">
      <FlexCell>
        <div className={styles.title}>Join Voxis Preview</div>
        <div className={styles.subTitle}>
          Apply to join the exclusive Voxis Preview,<br className={styles.subTitleBreak} />
          opening to a select number of people soon</div>
        <EmailSignUp source="prelaunch" buttonLabel="Apply Now" className={styles.emailField} />
      </FlexCell>
      {/* <FlexCell>
        <></>
      </FlexCell> */}
    </FlexBox>
  </>
);

HeroContent.propTypes = {
  className: PropTypes.string,
};

HeroContent.defaultProps = {
  className: ``,
};

export default HeroContent;
