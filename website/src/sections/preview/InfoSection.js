import PropTypes from 'prop-types';
import React from 'react';

import Button from '../../components/Button';
import FlexBox from '../../components/layout/FlexBox';
import FlexCell from '../../components/layout/FlexCell';
import DeploymentImage from '../../images/vector/foreground/features/zero-config-deployments.svg';

import styles from './InfoSection.module.scss';

const InfoSection = ({ className }) => {
  return (
    <section className={`${styles.section} ${className}`}>
      <h3>What is Voxis?</h3>
      <FlexBox direction="row" className={styles.flexer}>
        <FlexCell className={styles.markdownContainer}>
          <p>Voxis is an open source toolkit for shipping cloud software without the pain of DevOps. Voxis combines battle-tested leading open source software with industry best practice to help you architect, deliver, and maintain cloud software in record time.</p>
          <p>Forget all those custom deployment scripts, half-baked testing strategies, and boilerplate code - Voxis automates all the work you don't want to do so you can focus on the important that create value for your customers.</p>
        </FlexCell>
        <FlexCell className={styles.iconContainer}>
          <DeploymentImage className={styles.icon} />
        </FlexCell>
      </FlexBox>
      <Button className={styles.readMoreBtn} to="/learn/00001/welcome-to-voxis" icon="IconBlog">Continue Reading...</Button>
    </section>
  );
};

InfoSection.propTypes = {
  className: PropTypes.string,
};

InfoSection.defaultProps = {
  className: ``,
};

export default InfoSection;
