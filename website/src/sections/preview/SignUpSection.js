import PropTypes from 'prop-types';
import React from 'react';

import EmailSignUp from '../../components/EmailSignUp';

import styles from './SignUpSection.module.scss';

const SignUpSection = ({ className }) => (
  <section id="sign-up-section" className={`${styles.section} ${className}`}>
    <div className={styles.copy}>
      <strong>Join the Voxis waitlist</strong><br />
      Get notified when you reach the top of the list
    </div>
    <EmailSignUp
      source="prelaunch"
      buttonLabel="Apply Now"
      className={styles.emailField}
      buttonClassName={styles.emailButton}
      successMsgClassName={styles.successMsg}
    />
  </section>
);

SignUpSection.propTypes = {
  className: PropTypes.string,
};

SignUpSection.defaultProps = {
  className: ``,
};

export default SignUpSection;
