import PropTypes from 'prop-types';
import React from 'react';

import styles from './TeamSection.module.scss';

const TeamSection = ({ className }) => {
  return (
    <section className={`${styles.section} ${className}`}>

    </section>
  );
};

TeamSection.propTypes = {
  className: PropTypes.string,
};

TeamSection.defaultProps = {
  className: ``,
};

export default TeamSection;
