import PropTypes from 'prop-types';
import React from 'react';

import styles from './DetailsSection.module.scss';

const DetailsSection = ({ className }) => {
  return (
    <section className={`${styles.section} ${className}`}>

    </section>
  );
};

DetailsSection.propTypes = {
  className: PropTypes.string,
};

DetailsSection.defaultProps = {
  className: ``,
};

export default DetailsSection;
