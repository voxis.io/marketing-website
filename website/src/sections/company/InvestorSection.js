import PropTypes from 'prop-types';
import React from 'react';

import styles from './InvestorSection.module.scss';

const InvestorSection = ({ className }) => {
  return (
    <section className={`${styles.section} ${className}`}>

    </section>
  );
};

InvestorSection.propTypes = {
  className: PropTypes.string,
};

InvestorSection.defaultProps = {
  className: ``,
};

export default InvestorSection;
