import PropTypes from 'prop-types';
import React from 'react';

import styles from './StorySection.module.scss';

const StorySection = ({ className }) => {
  return (
    <section className={`${styles.section} ${className}`}>

    </section>
  );
};

StorySection.propTypes = {
  className: PropTypes.string,
};

StorySection.defaultProps = {
  className: ``,
};

export default StorySection;
