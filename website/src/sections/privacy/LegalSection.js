import PropTypes from 'prop-types';
import React from 'react';

import Button from '../../components/Button';
import privacyPolicyPdf from '../../../static/legal/privacy-policy-mar-2020.pdf';

import styles from './LegalSection.module.scss';

const LegalSection = ({ className }) => {
  return (
    <section className={`${styles.section} ${className}`}>
      <div className={styles.card}>
        <h3>Privacy Policy</h3>
        <p>Our GDPR-compliant privacy policy covers our website, products and services. You can view it by clicking here:</p>
        <Button className={styles.button} staticFile={privacyPolicyPdf} icon="IconExternalLink" staticMode="external">View Privacy Policy</Button>
      </div>
    </section>
  );
};

LegalSection.propTypes = {
  className: PropTypes.string,
};

LegalSection.defaultProps = {
  className: ``,
};

export default LegalSection;
