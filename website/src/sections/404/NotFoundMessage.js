import PropTypes from 'prop-types';
import React from 'react';

import Button from '../../components/Button';
import icons from '../../images/vector/foreground/icons';

import styles from './NotFoundMessage.module.scss';

const { IconNotFound } = icons;

const NotFoundMessage = ({ className }) => (
	<div className={styles.notFound}>
		<IconNotFound className={styles.icon} />
		<h1 className={styles.title}>Nothing to see here</h1>
		<p className={styles.copy}>There doesn't seem to be a page here.<br />Why not try one of these pages instead?</p>
		<div className={styles.buttonRow}>
			<Button className={styles.button} to="/" icon="IconHome">Home</Button>
			<Button className={styles.button} to="/use-cases" icon="IconStaticWebsite">Use Cases</Button>
			<Button className={styles.button} to="/learn" icon="IconTutorials">Learn</Button>
			<Button className={styles.button} to="/start" icon="IconRocket">Get Started Free</Button>
		</div>
	</div>
);

NotFoundMessage.propTypes = {
	className: PropTypes.string,
};

NotFoundMessage.defaultProps = {
	className: ``,
};

export default NotFoundMessage;
