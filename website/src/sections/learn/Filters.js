import PropTypes from 'prop-types';
import React from 'react';

import queries from '../../queries';

import styles from './Filters.module.scss';

const FilterBtn = ({ className, label, selected, onClick }) => {
  const labelFormatted = `${label[0].toUpperCase()}${label.substr(1)}`;

  return (
    <div className={`${styles.filterBtn} ${selected && styles.filterBtnOn} ${className}`} onClick={onClick} role="none">
      {labelFormatted}
    </div>
  );
};

const Filters = ({ className, selectedCategory, setSelectedCategory }) => {
  const metadata = queries.blogMetadata();
  const categories = [...metadata.categories].sort();

  return (
    <div className={`${styles.filters} ${className}`}>
      <span className={styles.filterLabel}>Show me:</span>
      <FilterBtn label="Everything" selected={!selectedCategory} onClick={() => setSelectedCategory(``)} />
      {categories.map((category, index) => <FilterBtn key={index} label={category} selected={selectedCategory === category} onClick={() => setSelectedCategory(category)} />)}
    </div>
  );
};

Filters.propTypes = {
  className: PropTypes.string,
  selectedCategory: PropTypes.string,
  setSelectedCategory: PropTypes.func,
};

Filters.defaultProps = {
  className: ``,
  selectedCategory: ``,
};

export default Filters;
