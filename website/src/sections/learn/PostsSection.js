import { Link } from 'gatsby';
import Img from 'gatsby-image';
import moment from 'moment';
import PropTypes from 'prop-types';
import React from 'react';

import FlexBox from '../../components/layout/FlexBox';
import FlexCell from '../../components/layout/FlexCell';
import queries from '../../queries';

import styles from './PostsSection.module.scss';

const BlogPost = ({ data }) => {
  const { featuredImage } = data.frontmatter;

  return (
    <FlexBox className={styles.blogPost}>
      <FlexCell className={styles.imageCell}>
        <Link to={data.fields.slug} className={styles.featuredImageContainer}>
          {featuredImage && <Img fluid={featuredImage.childImageSharp.fluid} className={styles.featureImageWrapper} objectFit="cover" objectPosition="center" />}
          <div className={styles.category}>{data.frontmatter.category}</div>
        </Link>
      </FlexCell>
      <FlexCell className={styles.infoCell}>
        <div className={styles.content}>
          <div className={styles.meta}>
            <div className={styles.date}>{moment(data.frontmatter.date).format(`D MMM, YYYY`)}</div>
            <div className={styles.author}>{data.frontmatter.author}</div>
          </div>
          <div className={styles.copy}>
            <Link to={data.fields.slug} className={styles.title}>{data.frontmatter.title}</Link>
            <div className={styles.synopsis}>{data.frontmatter.synopsis}</div>
          </div>
          <Link to={data.fields.slug} className={styles.readMore}>
            <span>Read More</span>
            <span>➞</span>
          </Link>
        </div>
      </FlexCell>
    </FlexBox>
  );
};

function __filterBlogPostsByCategory(blogPosts, selectedCategory) {
  if (!selectedCategory) return blogPosts;
  return blogPosts.filter(blogPost => blogPost.frontmatter.category === selectedCategory);
}

const PostsSection = ({ className, selectedCategory }) => {
  const blogPosts = queries.blogPosts();
  const filteredBlogPosts = __filterBlogPostsByCategory(blogPosts, selectedCategory);

  return (
    <section className={`${styles.section} ${className}`}>
      {filteredBlogPosts.map((data, index) => <BlogPost key={index} data={data} />)}
    </section>
  );
};

PostsSection.propTypes = {
  className: PropTypes.string,
  selectedCategory: PropTypes.string,
};

PostsSection.defaultProps = {
  className: ``,
  selectedCategory: ``,
};

export default PostsSection;
