import PropTypes from 'prop-types';
import React, { useRef, useState } from 'react';

import styles from './EmailSignUp.module.scss';

const EMAIL_SUBMISSION_URL = `https://8d22j5prfj.execute-api.eu-west-1.amazonaws.com/production/voxis-website-request-info`;

const submitEmail = async (status, setStatus, setErrorMsg, inputEl, email, source) => {
  if (status === `success`) return;

  const emailPrepared = email.trim();

  if (!emailPrepared) {
    setStatus(`failure`);
    setErrorMsg(`Please enter an email address`);
    return;
  }

  setStatus(`loading`);

  const res = await fetch(EMAIL_SUBMISSION_URL, {
    method: `POST`,
    headers: {
      'content-type': `application/x-www-form-urlencoded`,
    },
    body: `email=${encodeURIComponent(emailPrepared)}&source=${encodeURIComponent(source)}`,
  });
  const data = await res.json();

  if (data.success) {
    setStatus(`success`);
  } else {
    setStatus(`failure`);
    setErrorMsg(data.error || `Unknown error`);
    inputEl.current.focus();
  }
};

const EmailField = ({ email, setEmail, status, setStatus, setErrorMsg, placeholder, buttonLabel, buttonClassName, source }) => {
  const inputEl = useRef(null);
  const handleSubmitEmail = () => submitEmail(status, setStatus, setErrorMsg, inputEl, email, source);

  return (
    <div className={`${styles.field} ${status === `loading` && styles.fieldLoading} ${status === `success` && styles.fieldSuccess}`}>
      <input
        ref={inputEl}
        type="email"
        placeholder={placeholder}
        className={`${styles.input} ${status === `failure` && styles.inputFailure}`}
        value={email}
        disabled={[`loading`, `success`].includes(status)}
        onChange={(e) => setEmail(e.target.value)} onKeyDown={(e) => e.keyCode === 13 && handleSubmitEmail()}
      />
      <div className={`${styles.submit} ${buttonClassName} ${status === `success` && styles.submitSuccess}`} onClick={handleSubmitEmail} role="none">
        {status !== `loading` && <>{buttonLabel}&nbsp;&nbsp;➞</>}
        {status === `loading` && <>...</>}
      </div>
    </div>
  );
};

const EmailSignUp = ({ placeholder, buttonLabel, buttonClassName, failureMsgClassName, successMsgClassName, source, className }) => {
  if (!source) throw new Error(`Email signup source must be specified`);

  const [email, setEmail] = useState(``);
  const [status, setStatus] = useState(`ready`);
  const [errorMsg, setErrorMsg] = useState(null);

  return (
    <div className={`${styles.emailSignUp} ${className}`}>
      <EmailField
        email={email}
        setEmail={setEmail}
        status={status}
        setStatus={setStatus}
        setErrorMsg={setErrorMsg}
        placeholder={placeholder}
        buttonLabel={buttonLabel}
        buttonClassName={buttonClassName}
        source={source} />
      <div className={styles.resultMsg}>
        {status === `failure` && <p className={`${styles.failureMsg} ${failureMsgClassName}`}>✘ {errorMsg}</p>}
        {status === `success` && <p className={`${styles.successMsg} ${successMsgClassName}`}>✔︎ Thanks for signing up for our {source} email list.</p>}
      </div>
    </div>
  );
};

EmailSignUp.propTypes = {
  buttonClassName: PropTypes.string,
  buttonLabel: PropTypes.string,
  className: PropTypes.string,
  placeholder: PropTypes.string,
};

EmailSignUp.defaultProps = {
  buttonClassName: ``,
  buttonLabel: `Join`,
  className: ``,
  placeholder: `someone@email.com`,
};

export default EmailSignUp;
