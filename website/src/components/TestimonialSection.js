import Img from 'gatsby-image';
import PropTypes from 'prop-types';
import React, { useState, useRef, useEffect } from 'react';

import queries from '../queries';
import Quote from '../images/vector/foreground/quotes.svg';

import styles from './TestimonialSection.module.scss';

const TestimonialCard = ({ visible, data }) => {
  const { photo } = data.frontmatter;

  return (
    <div className={`${styles.slide} ${visible && styles.slideVisible}`}>
      <Quote className={styles.quoteLeft} />
      <div className={styles.topLine}>
        <div className={styles.photoContainer}>
          {photo && <Img fluid={photo.childImageSharp.fluid} className={styles.photoWrapper} objectFit="cover" objectPosition="center" />}
        </div>
        <div className={styles.info}>
          <div className={styles.name}>{data.frontmatter.name}</div>
          <div className={styles.job}>{data.frontmatter.job}</div>
        </div>
      </div>
      <div className={styles.text}>{data.frontmatter.text}</div>
      <Quote className={styles.quoteRight} />
    </div>
  );
};

const SliderDot = ({ visible, onClick }) => (
  <div className={`${styles.sliderDot} ${visible && styles.sliderDotOn}`} onClick={onClick} role="none"></div>
);

const TestimonialSection = ({ className, filterClientTypes, autoChangeMs }) => {
  const [visibleCard, setVisibleCard] = useState(0);
  const [lastClickTime, setLastClickTime] = useState(1);
  const refVisibleCard = useRef(visibleCard);
  refVisibleCard.current = visibleCard;
  const refLastClickTime = useRef(lastClickTime);
  refLastClickTime.current = lastClickTime;
  const testimonials = queries.testimonials(filterClientTypes);
  const maxCardIndex = testimonials.length - 1;

  function selectCard(index) {
    setVisibleCard(index);
    setLastClickTime(Date.now());
  }

  function userSelectedRecently() {
    return (refLastClickTime.current + (autoChangeMs * 2) >= Date.now());
  }

  useEffect(() => {
    const timer = setInterval(() => {
      if (userSelectedRecently()) return;
      const nextIndex = refVisibleCard.current + 1;
      setVisibleCard(nextIndex > maxCardIndex ? 0 : nextIndex);
    }, autoChangeMs);
    return () => clearInterval(timer);
  });

  return (
    <section className={`${styles.section} ${className}`}>
      <h3>Testimonials</h3>
      <div className={styles.slider}>
        {testimonials.map((item, index) => <TestimonialCard key={index} visible={visibleCard === index} data={item} />)}
        {testimonials.map((_, index) => <SliderDot key={index} visible={visibleCard === index} onClick={() => selectCard(index)} />)}
      </div>
    </section>
  );
};

TestimonialSection.propTypes = {
  autoChangeMs: PropTypes.number,
  className: PropTypes.string,
  filterClientTypes: PropTypes.arrayOf(PropTypes.string),
};

TestimonialSection.defaultProps = {
  autoChangeMs: (1000 * 15),
  className: ``,
  filterClientTypes: [],
};

export default TestimonialSection;
