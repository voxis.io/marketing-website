// import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';

import imgFeatures from '../images/vector/foreground/features';

import styles from './FeatureCard.module.scss';

const FeatureCard = ({ className, data }) => {
  const ImgFeature = imgFeatures[data.frontmatter.image];

  return (
    // <Link to={data.fields.slug} className={styles.featureCard}>
    <div className={styles.featureCard}>
      {ImgFeature && <ImgFeature className={styles.image} />}
      <div className={styles.title}>{data.frontmatter.title}</div>
      <div className={styles.subTitle}>{data.frontmatter.subTitle}</div>
    </div>
    // </Link>
  );
};

FeatureCard.propTypes = {
  className: PropTypes.string,
};

FeatureCard.defaultProps = {
  className: ``,
};

export default FeatureCard;
