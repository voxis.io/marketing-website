/**
 * Seo component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import { useStaticQuery, graphql } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';
import { Helmet } from 'react-helmet';

import ShareImage from '../../static/social/share-image.png';

function Seo({ title, description, author }) {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            titleShort
            description
            author
            siteUrl
            socialIds {
              twitter
            }
          }
        }
      }
    `,
  );

  const pageTitle = `${title ? `${title} | ` : ``}${site.siteMetadata.title}`;
  const shareTitle = (title ? `${title} | ${site.siteMetadata.titleShort}` : site.siteMetadata.title);
  const metaDescription = description || site.siteMetadata.description;
  const metaAuthor = author || site.siteMetadata.author;

  return (
    <Helmet>
      <html lang="en-GB" />

      {/* General */}
      <title>{pageTitle}</title>
      <meta name="description" content={metaDescription} />
      <meta name="author" content={metaAuthor} />

      {/* Facebook */}
      <meta property="og:type" content="website" />
      <meta property="og:url" content={site.siteMetadata.siteUrl} />
      <meta property="og:title" content={shareTitle} />
      <meta property="og:description" content={metaDescription} />
      <meta property="og:image" content={ShareImage} />

      {/* Twitter */}
      <meta property="twitter:card" content="summary_large_image" />
      <meta property="twitter:creator" content={site.siteMetadata.socialIds.twitter} />
      <meta property="twitter:title" content={shareTitle} />
      <meta property="twitter:description" content={metaDescription} />
      <meta property="twitter:image" content={ShareImage} />
    </Helmet>
  );
}

Seo.defaultProps = {
  title: ``,
  description: ``,
  author: ``,
};

Seo.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  author: PropTypes.string,
};

export default Seo;
