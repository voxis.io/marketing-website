import moment from 'moment';
import PropTypes from 'prop-types';
import React from 'react';

import styles from './FaqItem.module.scss';

const FaqItem = ({ className, data }) => {
	const { frontmatter: { question, lastUpdated }, fields: { slug }, html } = data;
	const [, , category, id] = slug.split(`/`);
	const anchorTag = `${category}-${id}`;

	return (
		<div id={anchorTag} className={`${styles.faqItem} ${className}`}>
			<a href={`#${anchorTag}`} className={styles.question}>{question}</a>
			<div className={styles.body}>
				<div className={styles.answer} dangerouslySetInnerHTML={{ __html: html }} />
				<div className={styles.lastUpdated}>Last updated: {moment(lastUpdated).format(`D MMM, YYYY`)}</div>
			</div>
		</div>
	);
};

FaqItem.propTypes = {
	className: PropTypes.string,
};

FaqItem.defaultProps = {
	className: ``,
};

export default FaqItem;
