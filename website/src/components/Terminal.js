import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';

import styles from './Terminal.module.scss';

const AnimatableUserInput = ({ userInput, visibleCharIndex, isActive, isComplete }) => {
	const chars = userInput.split(``);
	const html = chars.map((char, index) => isComplete || index <= visibleCharIndex ? `<span>${char}</span>` : ``).join(``).replace(/\s/g, `&nbsp;`);

	return (
		<span className={styles.terminalInput}>
			<span dangerouslySetInnerHTML={{ __html: html }}></span>
			{isActive && <span className={styles.terminalInputCursor}></span>}
		</span>
	);
};

const ColouredText = ({ text }) => {
	const html = text
		.replace(/\s/g, `&nbsp;`)
		.replace(/<col:cyan>/g, `<span class="${styles.terminalColour}" colour="cyan">`)
		.replace(/<col:green>/g, `<span class="${styles.terminalColour}" colour="green">`)
		.replace(/<\/col> /g, `</span>`);

	return (
		<span dangerouslySetInnerHTML={{ __html: html || `&nbsp;` }}></span>
	);
};

const markLineComplete = (lineStates, lineIndex, setLineStates) => {
	const newLineStates = lineStates.map((lineState, index) => ({ ...lineState, complete: index === lineIndex ? true : lineState.complete }));
	setLineStates(newLineStates);
};

const resetAllLineStates = (defaultLineStates, setLineStates) => {
	const newLineStates = defaultLineStates.map(lineState => ({ ...lineState }));
	setLineStates(newLineStates);

};

const TerminalLine = ({
	lineIndex,
	details,
	defaultPromptSymbol,
	defaultDir,
	defaultBranch,
	lineStates,
	setLineStates,
	charPrintSpeed,
	startInputPrintPause,
	endInputPrintPause,
	visible,
	isActive,
}) => {
	const curLineState = lineStates[lineIndex];
	const [visibleCharIndex, setVisibleCharIndex] = useState(-1);
	const [printPauseQty, setPrintPauseQty] = useState(0);
	const maxCharIndex = details.userInput ? details.userInput.length : 0;
	const startInputPrintPauseMaxQty = Math.ceil(startInputPrintPause / charPrintSpeed);
	const endInputPrintPauseMaxQty = Math.ceil(endInputPrintPause / charPrintSpeed);
	const visibleLineCls = visible ? styles.terminalLineVisible : ``;

	useEffect(() => {
		const timer = setInterval(() => {
			if (!isActive) return;
			if (curLineState.complete) return;

			if (!maxCharIndex) {
				markLineComplete(lineStates, lineIndex, setLineStates);
				return;
			};

			if (visibleCharIndex === -1 && printPauseQty < startInputPrintPauseMaxQty) {
				setPrintPauseQty(printPauseQty + 1);
				return;
			} else if (visibleCharIndex === -1 && printPauseQty >= startInputPrintPauseMaxQty) {
				setVisibleCharIndex(0);
				setPrintPauseQty(0);
				return;
			} else if (visibleCharIndex >= maxCharIndex && printPauseQty < endInputPrintPauseMaxQty) {
				setPrintPauseQty(printPauseQty + 1);
				return;
			} else if (visibleCharIndex >= maxCharIndex && printPauseQty >= endInputPrintPauseMaxQty) {
				setVisibleCharIndex(-1);
				setPrintPauseQty(0);
				markLineComplete(lineStates, lineIndex, setLineStates);
				return;
			}

			const nextIndex = visibleCharIndex + 1;
			setVisibleCharIndex(nextIndex);
		}, charPrintSpeed);
		return () => clearInterval(timer);
	});

	switch (details.type) {
		case `shell-prompt`:
			return (
				<div className={`${styles.terminalLineWithPrompt} ${visibleLineCls}`}>
					<span className={styles.terminalPromptText}>
						{details.promptSymbol || defaultPromptSymbol}&nbsp;
						<span className={styles.terminalColour} colour="green">{details.dir || defaultDir}</span>&nbsp;
						<span className={styles.terminalColour} colour="dim">({details.branch || defaultBranch})</span>
					</span>
					<AnimatableUserInput userInput={details.userInput} visibleCharIndex={visibleCharIndex} isActive={isActive} isComplete={curLineState.complete} />
				</div>
			);

		case `app-prompt`:
			return (
				<div className={`${styles.terminalLine} ${visibleLineCls}`}>
					<ColouredText text={details.print} />&nbsp;
					<AnimatableUserInput userInput={details.userInput} visibleCharIndex={visibleCharIndex} isActive={isActive} isComplete={curLineState.complete} />
				</div>
			);

		case `app-output`:
			return (
				<div className={`${styles.terminalLine} ${visibleLineCls}`}>
					<ColouredText text={details.print} />
				</div>
			);

		default:
			throw new Error(`Invalid terminal line type "${details.type}"`);
	}
};

const Terminal = ({
	className,
	windowClassName,
	lines,
	windowTitle = `Terminal`,
	defaultPromptSymbol = `$`,
	defaultDir = `~`,
	defaultBranch = `master`,
	linePrintSpeed = 50,
	charPrintSpeed = 100,
	startLinePrintPause = 1000,
	endLinePrintPause = 5000,
	startInputPrintPause = 500,
	endInputPrintPause = 1000,
}) => {
	const defaultLineStates = lines.map(() => ({ complete: false }));
	const [lineStates, setLineStates] = useState(defaultLineStates);
	const [visibleLineIndex, setVisibleLineIndex] = useState(-1);
	const [printPauseQty, setPrintPauseQty] = useState(0);
	const maxLineIndex = lines.length - 1;
	const startLinePrintPauseMaxQty = Math.ceil(startLinePrintPause / linePrintSpeed);
	const endLinePrintPauseMaxQty = Math.ceil(endLinePrintPause / linePrintSpeed);

	useEffect(() => {
		const timer = setInterval(() => {
			if (visibleLineIndex === -1 && printPauseQty < startLinePrintPauseMaxQty) {
				setPrintPauseQty(printPauseQty + 1);
				return;
			} else if (visibleLineIndex === -1 && printPauseQty >= startLinePrintPauseMaxQty) {
				setVisibleLineIndex(0);
				setPrintPauseQty(0);
				return;
			} else if (visibleLineIndex >= maxLineIndex && printPauseQty < endLinePrintPauseMaxQty) {
				setPrintPauseQty(printPauseQty + 1);
				return;
			} else if (visibleLineIndex >= maxLineIndex && printPauseQty >= endLinePrintPauseMaxQty) {
				setVisibleLineIndex(-1);
				setPrintPauseQty(0);
				resetAllLineStates(defaultLineStates, setLineStates);
				return;
			}

			const curLineState = lineStates[visibleLineIndex];
			if (!curLineState.complete) return;

			const nextIndex = visibleLineIndex + 1;
			setVisibleLineIndex(nextIndex);

		}, linePrintSpeed);
		return () => clearInterval(timer);
	});

	const preparedLines = lines.map((line, lineIndex) => {

		return (
			<TerminalLine
				key={lineIndex}
				lineIndex={lineIndex}
				details={line}
				defaultPromptSymbol={defaultPromptSymbol}
				defaultDir={defaultDir}
				defaultBranch={defaultBranch}
				charPrintSpeed={charPrintSpeed}
				startInputPrintPause={startInputPrintPause}
				endInputPrintPause={endInputPrintPause}
				lineStates={lineStates}
				setLineStates={setLineStates}
				visible={lineIndex <= visibleLineIndex}
				isActive={lineIndex === visibleLineIndex}
			/>
		);
	});

	return (
		<div className={`${styles.terminalContainer} ${className}`}>
			<div className={`${styles.terminalWindow} ${windowClassName}`}>
				<div className={styles.windowTitleBar}>
					<div className={styles.windowOrb}></div>
					<div className={styles.windowOrb}></div>
					<div className={styles.windowOrb}></div>
					<div className={styles.windowTitleText}>{windowTitle}</div>
				</div>
				<div className={styles.windowBody}>
					{preparedLines}
				</div>
			</div>
		</div>
	);
};

Terminal.propTypes = {
	className: PropTypes.string,
};

Terminal.defaultProps = {
	className: ``,
};

export default Terminal;
