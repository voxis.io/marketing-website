import PropTypes from 'prop-types';
import React, { useState, useRef, useEffect } from 'react';

import FlexBox from './layout/FlexBox';
import FlexCell from './layout/FlexCell';

import imgFeatures from '../images/vector/foreground/features';
import icons from '../images/vector/foreground/icons';
import queries from '../queries';

import styles from './WhatSection.module.scss';

const { IconArrowRightSolid } = icons;

const FeatureTitle = ({ data, number, visible, onClick }) => (
  <div className={`${styles.featureTitle} ${visible && styles.featureTitleSelected}`} onClick={onClick} role="none">
    <div className={styles.number}>{`${number}`.padStart(2, `0`)}</div>
    <div className={styles.text}>{data.frontmatter.title}</div>
    <div className={styles.arrowCell}>
      <IconArrowRightSolid className={`${styles.arrowIcon} ${visible && styles.arrowIconVisible}`} />
    </div>
  </div>
);

const FeatureCard = ({ data, visible }) => {
  const ImgFeature = imgFeatures[data.frontmatter.image];

  return (
    <div className={`${styles.featureCard} ${visible && styles.featureCardVisible}`}>
      {ImgFeature && <ImgFeature className={styles.image} />}
      <div className={styles.description}>{data.frontmatter.subTitle}</div>
    </div>
  );
};

const WhatSection = ({ className, autoChangeMs }) => {
  const [visibleCard, setVisibleCard] = useState(0);
  const [lastClickTime, setLastClickTime] = useState(1);
  const refVisibleCard = useRef(visibleCard);
  refVisibleCard.current = visibleCard;
  const refLastClickTime = useRef(lastClickTime);
  refLastClickTime.current = lastClickTime;
  const features = queries.features();
  const maxCardIndex = features.length - 1;

  function selectCard(index) {
    setVisibleCard(index);
    setLastClickTime(Date.now());
  }

  function userSelectedRecently() {
    return (refLastClickTime.current + (autoChangeMs * 2) >= Date.now());
  }

  useEffect(() => {
    const timer = setInterval(() => {
      if (userSelectedRecently()) return;
      const nextIndex = refVisibleCard.current + 1;
      setVisibleCard(nextIndex > maxCardIndex ? 0 : nextIndex);
    }, autoChangeMs);
    return () => clearInterval(timer);
  });

  return (
    <section className={`${styles.section} ${className}`}>
      <h3>What is Voxis</h3>
      <FlexBox direction="row" className={styles.featureList}>
        <FlexCell flex="1">
          {features.map((feature, index) => <FeatureTitle key={index} data={feature} number={index + 1} visible={visibleCard === index} onClick={() => selectCard(index)} />)}
        </FlexCell>
        <FlexCell flex="2">
          {features.map((feature, index) => <FeatureCard key={index} data={feature} visible={visibleCard === index} />)}
        </FlexCell>
      </FlexBox>
    </section>
  );
};

WhatSection.propTypes = {
  className: PropTypes.string,
  autoChangeMs: PropTypes.number,
};

WhatSection.defaultProps = {
  className: ``,
  autoChangeMs: (1000 * 15),
};

export default WhatSection;
