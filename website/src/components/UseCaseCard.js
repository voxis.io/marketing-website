import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';

import icons from '../images/vector/foreground/icons';
import imgUseCases from '../images/vector/foreground/use-cases';

import styles from './UseCaseCard.module.scss';

const { IconArrowRightSolid } = icons;

const UseCaseCard = ({ className, data }) => {
  const ImgUseCase = imgUseCases[data.frontmatter.image];

  return (
    <Link to={data.fields.slug} className={styles.useCaseCard}>
      <div className="topLine">
        {ImgUseCase && <ImgUseCase className="image" />}
        <div className="title">{data.frontmatter.titlePlural}</div>
        <div className="arrow"><IconArrowRightSolid /></div>
      </div>
      <div className="subTitle">{data.frontmatter.subTitle}</div>
    </Link>
  );
};

UseCaseCard.propTypes = {
  className: PropTypes.string,
};

UseCaseCard.defaultProps = {
  className: ``,
};

export default UseCaseCard;
