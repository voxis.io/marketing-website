import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';

import icons from '../images/vector/foreground/icons';

import styles from './Button.module.scss';

const ButtonInner = ({ icon, children }) => {
  const Icon = icons[icon];

  return (
    <>
      {icon && <Icon className={styles.icon} />}
      <span className={styles.label}>{children}</span>
    </>
  );
};

const Button = ({ className, to, onClick, staticFile, staticMode, icon, children }) => {
  if (to) {
    return (
      <Link to={to} className={`${styles.button} ${className}`}>
        <ButtonInner icon={icon} children={children} />
      </Link>
    );
  } else if (onClick) {
    return (
      <div onClick={onClick} className={`${styles.button} ${className}`} role="none">
        <ButtonInner icon={icon} children={children} />
      </div>
    );
  } else if (staticFile) {
    const downloadAttr = staticMode === `download`;
    const targetAttr = staticMode === `external` ? `_blank` : undefined;

    return (
      <a href={staticFile} className={`${styles.button} ${className}`} download={downloadAttr} target={targetAttr}>
        <ButtonInner icon={icon} children={children} />
      </a>
    );
  } else {
    throw new Error(`One of "to", "onClick", or "staticFile" must be passed to <Button>`);
  }
};

Button.propTypes = {
  className: PropTypes.string,
  to: PropTypes.string,
  icon: PropTypes.string,
  children: PropTypes.node.isRequired,
};

Button.defaultProps = {
  className: ``,
  to: ``,
  icon: ``,
  children: ``,
};

export default Button;
