import PropTypes from 'prop-types';
import React from 'react';

import Button from './Button';
import FlexBox from './layout/FlexBox';
import FlexCell from './layout/FlexCell';

import styles from './ShareSection.module.scss';

const scrollToTop = () => typeof window !== `undefined` && window.scrollTo({ top: 0, behavior: `smooth` });

const ShareSection = ({ className }) => {
  return (
    <section className={`${styles.section} ${className}`}>
      <FlexBox direction="row" className={styles.flexer}>
        <FlexCell className={styles.cellLeft}>
          <Button className={styles.backToTopBtn} icon="IconArrowUpSolid" onClick={scrollToTop}>Back to top</Button>
        </FlexCell>
        <FlexCell className={styles.cellRight}>
          <div className={`${styles.shareThisWidget} sharethis-inline-share-buttons`}></div>
        </FlexCell>
      </FlexBox>
    </section>
  );
};

ShareSection.propTypes = {
  className: PropTypes.string,
};

ShareSection.defaultProps = {
  className: ``,
};

export default ShareSection;
