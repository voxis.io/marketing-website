import PropTypes from 'prop-types';
import React from 'react';

import styles from './FlexBox.module.scss';

const FlexBox = ({ className, direction, children }) => {
  return (
    <div className={`${styles.flex} ${styles[direction]} ${className}`}>
      {children}
    </div>
  );
};

FlexBox.propTypes = {
  className: PropTypes.string,
  direction: PropTypes.string,
  children: PropTypes.node.isRequired,
};

FlexBox.defaultProps = {
  className: ``,
  direction: `column`,
};

export default FlexBox;
