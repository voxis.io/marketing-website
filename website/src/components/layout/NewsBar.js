import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';

import styles from './NewsBar.module.scss';

function __getThemedthemedStyles(styles, theme) {
  const themeNameFormatted = `${theme[0].toUpperCase()}${theme.substr(1)}`;
  return {
    ...styles,
    newsBar: `${styles.newsBar} ${styles[`newsBar${themeNameFormatted}`]}`,
  };
}

const NewsBar = ({ className, theme }) => {
  const themedStyles = __getThemedthemedStyles(styles, theme);

  return (
    <div className={`${themedStyles.newsBar} ${className}`}>
      <Link to="/preview">
        <strong>Voxis Preview</strong> is launching soon...
        <span className={themedStyles.divider}>|</span>
        Be first ⟶
      </Link>
    </div>
  );
};

NewsBar.propTypes = {
  className: PropTypes.string,
  theme: PropTypes.string,
};

NewsBar.defaultProps = {
  className: ``,
  theme: `dark`,
};

export default NewsBar;
