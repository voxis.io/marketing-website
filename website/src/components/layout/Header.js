import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

import FlexBox from './FlexBox';
import FlexCell from './FlexCell';
import HeaderLink from './HeaderLink';
import NewsBar from './NewsBar';
// import SubHeaderLink from './SubHeaderLink';
// import SubMenu from './SubMenu';

import icons from '../../images/vector/foreground/icons';
// import queries from '../../queries';
import LogoDark from '../../images/vector/foreground/voxis-logo-horizontal-dark.svg';
import LogoLight from '../../images/vector/foreground/voxis-logo-horizontal-light.svg';

import styles from './Header.module.scss';

const { IconBurgerMenu } = icons;

// function __preparePlatformMenuColumn(themedStyles, nodes, columnNumber) {
//   return nodes.map((node, index) => {
//     if (index % 3 !== columnNumber) {
//       return ``;
//     }
//     return (
//       <SubHeaderLink
//         key={node.id}
//         to={node.fields.slug}
//         icon={node.frontmatter.icon}
//         title={node.frontmatter.title}
//         subTitle={node.frontmatter.subTitle}
//         className={themedStyles.subMenuLink}
//       />
//     );
//   });
// }

function __getThemedthemedStyles(styles, theme) {
  const themeNameFormatted = `${theme[0].toUpperCase()}${theme.substr(1)}`;
  return {
    ...styles,
    header: `${styles.header} ${styles[`header${themeNameFormatted}`]}`,
    link: `${styles.link} ${styles[`link${themeNameFormatted}`]}`,
    joinBtn: `${styles.joinBtn} ${styles[`joinBtn${themeNameFormatted}`]}`,
    burgerMenuBtn: `${styles.burgerMenuBtn} ${styles[`burgerMenuBtn${themeNameFormatted}`]}`,
  };
}

const PrimaryNavLinks = ({ themedStyles, visibleSubMenu, setVisibleSubMenu }) => {
  return (
    <>
      <HeaderLink to="/use-cases" onMouseEnter={() => setVisibleSubMenu(`use-cases`)} caret={true} on={visibleSubMenu === `use-cases`} className={themedStyles.link}>Use Cases</HeaderLink>
      <HeaderLink to="/platform" onMouseEnter={() => setVisibleSubMenu(`platform`)} caret={true} on={visibleSubMenu === `platform`} className={themedStyles.link}>Platform</HeaderLink>
      {/* <HeaderLink to="/enterprise" onMouseEnter={() => setVisibleSubMenu(``)} className={themedStyles.link}>Enterprise</HeaderLink> */}
      <HeaderLink to="/pricing" onMouseEnter={() => setVisibleSubMenu(``)} className={themedStyles.link}>Pricing</HeaderLink>
      <HeaderLink to="/contact" onMouseEnter={() => setVisibleSubMenu(``)} className={themedStyles.link}>Talk to Us</HeaderLink>
    </>
  );
};

const SecondaryNavLinks = ({ themedStyles, setVisibleSubMenu }) => {
  return (
    <>
      <HeaderLink to="/learn" onMouseEnter={() => setVisibleSubMenu(``)} className={themedStyles.link}>Learn</HeaderLink>
      {/* <HeaderLink to="https://gitlab.com/voxis.io/docs/-/wikis/home" onMouseEnter={() => setVisibleSubMenu(``)} className={themedStyles.link}>Docs</HeaderLink> */}
      {/* <HeaderLink to="https://app.voxis.io" onMouseEnter={() => setVisibleSubMenu(``)} className={themedStyles.link}>Login</HeaderLink> */}
      <HeaderLink to="/start" onMouseEnter={() => setVisibleSubMenu(``)} className={themedStyles.joinBtn}>Get Started Free</HeaderLink>
    </>
  );
};

const Header = function ({ className, theme }) {
  const [visibleSubMenu, setVisibleSubMenu] = useState(null);
  const [mobileMenuVisible, setMobileMenuVisible] = useState(false);
  // const features = queries.features();
  const themedStyles = __getThemedthemedStyles(styles, theme);

  return (
    <>
      <NewsBar theme={theme} />
      <header className={themedStyles.header}>
        <div className={`${themedStyles.headerBar} ${className}`}>
          <FlexBox className={themedStyles.flexBox} direction="row">
            <FlexCell className={themedStyles.branding}>
              <Link to="/" className={themedStyles.logoLink} onMouseEnter={() => setVisibleSubMenu(``)}>
                {theme === `dark` && <LogoLight className={themedStyles.logoImage} />}
                {theme === `light` && <LogoDark className={themedStyles.logoImage} />}
              </Link>
            </FlexCell>
            <FlexCell className={themedStyles.primaryNav}>
              <PrimaryNavLinks themedStyles={themedStyles} visibleSubMenu={visibleSubMenu} setVisibleSubMenu={setVisibleSubMenu} />
            </FlexCell>
            <FlexCell className={themedStyles.secondaryNav}>
              <SecondaryNavLinks themedStyles={themedStyles} visibleSubMenu={visibleSubMenu} setVisibleSubMenu={setVisibleSubMenu} />
            </FlexCell>
            <FlexCell className={themedStyles.burgerMenuContainer}>
              <div className={themedStyles.burgerMenuBtn} onClick={() => setMobileMenuVisible(!mobileMenuVisible)} role="none">
                <IconBurgerMenu />
              </div>
            </FlexCell>
          </FlexBox>
        </div>
        <div className={`${themedStyles.mobileMenu} ${mobileMenuVisible && themedStyles.mobileMenuVisible}`}>
          <div className={themedStyles.primaryNavMobile}>
            <PrimaryNavLinks themedStyles={themedStyles} visibleSubMenu={visibleSubMenu} setVisibleSubMenu={setVisibleSubMenu} />
          </div>
          <div className={themedStyles.secondaryNavMobile}>
            <SecondaryNavLinks themedStyles={themedStyles} visibleSubMenu={visibleSubMenu} setVisibleSubMenu={setVisibleSubMenu} />
          </div>
        </div>
        {/* <SubMenu visible={visibleSubMenu === `platform`} onMouseLeave={() => setVisibleSubMenu(``)}>
          <FlexBox direction="row">
            <FlexCell className={themedStyles.subMenuColumn}>
              {__preparePlatformMenuColumn(themedStyles, features, 0)}
            </FlexCell>
            <FlexCell className={themedStyles.subMenuColumn}>
              {__preparePlatformMenuColumn(themedStyles, features, 1)}
            </FlexCell>
            <FlexCell className={themedStyles.subMenuColumn}>
              {__preparePlatformMenuColumn(themedStyles, features, 2)}
            </FlexCell>
          </FlexBox>
        </SubMenu>
        <SubMenu visible={visibleSubMenu === `use-cases`} onMouseLeave={() => setVisibleSubMenu(``)}>
          <FlexBox direction="row">
            <FlexCell className={themedStyles.subMenuColumn}>
            <SubHeaderLink
                to="/use-cases/static-websites"
                icon="IconStaticWebsite"
                title="Static websites"
                subTitle="Deploy and forget your static website assets for blazing fast load times."
                className={themedStyles.subMenuLink}
              />
            <SubHeaderLink
                to="/use-cases/backend-api"
                icon="IconBackendApi"
                title="Backend APIs"
                subTitle="Build APIs as always-on microservices or short-lived nanoservices."
                className={themedStyles.subMenuLink}
              />
              <SubHeaderLink
                to="/use-cases/mobile-game-backends"
                icon="IconMobileGameBackend"
                title="Mobile game backends"
                subTitle="Minimise the cost and complexity of your backend with serverless functions."
                className={themedStyles.subMenuLink}
              />
            </FlexCell>
            <FlexCell className={themedStyles.subMenuColumn}>
              <SubHeaderLink
                to="/use-cases/web-apps"
                icon="IconWebApp"
                title="Web apps"
                subTitle="Host your web app assets statically and provide interactivity via an API."
                className={themedStyles.subMenuLink}
              />
            <SubHeaderLink
                to="/use-cases/mobile-app-apis"
                icon="IconMobileAppApi"
                title="Mobile app APIs"
                subTitle="Low code, plug-and-play functionality for common mobile app requirements."
                className={themedStyles.subMenuLink}
              />
            <SubHeaderLink
                to="/use-cases/internet-of-things"
                icon="IconIot"
                title="Internet of things"
                subTitle="Serverless is the right choice for high-volume, low-size data transfers."
                className={themedStyles.subMenuLink}
              />
            </FlexCell>
          </FlexBox>
        </SubMenu> */}
      </header>
    </>
  );
};

Header.propTypes = {
  className: PropTypes.string,
  theme: PropTypes.string,
};

Header.defaultProps = {
  className: ``,
  theme: `dark`,
};

export default Header;
