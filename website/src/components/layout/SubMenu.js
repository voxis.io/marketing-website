import PropTypes from 'prop-types';
import React from 'react';

import SiteWidth from './SiteWidth';

import styles from './SubMenu.module.scss';

const SubMenu = ({ className, visible, onMouseLeave, children }) => (
  <div className={`${styles.subMenu} ${visible && styles.visible} ${className}`} onMouseLeave={onMouseLeave} role="none">
    <SiteWidth className={styles.siteWidth}>
      {children}
    </SiteWidth>
  </div>
);

SubMenu.propTypes = {
  className: PropTypes.string,
  visible: PropTypes.bool,
  onMouseLeave: PropTypes.func,
  children: PropTypes.node.isRequired,
};

SubMenu.defaultProps = {
  className: ``,
  visible: false,
};

export default SubMenu;
