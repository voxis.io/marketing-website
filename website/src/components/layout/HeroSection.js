import PropTypes from 'prop-types';
import React from 'react';

import styles from './HeroSection.module.scss';

const HeroSection = ({ children, className }) => (
  <section className={`${styles.section} ${className}`}>
    {children}
  </section>
);

HeroSection.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
};

HeroSection.defaultProps = {
  className: ``,
};

export default HeroSection;
