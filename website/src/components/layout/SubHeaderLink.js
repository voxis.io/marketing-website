import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';

import FlexBox from './FlexBox';
import FlexCell from './FlexCell';

import icons from '../../images/vector/foreground/icons';

import styles from './SubHeaderLink.module.scss';

const Inner = ({ icon, title, subTitle }) => {
  const Icon = icons[icon];

  return (
    <FlexBox direction="row" className={styles.flexBox}>
      {icon && <FlexCell className={styles.iconCell}>
        <Icon className={styles.icon} />
      </FlexCell>}
      <FlexCell>
        <div className={styles.title}>{title}</div>
        <div className={styles.subTitle}>{subTitle}</div>
      </FlexCell>
    </FlexBox>
  );
};

const SubHeaderLink = ({ className, to, icon, title, subTitle }) => {
  const isExternalLink = to.match(/^https?:\/\//);

  if (isExternalLink) {
    return (
      <a href={to} target="_blank" rel="noopener noreferrer" className={`${styles.link} ${className}`}>
        <Inner icon={icon} title={title} subTitle={subTitle} />
      </a>
    );
  }
  else {
    return (
      <Link to={to} className={`${styles.link} ${className}`}>
        <Inner icon={icon} title={title} subTitle={subTitle} />
      </Link>
    );
  }
};

SubHeaderLink.propTypes = {
  className: PropTypes.string,
  to: PropTypes.string,
  icon: PropTypes.string,
  title: PropTypes.string,
  subTitle: PropTypes.string,
};

SubHeaderLink.defaultProps = {
  className: ``,
  to: ``,
  icon: ``,
  title: ``,
  subTitle: ``,
};

export default SubHeaderLink;
