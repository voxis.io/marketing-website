import PropTypes from 'prop-types';
import React from 'react';

import styles from './FlexCell.module.scss';

const FlexCell = ({ className, flex, children }) => {
  const style = { flex: flex || void (0) };

  return (
    <div className={`${styles.cell} ${className}`} style={style}>
      {children}
    </div>
  );
};

FlexCell.propTypes = {
  className: PropTypes.string,
  flex: PropTypes.string,
  children: PropTypes.node.isRequired,
};

FlexCell.defaultProps = {
  className: ``,
  flex: ``,
};

export default FlexCell;
