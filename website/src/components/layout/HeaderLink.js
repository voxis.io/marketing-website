import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';

import icons from '../../images/vector/foreground/icons';

import styles from './HeaderLink.module.scss';

const { /*IconCaret,*/ IconExternalLink } = icons;

const Inner = ({ caret, external, children }) => (
  <>
    <span className={styles.label}>{children}</span>
    {/* {caret && <IconCaret className={styles.caret} />} */}
    {external && <IconExternalLink className={styles.caret} />}
  </>
);

const HeaderLink = ({ className, to, onMouseEnter, caret, on, children }) => {
  const isActive = (typeof window !== `undefined` && window.location.pathname.length > 1 && window.location.pathname.startsWith(to));
  const isExternalLink = to.match(/^https?:\/\//);

  return (
    <div className={`${styles.headerLink} ${on && styles.on} ${className}`} onMouseEnter={onMouseEnter} role="none">
      {isExternalLink
        ? <a href={to} className={`${styles.link} ${isActive && styles.active}`} target="_blank" rel="noopener noreferrer"><Inner caret={caret} external={true}>{children}</Inner></a>
        : <Link to={to} className={`${styles.link} ${isActive && styles.active}`}><Inner caret={caret}>{children}</Inner></Link>
      }
    </div>
  );
};

HeaderLink.propTypes = {
  className: PropTypes.string,
  to: PropTypes.string,
  onMouseEnter: PropTypes.func,
  caret: PropTypes.bool,
  on: PropTypes.bool,
  children: PropTypes.string,
};

HeaderLink.defaultProps = {
  className: ``,
  to: ``,
  caret: false,
  on: false,
  children: ``,
};

export default HeaderLink;
