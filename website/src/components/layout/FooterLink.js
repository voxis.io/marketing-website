import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';

import icons from '../../images/vector/foreground/icons';

import styles from './FooterLink.module.scss';

const { IconExternalLink } = icons;

const Inner = ({ external, children }) => (
  <>
    <span className={styles.label}>{children}</span>
    {external && <IconExternalLink className={styles.externalIcon} />}
  </>
);

const FooterLink = ({ className, to, children }) => {
  const isExternalLink = to.match(/^https?:\/\//);

  return (
    <div className={`${styles.footerLink} ${className}`} role="none">
      {isExternalLink
        ? <a href={to} target="_blank" rel="noopener noreferrer"><Inner external={true}>{children}</Inner></a>
        : <Link to={to}><Inner>{children}</Inner></Link>
      }
    </div>
  );
};

FooterLink.propTypes = {
  className: PropTypes.string,
  to: PropTypes.string,
  children: PropTypes.node.isRequired,
};

FooterLink.defaultProps = {
  className: ``,
  to: ``,
  children: ``,
};

export default FooterLink;
