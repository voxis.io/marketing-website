import PropTypes from 'prop-types';
import React from 'react';

import EmailSignUp from '../EmailSignUp';

import styles from './NewsletterSection.module.scss';

const NewsletterSection = ({ className }) => (
  <section className={`${styles.section} ${className}`}>
    <div className={styles.copy}>
      Stay up to date with everything happening at Voxis by subscribing to our newsletter
    </div>
    <EmailSignUp
      source="newsletter"
      buttonLabel="Join"
      className={styles.emailField}
      buttonClassName={styles.emailButton}
      successMsgClassName={styles.successMsg}
    />
  </section>
);

NewsletterSection.propTypes = {
  className: PropTypes.string,
};

NewsletterSection.defaultProps = {
  className: ``,
};

export default NewsletterSection;
