import { useStaticQuery, graphql } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';

import FlexBox from './FlexBox';
import FlexCell from './FlexCell';
import FooterLink from './FooterLink';

import icons from '../../images/vector/foreground/icons';
import Logo from '../../images/vector/foreground/voxis-logo-vertical-light.svg';

import styles from './Footer.module.scss';

const { IconFacebook, IconLinkedIn, IconTwitter, IconEmail } = icons;

const Footer = ({ className }) => {
  const data = useStaticQuery(graphql`
    query FooterInfoQuery {
      site {
        siteMetadata {
          legalName
          officeAddress
          companyNumber
          vatNumber
          socialLinks {
            facebook
            linkedIn
            twitter
          }
          emails {
            main
          }
        }
      }
    }
  `);

  const { legalName, officeAddress, companyNumber, /*vatNumber,*/ socialLinks, emails } = data.site.siteMetadata;

  return (
    <footer className={`${styles.footer} ${className}`}>
      <FlexBox className={styles.footerFlexer} direction="row">
        <FlexCell className={`${styles.cell} ${styles.logoCell}`}>
          <Logo className={styles.logoImg} />
        </FlexCell>
        <FlexCell className={`${styles.cell} ${styles.infoCell}`}>
          <div>
            <span className={styles.addressName}>{legalName}<br /><br /></span>
            {officeAddress.split(/,\s/g).map((line, index) => (<div key={index}>{line}</div>))}
          </div>
          <div className={styles.socials}>
            <a href={socialLinks.facebook} target="_blank" className={styles.socialIcon} rel="noopener noreferrer"><IconFacebook /></a>
            <a href={socialLinks.linkedIn} target="_blank" className={styles.socialIcon} rel="noopener noreferrer"><IconLinkedIn /></a>
            <a href={socialLinks.twitter} target="_blank" className={styles.socialIcon} rel="noopener noreferrer"><IconTwitter /></a>
            <a href={`mailto:${emails.main}`} className={styles.socialIcon}><IconEmail /></a>
          </div>
          <div className={styles.copyright}>
            © {new Date().getFullYear()} {legalName}
          </div>
          <div className={styles.companyDetails}>
            <span>Company: {companyNumber}</span>
            {/* <span>•</span>
            <span>VAT: {vatNumber}</span> */}
          </div>
        </FlexCell>
        <FlexCell className={`${styles.cell} ${styles.linkCellOne}`}>
          <FooterLink to="/use-cases" className={styles.footerLink}>Use Cases</FooterLink>
          <FooterLink to="/platform" className={styles.footerLink}>Platform</FooterLink>
          {/* <FooterLink to="/enterprise" className={styles.footerLink}>Enterprise</FooterLink> */}
          <FooterLink to="/pricing" className={styles.footerLink}>Pricing</FooterLink>
          <FooterLink to="/contact" className={styles.footerLink}>Talk to Us</FooterLink>
          <FooterLink to="/learn" className={styles.footerLink}>Learn</FooterLink>
          {/* <FooterLink to="https://gitlab.com/voxis.io/docs/-/wikis/home" className={styles.footerLink}>Docs</FooterLink> */}
        </FlexCell>
        <FlexCell className={`${styles.cell} ${styles.linkCellTwo}`}>
          {/* <FooterLink to="/learn" className={styles.footerLink}>Learn</FooterLink> */}
          {/* <FooterLink to="https://gitlab.com/voxis.io/docs/-/wikis/home" className={styles.footerLink}>Docs</FooterLink> */}
          {/* <FooterLink to="/company" className={styles.footerLink}>Company</FooterLink> */}
          <FooterLink to="/faq" className={styles.footerLink}>FAQ</FooterLink>
          {/* <FooterLink to="/support" className={styles.footerLink}>Support</FooterLink> */}
          {/* <FooterLink to="/press" className={styles.footerLink}>Press</FooterLink> */}
          {/* <FooterLink to="/terms" className={`${styles.footerLink} ${styles.footerLinkTerms}`}>
            <span className="thin">T&amp;C</span>
            <span className="wide">Terms and Conditions</span>
          </FooterLink> */}
          <FooterLink to="/privacy" className={`${styles.footerLink} ${styles.footerLinkPrivacy}`}>
            <span className="thin">Privacy</span>
            <span className="wide">Privacy Policy</span>
          </FooterLink>
          <FooterLink to="/preview" className={`${styles.footerLink} ${styles.footerLinkPreview}`}>
            <span className="thin">Preview</span>
            <span className="wide">Voxis Preview</span>
          </FooterLink>
        </FlexCell>
      </FlexBox>
    </footer>
  );
};

Footer.propTypes = {
  className: PropTypes.string,
};

Footer.defaultProps = {
  className: ``,
};

export default Footer;
