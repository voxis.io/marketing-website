import { useStaticQuery, graphql } from 'gatsby';

function __getUniqueMetadata(nodes) {
	return nodes.reduce((acc, node) => {
		acc.categories.add(node.frontmatter.category);
		node.frontmatter.tags.forEach(tag => acc.tags.add(tag));
		return acc;
	}, { categories: new Set(), tags: new Set() });
}

export default () => {
	const data = useStaticQuery(graphql`
		query BlogMetadata {
			allMarkdownRemark(filter: {children: {}, fields: {slug: {glob: "/learn/*/*"}}, frontmatter: {draft: {eq: false}}}) {
				nodes {
					id
					frontmatter {
						category
						tags
					}
				}
			}
		}
	`);

	const metadata = __getUniqueMetadata(data.allMarkdownRemark.nodes);

	return {
		categories: [ ...metadata.categories ],
		tags: [ ...metadata.tags ],
	};
}
