import { useStaticQuery, graphql } from 'gatsby';

export default () => {
	const data = useStaticQuery(graphql`
		query Features {
			allMarkdownRemark(filter: {children: {}, fields: {slug: {glob: "/platform/*"}}}, sort: {fields: frontmatter___weight, order: ASC}) {
				nodes {
					id
					frontmatter {
						title
						subTitle
						image
						weight
					}
					fields {
						slug
					}
				}
			}
		}
	`);

	return data.allMarkdownRemark.nodes;
}
