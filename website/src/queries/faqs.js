import { useStaticQuery, graphql } from 'gatsby';

export default () => {
	const data = useStaticQuery(graphql`
		query FAQs {
			allMarkdownRemark(filter: {children: {}, fields: {slug: {glob: "/faq/*/*"}}, frontmatter: {draft: {eq: false}}}, sort: {fields: frontmatter___weight, order: ASC}) {
				nodes {
					id
					html
					frontmatter {
						question
						pricingPage
						lastUpdated
					}
					fields {
						slug
					}
				}
			}
		}
	`);

	return data.allMarkdownRemark.nodes;
}
