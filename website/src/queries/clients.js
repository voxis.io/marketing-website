import { useStaticQuery, graphql } from 'gatsby';

export default () => {
	const data = useStaticQuery(graphql`
		query Clients {
			allMarkdownRemark(filter: {children: {}, fields: {slug: {glob: "/clients/*/client"}}}, sort: {fields: frontmatter___weight, order: ASC}) {
				nodes {
					id
					frontmatter {
						name
						url
						weight
						logo {
              childImageSharp {
								fixed(height: 30, quality: 80) {
									...GatsbyImageSharpFixed
								}
              }
            }
					}
					fields {
						slug
					}
				}
			}
		}
	`);

	return data.allMarkdownRemark.nodes;
}
