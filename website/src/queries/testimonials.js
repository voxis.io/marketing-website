import { useStaticQuery, graphql } from 'gatsby';

export default (filterClientTypes = []) => {
	const data = useStaticQuery(graphql`
		query Testimonials {
			allMarkdownRemark(filter: {children: {}, fields: { slug: {glob: "/testimonials/*/testimonial"}}, frontmatter: {hidden: {eq: false}}}, sort: {fields: frontmatter___weight, order: ASC}) {
				nodes {
					id
					frontmatter {
						name
						hidden
						job
						date
						clientType
						text
						weight
						photo {
              childImageSharp {
								fluid(maxWidth: 300 maxHeight: 300, quality: 80) {
									...GatsbyImageSharpFluid
								}
              }
            }
					}
					fields {
						slug
					}
				}
			}
		}
	`);

	const nodes = data.allMarkdownRemark.nodes;

	if (filterClientTypes.length) {
		return nodes.filter(node => filterClientTypes.includes(node.frontmatter.clientType));
	}

	return nodes;
};
