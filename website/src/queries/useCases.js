import { useStaticQuery, graphql } from 'gatsby';

export default () => {
	const data = useStaticQuery(graphql`
		query UseCases {
			allMarkdownRemark(filter: {children: {}, fields: {slug: {glob: "/use-cases/*"}}}, sort: {fields: frontmatter___weight, order: ASC}) {
				nodes {
					id
					frontmatter {
						titleSingular
						titlePlural
						subTitle
						image
						relatedFeatures
						demoRepoUrl
						demoName
						demoImage {
							childImageSharp {
								fluid(maxWidth: 300 maxHeight: 300, quality: 80) {
									...GatsbyImageSharpFluid
								}
              }
						}
						weight
					}
					fields {
						slug
					}
				}
			}
		}
	`);

	return data.allMarkdownRemark.nodes;
}
