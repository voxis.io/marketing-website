import { useStaticQuery, graphql } from 'gatsby';

export default () => {
	const data = useStaticQuery(graphql`
		query BlogPosts {
			allMarkdownRemark(filter: {children: {}, fields: {slug: {glob: "/learn/*/*"}}, frontmatter: {draft: {eq: false}}}, sort: {fields: frontmatter___date, order: DESC}) {
				nodes {
					id
					html
					frontmatter {
						title
						sponsored
						author
						date
						synopsis
						category
						tags
            featuredImage {
              childImageSharp {
								fluid(maxHeight: 600, quality: 80) {
									...GatsbyImageSharpFluid
								}
              }
            }
					}
					fields {
						slug
					}
				}
			}
		}
	`);

	return data.allMarkdownRemark.nodes;
}
