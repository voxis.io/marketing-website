import { useStaticQuery, graphql } from 'gatsby';

export default () => {
	const data = useStaticQuery(graphql`
		query PricingPlans {
			allMarkdownRemark(filter: {children: {}, fields: {slug: {glob: "/pricing-plans/*"}}}, sort: {fields: frontmatter___weight, order: ASC}) {
				nodes {
					id
					frontmatter {
						name
						priceMonthly
						priceAnnually
						priceText
						featureDeployedApps
						featureCompute
						featureDataStorage
						featureDataTransfer
						featureConcurrentBuilds
						mostPopular
						weight
					}
					fields {
						slug
					}
				}
			}
		}
	`);

	return data.allMarkdownRemark.nodes;
}
