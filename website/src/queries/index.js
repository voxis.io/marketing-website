import blogPosts from './blogPosts';
import blogMetadata from './blogMetadata';
import clients from './clients';
import faqs from './faqs';
import features from './features';
import pricingPlans from './pricingPlans';
import testimonials from './testimonials';
import useCases from './useCases';

export default {
	blogPosts,
	blogMetadata,
	clients,
	faqs,
	features,
	pricingPlans,
	testimonials,
	useCases,
};
