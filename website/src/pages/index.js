import React from 'react';

import CallToActionSection from '../sections/home/CallToActionSection';
import DemoSection from '../sections/home/DemoSection';
import Footer from '../components/layout/Footer';
import Header from '../components/layout/Header';
import HeroContent from '../sections/home/HeroContent';
import HeroSection from '../components/layout/HeroSection';
import LearnSection from '../sections/home/LearnSection';
import NewsletterSection from '../components/layout/NewsletterSection';
import Seo from '../components/Seo';
import TestimonialSection from '../components/TestimonialSection';
import TrustSection from '../sections/home/TrustSection';
import UseCasesSection from '../sections/home/UseCasesSection';

import styles from './index.module.scss';

const HomePage = () => (
  <>
    <Seo />
    <HeroSection className={styles.hero}>
      <Header />
      <HeroContent />
    </HeroSection>
    <TrustSection />
    <UseCasesSection />
    <DemoSection />
    <LearnSection />
    <TestimonialSection />
    <CallToActionSection />
    <NewsletterSection />
    <Footer />
  </>
);

export default HomePage;
