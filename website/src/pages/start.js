import React from 'react';

import Footer from '../components/layout/Footer';
import FormSection from '../sections/start/FormSection';
import Header from '../components/layout/Header';
import HeroContent from '../sections/start/HeroContent';
import HeroSection from '../components/layout/HeroSection';
import NewsletterSection from '../components/layout/NewsletterSection';
import Seo from '../components/Seo';

import styles from './start.module.scss';

const StartPage = () => {
  return (
    <>
      <Seo title="Get Started" description="Try Voxis for free. No credit card required. It's free to try and free to host static websites." />
      <HeroSection className={styles.hero}>
        <Header theme="light" />
        <HeroContent />
      </HeroSection>
      <FormSection />
      <NewsletterSection />
      <Footer />
    </>
  );
};

export default StartPage;
