import React, { useState } from 'react';

import Filters from '../sections/learn/Filters';
import Footer from '../components/layout/Footer';
import Header from '../components/layout/Header';
import HeroSection from '../components/layout/HeroSection';
import NewsletterSection from '../components/layout/NewsletterSection';
import PostsSection from '../sections/learn/PostsSection';
import Seo from '../components/Seo';

import styles from './learn.module.scss';

const LearnPage = () => {
  const [selectedCategory, setSelectedCategory] = useState(0);

  return (
    <>
      <Seo title="Learn" description="Read our blog to learn about building software for the cloud, using the Voxis platform, and the latest industry news." />
      <HeroSection className={styles.hero}>
        <Header theme="light" />
        <Filters selectedCategory={selectedCategory} setSelectedCategory={setSelectedCategory} />
      </HeroSection>
      <PostsSection selectedCategory={selectedCategory} />
      <NewsletterSection />
      <Footer />
    </>
  );
};

export default LearnPage;
