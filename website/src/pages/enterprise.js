import React from 'react';

import CtaSection from '../sections/enterprise/CtaSection';
import DeploymentSection from '../sections/enterprise/DeploymentSection';
import Footer from '../components/layout/Footer';
import Header from '../components/layout/Header';
import HeroSection from '../components/layout/HeroSection';
import NewsletterSection from '../components/layout/NewsletterSection';
import ReliabilitySection from '../sections/enterprise/ReliabilitySection';
import Seo from '../components/Seo';
import SupportSection from '../sections/enterprise/SupportSection';
import TestimonialSection from '../components/TestimonialSection';
import UseCaseSection from '../sections/enterprise/UseCaseSection';
import WhatSection from '../components/WhatSection';

import styles from './enterprise.module.scss';

const EnterprisePage = () => (
  <>
    <Seo title="Enterprise" description="Deploy Voxis to AWS, GCP, Azure, Digital Ocean, or on-premise, with enterprise grade features and support from the core team." />
    <HeroSection className={styles.hero}>
      <Header />
    </HeroSection>
    <DeploymentSection />
    <UseCaseSection />
    <WhatSection />
    <SupportSection />
    <ReliabilitySection />
    <CtaSection />
    <TestimonialSection filterClientTypes={[`enterprise`]} />
    <NewsletterSection />
    <Footer />
  </>
);

export default EnterprisePage;
