import React from 'react';

import ContactDetailsSection from '../sections/contact/ContactDetailsSection';
import Footer from '../components/layout/Footer';
import Header from '../components/layout/Header';
import HeroSection from '../components/layout/HeroSection';
import NewsletterSection from '../components/layout/NewsletterSection';
import Seo from '../components/Seo';

import styles from './contact.module.scss';

const ContactPage = () => {
  return (
    <>
      <Seo title="Talk to Us" description="Contact us by live chat or email for support or enquiries." />
      <HeroSection className={styles.hero}>
        <Header theme="light" />
      </HeroSection>
      <ContactDetailsSection />
      <NewsletterSection />
      <Footer />
    </>
  );
};

export default ContactPage;
