import React from 'react';

import Footer from '../components/layout/Footer';
import Header from '../components/layout/Header';
import NotFoundMessage from '../sections/404/NotFoundMessage';
import Seo from '../components/Seo';

const NotFoundPage = () => (
	<>
		<Seo title="404: Page Not Found" description="Terribly sorry! We seem to have lost that page somewhere." />
		<Header />
		<NotFoundMessage />
		<Footer />
	</>
);

export default NotFoundPage;
