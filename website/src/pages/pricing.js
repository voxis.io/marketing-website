import React from 'react';

import Footer from '../components/layout/Footer';
import Header from '../components/layout/Header';
import HeroSection from '../components/layout/HeroSection';
import NewsletterSection from '../components/layout/NewsletterSection';
import PricePlanSection from '../sections/pricing/PricePlanSection';
import PricingFaqsSection from '../sections/pricing/PricingFaqsSection';
import Seo from '../components/Seo';

import styles from './pricing.module.scss';

const PricingPage = () => {
  return (
    <>
      <Seo title="Pricing" description="Try Voxis for free. No credit card required. It's free to try and free to host static websites." />
      <HeroSection className={styles.hero}>
        <Header theme="light" />
      </HeroSection>
      <PricePlanSection />
      <PricingFaqsSection />
      <NewsletterSection />
      <Footer />
    </>
  );
};

export default PricingPage;
