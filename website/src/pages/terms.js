import React from 'react';

import Footer from '../components/layout/Footer';
import Header from '../components/layout/Header';
import HeroSection from '../components/layout/HeroSection';
import LegalSection from '../sections/terms/LegalSection';
import NewsletterSection from '../components/layout/NewsletterSection';
import Seo from '../components/Seo';

import styles from './terms.module.scss';

const TermsPage = () => {
  return (
    <>
      <Seo title="Terms & Conditions" description="Voxis terms and conditions of service." />
      <HeroSection className={styles.hero}>
        <Header theme="light" />
      </HeroSection>
      <LegalSection />
      <NewsletterSection />
      <Footer />
    </>
  );
};

export default TermsPage;
