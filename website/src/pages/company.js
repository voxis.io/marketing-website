import React from 'react';

import DetailsSection from '../sections/company/DetailsSection';
import Footer from '../components/layout/Footer';
import Header from '../components/layout/Header';
import HeroContent from '../sections/company/HeroContent';
import HeroSection from '../components/layout/HeroSection';
import InvestorSection from '../sections/company/InvestorSection';
import NewsletterSection from '../components/layout/NewsletterSection';
import Seo from '../components/Seo';
import StorySection from '../sections/company/StorySection';
import TeamSection from '../sections/company/TeamSection';

import styles from './company.module.scss';

const CompanyPage = () => {
  return (
    <>
      <Seo title="Company" description="Voxis is a British technology company building software to level-up software engineers." />
      <HeroSection className={styles.hero}>
        <Header theme="light" />
        <HeroContent />
      </HeroSection>
      <StorySection />
      <TeamSection />
      <InvestorSection />
      <DetailsSection />
      <NewsletterSection />
      <Footer />
    </>
  );
};

export default CompanyPage;
