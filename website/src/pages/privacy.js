import React from 'react';

import Footer from '../components/layout/Footer';
import Header from '../components/layout/Header';
import HeroSection from '../components/layout/HeroSection';
import LegalSection from '../sections/privacy/LegalSection';
import NewsletterSection from '../components/layout/NewsletterSection';
import Seo from '../components/Seo';

import styles from './privacy.module.scss';

const PrivacyPage = () => {
  return (
    <>
      <Seo title="Privacy Policy" description="Privacy policy, cookies, and GDPR data policy." />
      <HeroSection className={styles.hero}>
        <Header theme="light" />
      </HeroSection>
      <LegalSection />
      <NewsletterSection />
      <Footer />
    </>
  );
};

export default PrivacyPage;
