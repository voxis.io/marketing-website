import React from 'react';

import Footer from '../components/layout/Footer';
import Header from '../components/layout/Header';
import HeroContent from '../sections/preview/HeroContent';
import HeroSection from '../components/layout/HeroSection';
import InfoSection from '../sections/preview/InfoSection';
import DeploySection from '../sections/preview/DeploySection';
import Seo from '../components/Seo';
import SignUpSection from '../sections/preview/SignUpSection';

import styles from './preview.module.scss';

const PreviewPage = () => (
  <>
    <Seo title="Waitlist" description="Join the waitlist now! A select number of engineers are getting early access, don't miss out." />
    <HeroSection className={styles.hero}>
      <Header />
      <HeroContent />
    </HeroSection>
    <InfoSection />
    <DeploySection />
    <SignUpSection />
    <Footer />
  </>
);

export default PreviewPage;
