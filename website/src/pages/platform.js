import React from 'react';

import FeatureListSection from '../sections/platform/FeatureListSection';
import Footer from '../components/layout/Footer';
import Header from '../components/layout/Header';
import HeroSection from '../components/layout/HeroSection';
import NewsletterSection from '../components/layout/NewsletterSection';
import Seo from '../components/Seo';

import styles from './platform.module.scss';

const PlatformPage = () => {
  return (
    <>
      <Seo title="Platform Features" description="Features and functionality including; zero config deployments, CI/CD pipelines, infinite auto-scaling, automatic rollbacks, global CDN and more." />
      <HeroSection className={styles.hero}>
        <Header theme="light" />
      </HeroSection>
      <FeatureListSection />
      <NewsletterSection />
      <Footer />
    </>
  );
};

export default PlatformPage;
