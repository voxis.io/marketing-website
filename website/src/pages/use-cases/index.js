import React from 'react';

// import BlurbSection from '../../sections/use-cases/BlurbSection';
import Footer from '../../components/layout/Footer';
import Header from '../../components/layout/Header';
import HeroSection from '../../components/layout/HeroSection';
import NewsletterSection from '../../components/layout/NewsletterSection';
import Seo from '../../components/Seo';
import UseCaseListSection from '../../sections/use-cases/UseCaseListSection';

import styles from './index.module.scss';

const UseCasesPage = () => {
  return (
    <>
      <Seo title="Use Cases" description="Find solutions to common cloud engineering problems that Voxis can solve for your team." />
      <HeroSection className={styles.hero}>
        <Header theme="light" />
      </HeroSection>
      <UseCaseListSection />
      {/* <BlurbSection /> */}
      <NewsletterSection />
      <Footer />
    </>
  );
};

export default UseCasesPage;
