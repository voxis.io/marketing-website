import React from 'react';
import { graphql } from 'gatsby';

import BlurbSection from '../../../sections/use-cases/static-websites/BlurbSection';
import FeatureListSection from '../../../sections/use-cases/shared/FeatureListSection';
import Footer from '../../../components/layout/Footer';
import Header from '../../../components/layout/Header';
import HeroContent from '../../../sections/use-cases/shared/HeroContent';
import HeroSection from '../../../components/layout/HeroSection';
import HookSection from '../../../sections/use-cases/static-websites/HookSection';
import NewsletterSection from '../../../components/layout/NewsletterSection';
import Seo from '../../../components/Seo';

import styles from './index.module.scss';

const StaticWebsitesPage = ({ data }) => {
  return (
    <>
      <Seo title={`${data.markdownRemark.frontmatter.titlePlural} | Use Cases`} description={data.markdownRemark.frontmatter.subTitle} />
      <HeroSection className={styles.hero}>
        <Header theme="light" />
        <HeroContent />
      </HeroSection>
      <HookSection />
      <BlurbSection data={data} />
      <FeatureListSection relatedFeatures={data.markdownRemark.frontmatter.relatedFeatures} />
      <NewsletterSection />
      <Footer />
    </>
  );
};

export default StaticWebsitesPage;

export const query = graphql`
  query {
    markdownRemark(fields: { slug: { eq: "/use-cases/static-websites" } }) {
      id
      html
      frontmatter {
        titleSingular
        titlePlural
        subTitle
        relatedFeatures
      }
    }
  }
`;
