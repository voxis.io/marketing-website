import React from 'react';

import FaqListSection from '../sections/faq/FaqListSection';
import Footer from '../components/layout/Footer';
import Header from '../components/layout/Header';
import HeroSection from '../components/layout/HeroSection';
import NewsletterSection from '../components/layout/NewsletterSection';
import Seo from '../components/Seo';

import styles from './faq.module.scss';

const FaqPage = () => {
  return (
    <>
      <Seo title="FAQ" description="Frequently asked questions about the Voxis platform and the Voxis cloud." />
      <HeroSection className={styles.hero}>
        <Header theme="light" />
      </HeroSection>
      <FaqListSection />
      <NewsletterSection />
      <Footer />
    </>
  );
};

export default FaqPage;
