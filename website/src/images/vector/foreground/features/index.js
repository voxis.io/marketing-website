
import ImgFeatureInfiniteScaling from './auto-scaling.svg';
import ImgFeatureAutomatedTesting from './automated-testing.svg';
import ImgFeatureAutomaticRollbacks from './automatic-rollbacks.svg';
import ImgFeatureDashboard from './dashboard.svg';
import ImgFeatureDeployments from './deployments.svg';
import ImgFeatureFreeTls from './free-tls.svg';
import ImgFeatureGlobalCdn from './global-cdn.svg';
import ImgFeatureMetricsAndLogging from './metrics-and-logging.svg';
import ImgFeatureMultipleEnvironments from './multiple-environments.svg';
import ImgFeaturePredictableCosts from './predictable-costs.svg';
import ImgFeatureRedundancy from './redundancy.svg';
import ImgFeatureZeroConfigDeployments from './zero-config-deployments.svg';

export default {
	ImgFeatureInfiniteScaling,
	ImgFeatureAutomatedTesting,
	ImgFeatureAutomaticRollbacks,
	ImgFeatureDashboard,
	ImgFeatureDeployments,
	ImgFeatureFreeTls,
	ImgFeatureGlobalCdn,
	ImgFeatureMetricsAndLogging,
	ImgFeatureMultipleEnvironments,
	ImgFeaturePredictableCosts,
	ImgFeatureRedundancy,
	ImgFeatureZeroConfigDeployments,
};
