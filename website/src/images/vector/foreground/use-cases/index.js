
import ImgUseCaseApi from './api.svg';
import ImgUseCaseStaticWebsite from './static-website.svg';
import ImgUseCaseWebApp from './web-app.svg';

export default {
	ImgUseCaseApi,
	ImgUseCaseStaticWebsite,
	ImgUseCaseWebApp,
};
