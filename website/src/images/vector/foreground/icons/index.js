
import IconArrowLeftSolid from './arrow-left-solid.svg';
import IconArrowRightSolid from './arrow-right-solid.svg';
import IconArrowUpSolid from './arrow-up-solid.svg';
import IconAutomatedTesting from './vials-duotone.svg';
import IconAutomaticRollback from './undo-alt-duotone.svg';
import IconBackendApi from './brackets-curly-duotone.svg';
import IconBlog from './book-open-duotone.svg';
import IconBurgerMenu from './bars-solid.svg';
import IconCaret from './caret-down-solid.svg';
import IconCdn from './globe-europe-duotone.svg';
import IconCertificate from './award-duotone.svg';
import IconChat from './comments-duotone.svg';
import IconDashboard from './tachometer-alt-fast-duotone.svg';
import IconDocs from './book-duotone.svg';
import IconEmail from './envelope-solid.svg';
import IconEnterprise from './building-duotone.svg';
import IconExternalLink from './external-link-square-alt-solid.svg';
import IconFacebook from './facebook.svg';
import IconFaq from './question-circle-duotone.svg';
import IconFreeForever from './acorn-duotone.svg';
import IconHome from './home-duotone.svg';
import IconInfiniteScaling from './sort-size-up-duotone.svg';
import IconInsight from './chart-bar-duotone.svg';
import IconIot from './microchip-duotone.svg';
import IconLinkedIn from './linkedin.svg';
import IconLock from './lock-alt-duotone.svg';
import IconMobileAppApi from './mobile-alt-duotone.svg';
import IconMobileGameBackend from './game-console-handheld-duotone.svg';
import IconMultipleEnvironments from './trees-duotone.svg';
import IconNotFound from './mask-duotone.svg';
import IconOpenSource from './lock-open-alt-duotone.svg';
import IconPayAsYouGrow from './hands-usd-duotone.svg';
import IconPredictableCosts from './funnel-dollar-duotone.svg';
import IconRedundancy from './clone-duotone.svg';
import IconRocket from './rocket-duotone.svg';
import IconSayNo from './comment-times-duotone.svg';
import IconStaticWebsite from './browser-duotone.svg';
import IconStopWatch from './stopwatch-duotone.svg';
import IconTutorials from './graduation-cap-duotone.svg';
import IconTwitter from './twitter.svg';
import IconWebApp from './laptop-duotone.svg';

export default {
	IconArrowLeftSolid,
	IconArrowRightSolid,
	IconArrowUpSolid,
	IconAutomatedTesting,
	IconAutomaticRollback,
	IconBackendApi,
	IconBlog,
	IconBurgerMenu,
	IconCaret,
	IconCdn,
	IconCertificate,
	IconChat,
	IconDashboard,
	IconDocs,
	IconEmail,
	IconEnterprise,
	IconExternalLink,
	IconFacebook,
	IconFaq,
	IconFreeForever,
	IconHome,
	IconInfiniteScaling,
	IconInsight,
	IconIot,
	IconLinkedIn,
	IconLock,
	IconMobileAppApi,
	IconMobileGameBackend,
	IconMultipleEnvironments,
	IconNotFound,
	IconOpenSource,
	IconPayAsYouGrow,
	IconPredictableCosts,
	IconRedundancy,
	IconRocket,
	IconSayNo,
	IconStaticWebsite,
	IconStopWatch,
	IconTutorials,
	IconTwitter,
	IconWebApp,
};
