import { graphql } from 'gatsby';
import React from 'react';

import Button from '../../components/Button';
import Footer from '../../components/layout/Footer';
import Header from '../../components/layout/Header';
import HeroSection from '../../components/layout/HeroSection';
import NewsletterSection from '../../components/layout/NewsletterSection';
import PostContentSection from './PostContentSection';
import Seo from '../../components/Seo';
import ShareSection from '../../components/ShareSection';

import styles from './BlogPost.module.scss';

const BlogPost = ({ meta, data }) => {
  const title = data.markdownRemark.frontmatter.title || meta.title;
  const author = data.markdownRemark.frontmatter.author || meta.author;
  const synopsis = data.markdownRemark.frontmatter.synopsis || meta.synopsis;

  return (
    <>
      <Seo title={`${title} | Learn`} description={synopsis} author={author} />
      <HeroSection>
        <Header theme="light" />
        <div className={styles.backBtnContainer}>
          <Button className={styles.backBtn} icon="IconArrowLeftSolid" to="/learn">Back to blog</Button>
        </div>
      </HeroSection>
      <PostContentSection data={data} />
      <ShareSection />
      <NewsletterSection />
      <Footer />
    </>
  );
};

export default BlogPost;

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      tableOfContents
      excerpt(pruneLength: 500)
      html
      frontmatter {
        title
        sponsored
        author
        date
        synopsis
        category
        tags
        featuredImage {
          childImageSharp {
            fluid(maxHeight: 1200, quality: 80) {
              ...GatsbyImageSharpFluid
            }
            fixed(height: 1200, quality: 80) {
              ...GatsbyImageSharpFixed
            }
          }
        }
        featuredImageRenderMode
      }
    }
  }
`;
