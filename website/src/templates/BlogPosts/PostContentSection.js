import Img from 'gatsby-image';
import moment from 'moment';
import PropTypes from 'prop-types';
import React from 'react';

import VoxisLogoMark from '../../images/vector/foreground/voxis-logo-mark.svg';

import styles from './PostContentSection.module.scss';

const Tag = ({ className, children }) => <div className={className}>{children}</div>;

const FeaturedImage = ({ featuredImage, renderMode }) => {
  switch (renderMode) {
    case `constrained`:
      return (
        <Img fixed={featuredImage.childImageSharp.fixed} className={styles.featureImageWrapperConstrained} />
      );

    case `cover`:
      return (
        <>
          <Img fluid={featuredImage.childImageSharp.fluid} className={styles.featureImageWrapperCover} objectFit="cover" objectPosition="center" />
          <div className={styles.featuredImageCoverPusher}></div>
        </>
      );

    case `fullwidth`:
    default:
      return (
        <Img fluid={featuredImage.childImageSharp.fluid} className={styles.featureImageWrapperFullWidth} objectFit="cover" objectPosition="center" />
      );
  }
};

const PostContentSection = ({ className, data }) => {
  const { frontmatter: { title, date, author, category, tags, featuredImage, featuredImageRenderMode }, html } = data.markdownRemark;

  return (
    <section className={`${styles.section} ${className}`}>
      <div className={styles.floater}>
        <h1 className={styles.title}>{title}</h1>
        <div className={styles.meta}>
          <div className={styles.detail}>
            <div className={styles.date}>{moment(date).format(`D MMM, YYYY`)}</div>
            <div className={styles.author}>{author}</div>
          </div>
          <div className={styles.tags}>
            <Tag className={styles.categoryTag}>{category}</Tag>
            {tags.filter(tag => tag !== category).map((tag, index) => <Tag key={index} className={styles.tag}>{tag}</Tag>)}
          </div>
        </div>
        {featuredImage && <FeaturedImage featuredImage={featuredImage} renderMode={featuredImageRenderMode} />}
        <div className={styles.body} dangerouslySetInnerHTML={{ __html: html }} />
        <VoxisLogoMark className={styles.logoMark} />
      </div>
    </section>
  );
};

PostContentSection.propTypes = {
  className: PropTypes.string,
};

PostContentSection.defaultProps = {
  className: ``,
};

export default PostContentSection;
